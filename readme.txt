This repository contains the full source code of http://neib.org/ - yelp-like business directory written in DJANGO.

The content is distributed under a GPL license:
http://www.gnu.org/licenses/gpl-3.0.en.html

Main contributors: Łukasz Kidziński, Paweł Bedyński, Adam Kaliński
Contact: lukasz.kidzinski at gmail.com
