# -*- coding: utf-8 -*- 
"""
URLConf for neib project

Currently all urls are stored here.

"""

from django.conf.urls.defaults import patterns, include
from django.contrib import admin
from django.views.generic.simple import redirect_to

from business.views import *
from community.views import *
from gallery.views import *
from page.views import *
from common.views import *
from location.views import home_city

from main.views import *

from django.contrib.sitemaps import GenericSitemap

from badges.models import BADGES

from django.utils.translation import ugettext_lazy as _

#business_dict = {
#    'queryset': Business.objects.all(),
#    'date_field': 'date',
#}
#user_dict = {
#    'queryset': Profile.objects.all(),
#    'date_field': 'date',
#}
#sitemaps = {
#    'businesses': GenericSitemap(business_dict, priority=0.6),
#    'users': GenericSitemap(user_dict, priority=0.6),
#}

admin.autodiscover()

urlpatterns = patterns('django.views.generic.simple',
    ((r'^profile/edytuj/$'), 'direct_to_template', {'template': 'community/profile.edit.choose.html'}, 'profile_edit'),
    ((r'^xd_receiver.htm$'), 'direct_to_template', {'template': 'misc/xd_receiver.htm'}),
    ((r'^test.html$'), 'direct_to_template', {'template': 'tmp/test.html'}),
    ((r'^internship/$'), 'direct_to_template', {'template': 'recruit/recruit.html'}, 'recruit'),
    ((r'^contest/terms/$'), 'direct_to_template', {'template': 'common/conditions.html'}, 'competition_conditions'),
    ((r'^googlehostedservice.html$'), 'direct_to_template', {'template': 'googlehostedservice.html'}, 'google_hosted_service'),
    ((r'^google24afc633668495b8.html$'), 'direct_to_template', {'template': 'google24afc633668495b8.html'}, 'google_hosted_service'),
)

if 0:
    urlpatterns += patterns('',
        (r"static/(?P<path>.*)$", "django.views.static.serve", {
                "document_root": settings.MEDIA_ROOT,
        }),
    )

urlpatterns += patterns('',
    # REDIRECTY
    ((r'^users/(?P<username>[-\w]+)/$'), redirect_to, {'url': '/profil/%(username)s/'}),
    ((r'^biz/krzyz-pod-paacem-prezydenckim-warszawa/$'), redirect_to, {'url': '/biz/krzyz-pod-palacem-prezydenckim-warszawa/'}),
    ((r'^page/recruit/$'), redirect_to, {'url': '/recruit/'}),
    ((r'^recruit/$'), redirect_to, {'url': '/internship/'}),
    ((r'^category/(?P<category_name>[-\w]+)/$'), redirect_to, {'url': '/k/warszawa/%(category_name)s/'}),
    ((r'^api/$'), redirect_to, {'url': 'http://code.google.com/p/dood-api/wiki/HowItWorks'}),

#    ((r'^ping/$'), main_ping_google, {}, 'main_ping_google'),

    # main
    ((r'^$'), home_search, {}, 'home'),
    ((r'^start/$'), home, {}, 'start'),
    ((r'^start/search/$'), home_search, {}, 'home_search'),
    ((r'^about/$'), about_us, {}, 'about_us'),

    # static page urls
    ((r'^mobile/emulator/$'), iphone_emulator, {}, 'iphone_emulator'),

    ((r'^page/(?P<page_id>[-\w]+)/$'), generic_page, {}, 'generic_page'),
    ((r'^page/contest/$'), competition, {}),
    ((r'^contest/$'), competition, {}, 'competition'),
    ((r'^elite/'), elite_info, {}, 'elite_info'),
    ((r'^robots.txt$'), robots_txt, {}, 'robots.txt'),
    ((r'^500error.html$'), error500test, {}, 'error500test'),
    ((r'^dynamicmedia/category.widget.js$'), category_widget_js, {}, 'dynamic_media'),
    ((r'^dynamicmedia/(?P<file>.+\..+)$'), dynamic_media, {}, 'dynamic_media'),
#    ((r'^sitemap.xml$'), 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),

    # BIZNES URLS W Z�YM MIEJSCU!
    ((r'^biz/list/new/$'), business_list_add, {}, 'business_list_add'),
    ((r'^biz/list/edit/(?P<business_list_id>[-\w]+)/$'), business_list_add, {}, 'business_list_edit'),
    ((r'^biz/list/remove/(?P<business_list_id>[-\w]+)/$'), business_list_del, {}, 'business_list_del'),
    ((r'^biz/list/add-business/(?P<business_list_id>[-\w]+)/(?P<business_slug>[-\w]+)/$'), business_list_item_add, {}, 'business_list_item_add'),
    ((r'^biz/list/remove-business/(?P<business_list_id>[-\w]+)/(?P<business_slug>[-\w]+)/$'), business_list_item_del, {} , 'business_list_item_del'),
    ((r'^biz/list/(?P<business_list_id>[-\w]+)/$'), business_list_detail, {}, 'business_list_detail'),
    ((r'^biz/list/(?P<business_list_id>[-\w]+)/(?P<flag>[-\w]+)/$'), business_list_detail, {}, 'business_list_detail'),
    ((r'^biz/category/(?P<category_name>[-\w]+)/$'), business_category, {}, 'category_listing'),
    ((r'^search/(?P<category_name>[-\w]+)/$'), business_search, {}, 'business_search_category'),
    ((r'^search/$'), business_search, {'category_name': 0}, 'business_search'),
    ((r'^biz/review/(?P<review_id>[-\w]+)/(?P<flag>[-\w]+)/$'), review_detail_flag, {}, 'review_detail_flag'),
    ((r'^biz/review/(?P<review_id>[-\w]+)/$'), review_detail, {}, 'review_detail'),
    ((r'^biz/write-review/$'), review_add_search, {}, 'review_add_search'),
    ((r'^biz/write-review/choose-business/$'), review_add_select, {}, 'review_add_select'),
    ((r'^biz/write-review/(?P<business_slug>[-\w]+)/$'), review_add, {}, 'review_add'),

    ((r'^k/(?P<city_slug>[-\w]+)/(?P<category_name>[-\w]+)/$'), business_city_category, {}, 'category_city_listing'),

    # GALLERY URLS W Z�YM MIEJSCU
    ((r'^gallery/photo/(?P<photo_id>[-\d]+)/$'), photo_detail, {}, 'photo_detail'),
    ((r'^gallery/photo/(?P<photo_id>[-\d]+)/usun/$'), photo_delete, {}, 'photo_delete'),

    ((r'^faq/$'), 'businessownersguide.views.faq', {'group': 'use(r', 'template': 'faq/faq.html'}, 'faq'),

    # urlsy aplikacji alfabetycznie :)
#    ((r'^blog/'), include('articles.urls')),
    ((r'^api/'), include('api2.urls')),
    ((r'^biz/'), include('business.urls')),
    ((r'^feedback/'), include('feedback.urls')),
    ((r'^gallery/'), include('gallery.urls')),
    ((r'^inline-admin/'), include('inlineadmin.urls')),
    ((r'^json/'), include('api.urls')),
    ((r'^account/'), include('registration.urls')),
    ((r'^compliments/'), include('compliments.urls')),
    ((r'^mailing/'), include('mailing.urls')),
    ((r'^newsletter/'), include('newsletter.urls')),
    ((r'^badges/'), include('badges.urls')),
    ((r'^partners/$'), include('partners.urls')),
    ((r'^notification/'), include('notification.urls')),
    ((r'^profile/'), include('community.urls')),
    ((r'^forum/'), include('forum.urls')),
#    ((r'^rss/'), include('rss.urls')),
    ((r'^socialregistration/'), include('socialregistration.urls')),
    ((r'^messages/'), include('messages.urls')),
    ((r'^owner/'), include('ownership.urls')),
    ((r'^owner/guide/'), include('businessownersguide.urls')),
    ((r'^events/'), include('event.urls')),
    ((r'^invite/'), include('invite.urls')),

    (r'^rosetta/', include('rosetta.urls')),    
    # admin urls
    #((r'^admin/doc/'), include('django.contrib.admindocs.urls')),
    ((r'^adm/'), include(admin.site.urls)),
    #((r'^panel/'), include('panel.urls')),
    #((r'^neibadmin/'), include('doodadmin.urls')),
    
    ((r'^'), include('location.urls')),

    ((r'^toolbox/category_tag_add.html$'),category_tag_chooser, {}),       
)

try:
    from localurls import local_patterns
    urlpatterns += local_patterns
except ImportError:
    pass
