from community.models import FriendshipProposition, Friendship
from django.contrib.auth.models import User

def add_friend_propositions():
    qs = User.objects.all()
    for user in qs:
        users = Friendship.objects.get_friends(user)
        a = dict()
        for u in users:
            u_friends = Friendship.objects.get_friends(u)
            for f in u_friends:
                if not f in a.keys():
                    a[f] = 0
                a[f] += 1
        for item in a.keys():
            user1 = item
            user2 = user
            if (user1 != user2) and (not Friendship.objects.are_friends(user1, user2)):    
                if (FriendshipProposition.objects.filter(user1=user1, user2=user2).count() == 0) and (FriendshipProposition.objects.filter(user1=user2, user2=user1).count() == 0):
                    fp = FriendshipProposition(user1=user1, user2=user2)
                    fp.level = a[item]
                    fp.save()
    return

add_friend_propositions()