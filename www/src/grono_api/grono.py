'''
Created on 2009-11-08

@author: kalin
'''

import urllib2, urllib
import json

api_url = "http://grono.net/api2/"

def chec_errors(res):
    if int(res['status']) != 0:
            raise GronoError("%s:%s" % (res['error'],res['description']))

class Grono(object):                
    def login(self, login, password):
        res = self.fetch_data('login',{'login':login, 'password':password})
        chec_errors(res)
        self.cook = res[u'data']
        return res[u'data']

    def friend_list(self):
        res = self.fetch_data(['friend','list' ], {'___cook':self.cook})
        chec_errors(res)
        return res['data']
        
    def send_message(self, to, subject, body):
        res = self.fetch_data(['message','send'], {'to':to,
                                                    'subject':subject,
                                                    'body':body,
                                                    '___cook':self.cook})
        chec_errors(res)
    
    def my_id(self):
        res = self.fetch_data(['user','myid'], {'___cook':self.cook})
        chec_errors(res)
        return int(res['data'])
    
    def profile(self,id):
        res = self.fetch_data(['user','info'], {'id':id, '___cook':self.cook})
        chec_errors(res)
        return res['data']
    
    def my_profile(self):
        return self.profile(self.my_id())
    
    def fetch_data(self, params, data):
        response = None
        if isinstance(params, str):
            params = [params]
        post = urllib.urlencode(data)
        url = "%s%s/" % (api_url, '/'.join(params))
        http = urllib2.urlopen(url, post)
        response = json.load(http)
        return response
    
class GronoError(Exception):
    pass