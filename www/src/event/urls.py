from django.conf.urls.defaults import *

urlpatterns = patterns('event.views',
    url((r'^promotions/add/(?P<slug>[-\w]+)/$'),
        view='promotion_add_to_business',
        name='promotion_add_to_business'
    ),
    url((r'^add/$'),
        view='events_add',
        name='events_add'
    ),    
    url((r'^add/(?P<slug>[-\w]+)/$'),
        view='events_add_to_business',
        name='events_add_to_business'
    ),
    url((r'^(?P<slug>[-\w]+)/$'),
        view='events_details',
        name='events_details'
    ),
    url((r'^edit/(?P<event_slug>[-\w]+)/$'),
        view='events_edit',
        name='events_edit'
    ),
    url((r'^remove/(?P<event_slug>[-\w]+)/$'),
        view='events_delete',
        name='events_delete'
    ),
    url((r'^$'),
        view='events',
        name='events'
    ),
)