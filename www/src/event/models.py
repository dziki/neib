# -*- coding: utf-8 -*-
import time
from django.utils.translation import ugettext_lazy as _
from django.db.models import permalink
from django.db import models

from location.models import GeoModel
from business.models import Business
from gallery.models import GalleryField, Photo, Gallery

from common.string import unique_slug
from django.contrib.auth.models import User
from django.contrib.contenttypes import generic
from flagging.models import Flag
from international.managers import TransManager

VERIFY_CHOICES = (
    (0, 'Nie'),
    (1, 'Tak'),
)

class Category(models.Model):
    """Category model."""
    title = models.CharField(_('title'), max_length=100)
    slug = models.SlugField(_('slug'), unique=True, max_length=100)

    class Meta:
        verbose_name = _('category')
        verbose_name_plural = _('categories')
        ordering = ('title',)

    def __unicode__(self):
        return u'%s' % self.title

#    def save(self, *args, **kwargs):
#        self.slug = slugify(self.title)
#        super(Category, self).save(*args, **kwargs)

    @permalink
    def get_absolute_url(self):
        return ('event_category_detail', None, {'slug': self.slug})

class Event(GeoModel):
    """ Business description """
    slug = models.SlugField(_('slug'), unique=True, max_length=100)
    user = models.ForeignKey(User, blank=True, null=True, verbose_name=_("Added by"), related_name="dood_event_set" )
    business = models.ForeignKey(Business, verbose_name=_("Venue"))
    category = models.ForeignKey(Category, verbose_name=_("Category"))
    gallery = GalleryField("event")
#    participants = models.ManyToManyField(User, blank=True, null=True, verbose_name="Uczestnicy")

    name = models.CharField(max_length=100, verbose_name=_("Name"))
    cost = models.CharField(max_length=200, verbose_name=_("Cost"), null=True, blank=True)
    www = models.CharField(max_length=200, verbose_name=_("WWW"), null=True, blank=True)
    email = models.CharField(max_length=100, verbose_name=_("E-mail"), null=True, blank=True)
    event_date = models.DateTimeField(_("Date"))

    description = models.TextField(max_length=2000, verbose_name=_("Description"), null=True, blank=True)
    comment = models.TextField(max_length=255, verbose_name=_("Comment"), null=True, blank=True)

    verified = models.IntegerField(verbose_name=_("Checked"), choices=VERIFY_CHOICES, default=0)

#    objects = BusinessManager()

    flags = generic.GenericRelation(Flag)
    
    active = models.BooleanField(verbose_name=_("Active"), default=1)

    export_fields = ('name', 'business', 'description')
    export_fields_ex = ('category', 'user', 'www', 'email')
    lang = models.CharField(max_length=6, verbose_name=_("Language"), null=False, blank=False, default='pl')
    
    objects = TransManager()

    def photo(self):
        photo = Photo.objects.get_gallery_photo(self.gallery)
        if photo is None:
            photo = Photo()
            photo.disp = 'event'
        return photo

    def __unicode__(self):
        return u'%s' % self.name
    
    def save(self, *args, **kwargs):
        instance = self
        slug = instance.name + ' ' + instance.business.name + ' ' + instance.business.city
        instance.slug = unique_slug(Event, 'slug', slug)
            
        gallery = Gallery()
        gallery.save()
        
        instance.gallery = gallery

        super(Event, self).save(*args, **kwargs)


    @classmethod
    def get_sidebar_template(cls):
        return "event/event.sidebar.html"

    @permalink
    def get_absolute_url(self):
        return ('events_details', None, {'slug': self.slug})
    
class Promotion(GeoModel):
    slug = models.SlugField('slug', unique=True, max_length=100)
    user = models.ForeignKey(User, blank=True, null=True, verbose_name=_("Added by"), related_name="dood_promotion_set" )
    business = models.ForeignKey(Business, verbose_name=_("Venue"))

    name = models.CharField(max_length=30, verbose_name=_("Name"))
    description = models.TextField(max_length=160, verbose_name=_("Description"))

    date_from = models.DateField(_("Start"))
    date_to = models.DateField(_("End"))

    lang = models.CharField(max_length=6, verbose_name=_("Language"), null=False, blank=False, default='pl')

    def expires(self):
        return int(time.mktime(self.date_to.timetuple())).__str__()
    def title(self):
        return self.name

    export_fields = ('title', 'description', 'expires', 'version', 'business')
    export_fields_ex = ()
    
    objects = TransManager()

    def save(self, *args, **kwargs):
        instance = self
        slug = instance.name + ' ' + instance.business.name + ' ' + instance.business.city
        instance.slug = unique_slug(Promotion, 'slug', slug)

        super(Promotion, self).save(*args, **kwargs)


    def __unicode__(self):
        return u'%s' % self.name
