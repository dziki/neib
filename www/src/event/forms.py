# -*- coding: utf-8 -*- 
"""
Profile form. There will be more fields. TODO
"""
import datetime
from django import forms
from django.forms import ModelForm, Form
from django.forms.extras.widgets import SelectDateWidget
from django.forms import SplitDateTimeField
from django.contrib.auth.models import User
from date.widgets import SplitSelectDateTimeWidget
from widgets.date import EventTimeWidget, EventDateWidget
from django.utils.translation import ugettext_lazy as _

from event.models import *

class EventForm(ModelForm):
    class Meta:
        model = Event
        fields = (
            'business',
            'category',
            'event_date',
            'name',
            'cost',
            'www',
            'email',
            'description',
        )
        widgets={
            'business': forms.TextInput(attrs={'id':'biz-search'}),
        }
    event_date = SplitDateTimeField(label=_("Date"), 
                                    widget=EventTimeWidget(), 
                                    input_date_formats=['%d.%m.%Y','%d-%m-%Y'],
                                    input_time_formats=['%H:%M','%H'])
     #event_date = forms.DateTimeField(label="Data", widget=SplitSelectDateTimeWidget(None, None, 5, 60), initial=datetime.now())
     
    
    def __init__(self, *args, **kwargs):
        super(EventForm, self).__init__(*args, **kwargs)

class PromotionForm(ModelForm):
    class Meta:
        model = Promotion
        fields = (
            'name',
            'description',
            'date_from',
            'date_to',
        )
#        widgets={
#            'business': forms.TextInput(attrs={'id':'biz-search'}),
#        }
    name = forms.fields.CharField(label=_("Name"),
                                  max_length=30,
                                  help_text=_("Write short promotion title (max 30 characters)")) 
    date_from = forms.fields.DateField(label=_("Start"),
                                    widget=forms.extras.widgets.SelectDateWidget(),
                                    initial=datetime.datetime.now(),
                                    help_text=_("When does it start?"))
    date_to = forms.fields.DateField(label=_("End"), 
                                    widget=forms.extras.widgets.SelectDateWidget(), 
                                    initial=datetime.datetime.now(),
                                    help_text=_("When does it finish?"))   
    def __init__(self, *args, **kwargs):
        super(PromotionForm, self).__init__(*args, **kwargs)
