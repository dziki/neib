# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse

from forum.forms import PostForm
from forum.models import Post
from event.forms import *

from notify.models import notify_user
from inlineadmin.decorators import permission_required
from django.utils import translation
from django.shortcuts import get_object_or_404

def events_details(request, slug):
    event = get_object_or_404(Event, slug = slug)
    posts = Post.objects.get_posts(event)

    if request.POST:
        form = PostForm(request.POST)
        if form.is_valid():
            form.instance.set_target(event)
            if request.user.is_authenticated():
                form.instance.user = request.user
                form.save()
            else:
                raise Exception, "LOGIN needed to add event"
    else:
        form = PostForm()

    return render_to_response('event/event.html',
                              context_instance=RequestContext(request,
                                                              {"event": event,
                                                               "form": form,
                                                               "posts": posts,
                                                               }))

def events(request):
    area = request.session['area']
    events = Event.objects.all().filter(verified=1, business__position__within = area.poly).order_by('-event_date')
    categories = Category.objects.all()

    return render_to_response('event/events.html',
                              context_instance=RequestContext(request,
                                                              {"events": events,
                                                               "categories": categories
                                                               }))

@login_required()
def events_add(request):
    if request.POST:
        form = EventForm(request.POST)
        if form.is_valid():
            form.instance.user = request.user
            form.instance.verified = True
            form.instance.save()
            notify_user(request, u'Dodawanie wydarzenia powiodło się', None)
            return HttpResponseRedirect(reverse('events'))
        else:
            notify_user(request, u'Wystąpił problem. Prosimy o poprawienie poniższych danych', None)

    else:
        form = EventForm()

    return render_to_response('event/event.add.html',
                              context_instance=RequestContext(request,
                                                              {"events": events,
                                                               "form": form
                                                               }))

@login_required()
@permission_required(1)
def events_edit(request, event_slug):
    event = Event.objects.get(slug__exact=event_slug)
    if request.POST:
        form = EventForm(request.POST, instance=event)
        if form.is_valid():
            form.instance.save()
            notify_user(request, u'Dodawanie wydarzenia powiodło się', None)
            return HttpResponseRedirect(reverse('events'))
        else:
            notify_user(request, u'Wystąpił problem. Prosimy o poprawienie poniższych danych', None)

    else:
        form = EventForm(instance=event)

    return render_to_response('event/event.add.html',
                              context_instance=RequestContext(request,
                                                              {"form": form,
                                                               "edit": True}))

@login_required()
@permission_required(1)
def events_delete(request, event_slug):
    event = Event.objects.get(slug__exact=event_slug)
    event.verified = 0
    event.save()

    return HttpResponseRedirect(reverse('events'))

@login_required()
def events_add_to_business(request, slug):
    business = Business.objects.get(slug=slug)
    if request.POST:
        form = EventForm(request.POST)
        if form.is_valid():
            form.instance.user = request.user
            form.save()
            form.instance.verified = True
            form.instance.save()
        return events(request)
    else:
        form = EventForm(initial={'business': business.id})

    return render_to_response('event/event.add.html',
                              context_instance=RequestContext(request,
                                                              {"events": events,
                                                               "form": form
                                                               }))

@login_required()
def promotion_add_to_business(request, slug):
    business = Business.objects.get(slug=slug)
    if request.POST:
        form = PromotionForm(request.POST)
        if form.is_valid():
            form.instance.user = request.user
            form.instance.business = business
            form.instance.verified = True
            form.instance.save()
            notify_user(request, u'Dodawanie promocji powiodło się', None)
            return HttpResponseRedirect(reverse('business_detail', args=[business.id]))
        else:
            notify_user(request, u'Wystąpił problem. Prosimy o poprawienie poniższych danych', None)

    else:
        form = PromotionForm(initial={'business': business.id})

    return render_to_response('event/promotion.add.html',
                              context_instance=RequestContext(request,
                                                              {"form": form
                                                               }))


