from django.contrib import admin
from event.models import *

class EventAdmin(admin.ModelAdmin):
    fields = ['name','event_date','cost','www','email','slug','user','business','category','gallery','description','comment','verified']
    prepopulated_feields = {'slug': ('name',),}

class CategoryAdmin(admin.ModelAdmin):
    prepopulated_feields = {'slug': ('title',),}

class PromotionAdmin(admin.ModelAdmin):
    pass

admin.site.register(Event, EventAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Promotion, PromotionAdmin)