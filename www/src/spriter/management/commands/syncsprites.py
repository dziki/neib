# -*- coding: utf-8 -*-
from spriter import create_sprites
from django.core.management.base import BaseCommand

import logging as log

__author__ = 'kidzik'

log.basicConfig(level=log.INFO)

class Command(BaseCommand):
    args = '[]'
    help = 'Create sprites'

    option_list = BaseCommand.option_list

    def handle(self, *args, **options):
        create_sprites()
