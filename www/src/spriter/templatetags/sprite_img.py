from spriter.models import Image
from common.helpers import generate_media_url

__author__ = 'kidzik'

from django.template import Library
from django.core.urlresolvers import reverse

register = Library()

@register.simple_tag
def sprite_img(sprite, filename, alt):
    img = Image.objects.get(sprite=sprite, filename=filename)
    sprite_url = generate_media_url("gfx/sprites/" + img.sprite + ".png")
    rect = "clip:rect(%dpx %dpx %dpx %dpx)" % (img.y,img.x+img.width,img.y+img.height,img.x)
    return "<div class=\"sprite\" style=\"width: " + str(img.width) +"px; height: " + str(img.height)\
           + "px\"><img src=\"" + sprite_url + "\" alt=\"" + alt + "\" style=\"position:absolute; top: -" + str(img.y) +"px; left: -" + str(img.x) + "px; "\
           + rect + "\"/></div>"