from PIL import Image
import os
import spriter

__author__ = 'kidzik'

def _files_in_dir(sprite_im, sprite_name, dir):
    full_dir = "static/" + dir
    objects = os.listdir(full_dir)
    sprite_size = sprite_im.size
    for item in objects:
        full_item = full_dir + "/" + item
        if os.path.isdir(full_item) == True:
            continue

        img_obj = spriter.models.Image()
        img_obj.sprite = sprite_name

        im = Image.open(full_item)
        size = im.size
        if sprite_im.curr_y + size[1] > sprite_size[1]:
            sprite_im.curr_y = 0
            sprite_im.curr_x = sprite_im.max_x

        img_obj.x = sprite_im.curr_x
        img_obj.y = sprite_im.curr_y
        img_obj.width = size[0]
        img_obj.height = size[1]
        img_obj.filename = item
        img_obj.save()

        sprite_im.paste(im, (sprite_im.curr_x, sprite_im.curr_y))
        if sprite_im.max_x < sprite_im.curr_x + size[0]:
            sprite_im.max_x = sprite_im.curr_x + size[0]
        sprite_im.curr_y = sprite_im.curr_y + size[1]
    return

def create_sprites():
    sprite = Image.new("RGBA", (200,200))
    sprite.curr_x = 0
    sprite.max_x = 0
    sprite.curr_y = 0

    sprite_name = "categories"

    spriter.models.Image.objects.all().delete()

    DIRS = ['gfx/icons/categories', ] #['gfx/icons', 'gfx/jquery/rating',]
    for dir in DIRS:
        _files_in_dir(sprite, sprite_name,dir)

    sprite.save("static/gfx/sprites/" + sprite_name + ".png")