from date.models import TimedModel
from django.db import models

__author__ = 'kidzik'

class Image(TimedModel):
    sprite = models.CharField(max_length=150, verbose_name = "Sprite")
    filename = models.CharField(max_length=150, verbose_name = "Nazwa pliku")
    x = models.IntegerField(verbose_name = "X")
    y = models.IntegerField(verbose_name = "Y")
    width = models.IntegerField(verbose_name = "Width")
    height = models.IntegerField(verbose_name = "Height")
