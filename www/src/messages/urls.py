from django.conf.urls.defaults import *
from django.views.generic.simple import redirect_to

from messages.views import *

urlpatterns = patterns('',
    url(r'^$', redirect_to, {'url': 'odebrane/'}),
    url((r'^inbox/$'), inbox, name='messages_inbox'),
    url((r'^sent/$'), outbox, name='messages_outbox'),
    url((r'^write/$'), compose, name='messages_compose'),
    url((r'^write/(?P<recipient>[\+\w]+)/$'), compose, name='messages_compose_to'),
    url((r'^reply/(?P<message_id>[\d]+)/$'), reply, name='messages_reply'),
    url((r'^details/(?P<message_id>[\d]+)/$'), view, name='messages_detail'),
    url((r'^remove/(?P<message_id>[\d]+)/$'), delete, name='messages_delete'),
    url((r'^undelete/(?P<message_id>[\d]+)/$'), undelete, name='messages_undelete'),
    url((r'^bin/$'), trash, name='messages_trash'),
)
