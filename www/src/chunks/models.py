# -*- coding: utf-8 -*- 
from django.db import models

class Chunk(models.Model):
    """
    A Chunk is a piece of content associated
    with a unique key that can be inserted into
    any template with the use of a special template
    tag
    """
    class Meta:
        unique_together = ['key', 'lang']
    
    key = models.CharField(help_text="A unique name for this chunk of content", blank=False, max_length=255, unique=False)
    lang = models.CharField(help_text="Content Language", blank=False, max_length=5, unique=False)
    markup = models.CharField(help_text="Markup used", blank=True, max_length=10, unique=False)
    title = models.CharField(help_text="Title", blank=True, max_length=255, unique=False)
    content = models.TextField(blank=True)
    description = models.CharField(blank=True, max_length=64, help_text="Short Description")

    def __unicode__(self):
        return u"%s" % (self.key,)
