from django.contrib import admin
from models import Chunk

class ChunkAdmin(admin.ModelAdmin):
  list_display = ('key', 'title', 'lang', 'markup', 'description',)
  search_fields = ('key','lang',  'content')

admin.site.register(Chunk, ChunkAdmin)
