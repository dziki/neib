from django.shortcuts import get_object_or_404
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import Http404
from django.utils import translation

from chunks.models import Chunk
    
def generic_page(request, page_id):
    try:
        page = Chunk.objects.get(key="page_"+page_id, lang=translation.get_language())
    except Chunk.DoesNotExist:
        try:
            page = Chunk.objects.filter(key="page_"+page_id)[0]
        except IndexError:
            raise Http404
    return render_to_response('page/default.html',
        context_instance=RequestContext(request,
            {"page": page, }
            ))
    
