from providers import GoogleContactProvider
from providers.base import BadAuthorizationError
import unittest        

class TestGoogleProvider(unittest.TestCase):
    def __init__(self,*args, **kwargs):
        self.passwd = None
        super(TestGoogleProvider, self).__init__(*args, **kwargs)
    
    def setUp(self):
        prov = GoogleContactProvider()
        self.passwd = self.passwd or raw_input("Password: ")
        prov.authenticate('adamkalinski',self.passwd)
        self.prov = prov
    
    def test_getting_contacts(self):
        prov = GoogleContactProvider()
        self.assertTrue(prov.authenticate('adamkalinski',self.passwd))
        cl = prov.get_contacts()
        self.assertTrue(type(cl) is list, "No contact list")
        
    def test_login_fail(self):
        prov = GoogleContactProvider()
        try:
            self.assertRaises(BadAuthorizationError, prov.authenticate('A_person','bad pa$$sord'))     
        except BadAuthorizationError:
            pass   
        
    def test_token(self):
        self.assertTrue(isinstance(self.prov.token, str))
        
    def test_auth_by_token(self):
        gp = GoogleContactProvider()
        gp.auth_by_token(self.prov.token)
        cl = gp.get_contacts()
        self.assertTrue(type(cl) is list, "No contact list")