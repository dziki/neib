# -*- coding: utf-8 -*- 
'''
Created on 2009-11-13

@author: kalin
'''
from base import BaseContactProvider, BadAuthorizationError, Contact, send_email_invitations
import xmlrpclib

# To mozesz chceic wrzucic do settings.py ?
WIELOKONT_VALIDATION_CODE = 'lmxyjfluxhufusok'
WIELOKONT_HOST = 'doodpl'
WIELOKONT_RPC_ADDRESS = "http://www.wielokont.pl/get_rpc.php"

def _rpc_get_contacts(login, domain, password):
    wielokont = xmlrpclib.ServerProxy(WIELOKONT_RPC_ADDRESS)
    resp = wielokont.importContacts([{'email': login,
                               'domain': domain,
                               'password': password}],
                               {'sort':'all'},
                               {'clientId':0}, # To ma byc przydatne dla statystyk. 
                               #poprostu jakies id itentyfikujące uzytkowników neib.org
                               {'host':WIELOKONT_HOST},
                               {'validCode':WIELOKONT_VALIDATION_CODE})
    
    if len(resp['error'].strip()) > 0 or not resp['contacts'][0].has_key('contactData'):
        raise BadAuthorizationError, resp['error'].strip() or "Unknown error"
    
    return resp['contacts'][0]['contactData']


class WielokontContactProvider(BaseContactProvider):
    contacts = []
    domain = None
    
    def authenticate(self, login, passwd):
        self.login = login
        self.password = passwd
        if self.domain is None:
            raise Exception, "Use WielokontContactProviderFactory or set the domain property"
        self.contacts = _rpc_get_contacts(login,self.domain ,passwd)
        
    def get_all_contacts(self):
        contact_list = []
        for contact in self.contacts:
            c = Contact()
            c.name = contact['name']
            c.extra = contact['email']
            c.address = contact['email']
            contact_list.append(c)
        return contact_list
                    
    def send_invitations(self, to=None, subject=None, message=None):
        to = to or self.send_to
        message = message or self.message
        send_email_invitations(to, subject, message)
        
        