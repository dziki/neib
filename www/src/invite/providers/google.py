# -*- coding: utf-8 -*-
from base import BaseContactProvider, is_valid_email, BadAuthorizationError, Contact, send_email_invitations
import gdata.contacts
import gdata.contacts.service

class GoogleContactProvider(BaseContactProvider):     
    token = property(lambda self: self.client.GetClientLoginToken())
    user_name = None
    _contact_list = []
    
    def authenticate(self, login, passwd):
        '''
        Authorizes user at google and receives token for single 
        transaction
        '''
        try:
            client = gdata.contacts.service.ContactsService()
            client.email = login
            client.password = passwd
            client.source = "neib.org"
            client.ProgrammaticLogin()
            
            self.client = client
            
            return True
        except gdata.service.Error, message:
            raise BadAuthorizationError(message)
        
    
    def auth_by_token(self,token):
        self.client = gdata.contacts.service.ContactsService()
        self.client.SetClientLoginToken(token)
    
    def get_all_contacts(self):
        if self._contact_list: return self._contact_list
        
        for entry in self._bulk_contacts_generator(100):
            for email in entry.email:
                if is_valid_email(email.address):
                    self._contact_list.append(Contact(entry.title.text, 
                                                email.address, 
                                                extra=email.address))
        
        return self._contact_list

    def _bulk_contacts_generator(self, size):
        offset = 1
        while True: 
            query = gdata.contacts.service.ContactsQuery()
            query.max_results = size
            query.start_index = offset
            offset = offset + size
            feed = self.client.GetContactsFeed(query.ToUri())
            if len(feed.entry) < 1:
                return 
            else:
                for entry in feed.entry:
                    yield entry
                    
    def send_invitations(self, to=None, subject=None,  message=None):
        to = to or self.send_to
        message = message or self.message
        send_email_invitations(to, subject, message)
    