#from django.forms.fields import email_re
from django.core.mail import EmailMultiAlternatives, get_connection
from django.template.loader import get_template
from django.template import Context


class BaseContactProvider(object):
    send_to = None
    #List of contacts that the invitation message will be send to
    message = None
    #Content of the invitation message
    
    def authenticate(self, login, passwd):
        # Authenticates user.
        # Login could be an e-mail in some cases.
        # The function should return True if login was successful
        raise NotImplementedError
    
    def get_all_contacts(self):
        """Retrieve contacts as list of Contact objects"""
        raise NotImplementedError
    
    def send_invitations(self, to=None, subject=None, message=None):
        """Method for sending invitations suitable for this provider"""
        raise NotImplementedError


class Contact(object):
    name = None;
    # Name of the contact in any form (nick name, first name, last name, or mixture.
    # This field is what is printed next to checkbox in the form later on"""
    address = None;
    # Address suitable to the provider. Could be email or user id. This field is
    # used later on as the address at which the invitation is sent

    avatar = None;
    # Link to a picture of the person"""
    extra = None;
    # Some extra information. Used to ease the identification of contact.
    # It could be email that gives the idea for which company the person works"""
    
    def __init__(self, name=None, address=None, avatar=None,extra=None):
        self.name = name
        self.address = address
        self.avatar = avatar
        self.extra = extra

def is_valid_email(email):
    return True #if email_re.match(email) else False
       
def send_email_invitations(to, subject, message):
    if not to:
        raise ValueError, "Invalid list of contacts"
    if not message:
        raise ValueError, "Invalid invitation message"
    
    to = _validate_emails(to)
    
    html_template = get_template('invite/html_message.html')
    text_template = get_template('invite/text_message.txt')
    context = Context({'message':message})
    
    subject = subject
    mail_from = 'webmaster@neib.org'
    
    text_content = text_template.render(context)
    html_content = html_template.render(context)
    
    messages = []
    for email in to:
        messages.append(_create_email(subject, mail_from, email, text_content, html_content))
        
    conn = get_connection(fail_silently=True)
    conn.send_messages(messages)


def _create_email(subject, from_email, to, text_content, html_content):
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    return msg

def _validate_emails(list):
    return filter(is_valid_email, list)

        
# Error classes
class ContactProviderError(Exception):
    '''Base for errors of ContactsProvicer's'''
    pass

class BadAuthorizationError(ContactProviderError):
    '''
    Exception raised when authentication was unsuccessful
    ie. incorrect login or password
    '''
    pass
        
class ConnectionError(ContactProviderError):
    '''
    Exception raised when there where problems with connecting
    to contact provider
    '''
    pass