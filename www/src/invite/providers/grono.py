# -*- coding: utf-8 -*- 
'''
Created on 2009-11-13

@author: kalin
'''
from base import BaseContactProvider, BadAuthorizationError, Contact
from grono_api import Grono,GronoError
from ..models import GronoUser,GronoInvitation,GronoMessage

class GronoContactProvider(BaseContactProvider):
    def __init__(self):
        self.grono = Grono()
    
    def authenticate(self, login, passwd):
        try:
            self.grono.login(login, passwd)
        except GronoError, msg:
            raise BadAuthorizationError(msg)
        
    def get_all_contacts(self):
        friends = self.grono.friend_list()
        contacts = []
        for friend in friends:
            c = Contact()
            c.address = friend['id']
            c.avatar = friend['avatar_url']
            c.name = friend['scrname']
            #przydatne ale grono wprowadza ograniczenie do 100 zapytan na minute
            #co uniemożliwia użycie tej opcji
            #friend = self.grono.profile(friend['id'])
            #if 'name' in friend and 'surname' in friend:
            #    c.extra = "%s %s" % (friend['name'] or "",friend['surname'] or "")
            contacts.append(c)
        return contacts
        
                    
    def send_invitations(self, to=None, subject=None, message=None):
        to = to or self.send_to
        message = message or self.message
        if not to:
            raise ValueError, "Invalid list of contacts"
        if not message:
            raise ValueError, "Invalid invitation message"
        
        user = GronoUser.objects.filter(pk = self.grono.my_id())
        user = user and user[0] or None # if user not found return None else return user 
        if user is None:
            id = self.grono.my_id()
            user = GronoUser(pk=id, code=self.grono.cook, name=self.grono.my_profile()['scrname'])
            user.save()
        
        msg = GronoMessage(subject=subject,body=message)
        msg.save()
        
        for contact in to:
            inv = GronoInvitation(to=contact,message=msg, user=user)
            inv.save()