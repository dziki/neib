'''
Created on 2009-11-21

@author: kalin
'''
from django.contrib import admin
from invite.models import GronoInvitation,GronoMessage,GronoUser

class GronoUserAdmin(admin.ModelAdmin):
    list_display = ('gronoId','name','code',)


admin.site.register(GronoMessage)
admin.site.register(GronoUser,GronoUserAdmin)
admin.site.register(GronoInvitation)
