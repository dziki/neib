# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response as dj_render2resp, redirect
from django.template.context import RequestContext
from django.template.loader import render_to_string
from invite.forms import LoginForm, MessageForm
from invite.providers import GoogleContactProvider, BadAuthorizationError, GronoContactProvider
from invite.providers import WielokontContactProvider

from django.http import Http404

class wielokont(object):
    def __init__(self, domain):
        self.domain = domain

    def __call__(self):
        w = WielokontContactProvider()
        w.domain = self.domain
        return w

SESSION_PROVIDER = 'invitation_provider'

CONTACTS_PROVIDERS = {'google': GoogleContactProvider,
                      'grono':  GronoContactProvider,
                      'amorki.pl': wielokont('amorki.pl'),
                      'autograf.pl': wielokont('autograf.pl'),
                      'buziaczek.pl': wielokont('buziaczek.pl'),
                      'gazeta.pl': wielokont('gazeta.pl'),
                      #'gmail.com': wielokont('gmail.com'), #zostawiam dla celow testowych
                      'go2.pl': wielokont('go2.pl'),
                      'interia.pl': wielokont('interia.pl'),
                      'neostrada.pl': wielokont('neostrada.pl'),
                      'o2.pl': wielokont('o2.pl'),
                      'onet.eu': wielokont('onet.eu'),
                      'op.pl': wielokont('op.pl'),
                      'poczta.fm': wielokont('poczta.fm'),
                      'poczta.onet.eu': wielokont('poczta.onet.eu'),
                      'poczta.onet.pl': wielokont('poczta.onet.pl'),
                      'poczta.wp.pl': wielokont('poczta.wp.pl'),
                      'tlen.pl': wielokont('tlen.pl'),
                      'vp.pl': wielokont('vp.pl'),
                      'wp.pl': wielokont('wp.pl')
                      }

def render_to_response(request, template, params):
    return dj_render2resp(template, params, 
                          context_instance=RequestContext(request))

def auth(request, provider):
    errors = None
    if provider not in CONTACTS_PROVIDERS.keys():
        raise Http404
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            #log("Using provider: %s" % provider)
            provider_obj = CONTACTS_PROVIDERS[provider.strip()]()
            try: 
                provider_obj.authenticate(form.cleaned_data['login'],form.cleaned_data['password'])
                request.session[SESSION_PROVIDER] = provider_obj
                return redirect('invitation_contacts')
            except BadAuthorizationError, message:
                errors = message
    else:
        form = LoginForm()

    return render_to_response(request, 'invite/auth.html', {
        'form': form,
        'errors': errors,
    })    
    
def contacts(request):
    provider = request.session[SESSION_PROVIDER]
    params = dict(request.POST.lists())
    if request.method == 'POST' and params.has_key('contacts') and params['contacts']:
        provider.send_to = params['contacts']
        request.session[SESSION_PROVIDER] = provider
        return redirect("invitation_message")
    else:
        contacts = provider.get_all_contacts()
        error = None
        if request.method == 'POST' and not params.has_key('contacts'):
            error = "Nie wybrano żadnych kontaktów"
        return render_to_response(request, 'invite/contacts.html', {
            'contacts': contacts,
            'error':error
        }) 
        
def cleanup(request):
    del request.session[SESSION_PROVIDER]  
    return render_to_response(request, 'invite/done.html', {
    })

def message(request):
    provider = request.session[SESSION_PROVIDER]
    if request.method == 'POST':
        form = MessageForm(request.POST)
        if form.is_valid(): 
            provider.send_invitations(subject=form.cleaned_data['subject'],
                                      message=form.cleaned_data['message'])
            return cleanup(request)
    else:
        msg = render_to_string('invite/default_message.txt',{})
        form = MessageForm(initial={'subject':'Zaproszenie do neib.org', 
                                    'message': msg})

    return render_to_response(request, 'invite/message.html', {
        'choice': provider.send_to,
        'form': form,
    })
    
