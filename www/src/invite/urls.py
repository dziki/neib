from django.conf.urls.defaults import patterns, url
from django.views.generic.simple import direct_to_template

from django.contrib import admin
admin.autodiscover()

from django.utils.translation import ugettext_lazy as _

urlpatterns = patterns('',
    url((r'^$'), direct_to_template, {'template':'invite/main.html'}, name='invitation_main' ),
    url((r'contacts/$'), 'invite.views.contacts', name='invitation_contacts'),
    url((r'edit-message/$'), 'invite.views.message', name='invitation_message'),
    url((r'sent/$'), 'invite.views.cleanup', name='invitation_done' ),
    url((r'(?P<provider>[\w | \.]+)/$'), 'invite.views.auth', name='invitation_auth'),
)
