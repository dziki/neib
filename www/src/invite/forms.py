# -*- coding: utf-8 -*-
'''
Created on 2009-10-14

@author: Adam Kalinski
'''

from django import forms
from invite.widget import ContactListWidget 


class LoginForm(forms.Form):
    '''
    Default form used for loging in
    '''
    login = forms.CharField(required=True, label="Login poczty")
    password = forms.CharField(widget=forms.PasswordInput(), label="Hasło poczty",
                                      required=True)

class MessageForm(forms.Form):
    '''
    Form for modifying the invitation message
    '''
    subject = forms.CharField(required=True, label=u"Tytuł")
    message = forms.CharField(widget=forms.Textarea(),
                              required=True, 
                              label=u"Treść zaproszenia",
                              help_text=u"""Wiadomość, którą otrzymają Twoi znajomi od
                              nas jako zaposzenie""")

def contacts_form_factory(choices):
    '''
    Factory generating form with only one MultipleChoiceField but with provided choices
    that are a list of contacts as tuples
    '''
    contacts = forms.MultipleChoiceField(choices=choices, widget=ContactListWidget())
    return type('ContactsForm', (forms.Form,), {'contacts':contacts})