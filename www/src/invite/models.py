from django.db import models

#Grono models used for late message sending 
class GronoUser(models.Model):
    code = models.CharField(blank=False, max_length=100)
    name = models.CharField(blank=True, max_length=20)
    
    def gronoId(self):
        return self.pk
    
class GronoMessage(models.Model):
    subject = models.CharField(blank=False, max_length=20)
    body = models.TextField(blank=False)
    
class GronoInvitation(models.Model):
    to = models.CharField(blank=False, max_length=20)
    message = models.ForeignKey(GronoMessage)
    user = models.ForeignKey(GronoUser)
    created_on = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        get_latest_by = 'created_on'