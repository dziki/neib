# -*- coding: utf-8 -*- 
"""
URLConf for neib project

Currently all urls are stored here.

"""

from django.conf.urls.defaults import patterns, include
from django.contrib import admin
from django.views.generic.simple import redirect_to

from business.views import *
from community.views import *
from gallery.views import *
from chunks.views import generic_page
from common.views import *
from location.views import home_city

from main.views import *

from django.contrib.sitemaps import GenericSitemap

from badges.models import BADGES

from django.contrib.auth import views as auth_views

#business_dict = {
#    'queryset': Business.objects.all(),
#    'date_field': 'date',
#}
#user_dict = {
#    'queryset': Profile.objects.all(),
#    'date_field': 'date',
#}

#sitemaps = {
#    'businesses': GenericSitemap(business_dict, priority=0.6),
#    'users': GenericSitemap(user_dict, priority=0.6),
#}

admin.autodiscover()

urlpatterns = patterns('django.views.generic.simple',
    ((r'^profile/edytuj/$'), 'direct_to_template', {'template': 'community/profile.edit.choose.html'}, 'profile_edit'),
    ((r'^xd_receiver.htm$'), 'direct_to_template', {'template': 'misc/xd_receiver.htm'}),
    ((r'^test.html$'), 'direct_to_template', {'template': 'tmp/test.html'}),
    ((r'^contest/terms/$'), 'direct_to_template', {'template': 'common/conditions.html'}, 'competition_conditions'),
    ((r'^googlehostedservice.html$'), 'direct_to_template', {'template': 'googlehostedservice.html'}, 'google_hosted_service'),
    ((r'^google24afc633668495b8.html$'), 'direct_to_template', {'template': 'google24afc633668495b8.html'}, 'google_hosted_service'),
)

if 0:
    urlpatterns += patterns('',
        (r"static/(?P<path>.*)$", "django.views.static.serve", {
                "document_root": settings.MEDIA_ROOT,
        }),
    )

urlpatterns += patterns('',
    # REDIRECTY
    ((r'^profil/(?P<username>[-\w]+)/aktualnosci/$'), redirect_to, {'url': '/profile/%(username)s/news/', 'permanent': True}),
    ((r'^profil/(?P<username>[-\w]+)/recenzje/$'), redirect_to, {'url': '/profile/%(username)s/reviews/', 'permanent': True}),
    ((r'^profil/(?P<username>[-\w]+)/znajomi/$'), redirect_to, {'url': '/profile/%(username)s/friends/', 'permanent': True}),
    ((r'^profil/(?P<username>[-\w]+)/ulubione/$'), redirect_to, {'url': '/profile/%(username)s/favorites/', 'permanent': True}),
    ((r'^komplementy/(?P<username>[-\w]+)/lista/$'), redirect_to, {'url': '/compliments/%(username)s/list/', 'permanent': True}),
    ((r'^odznaczenia/(?P<id>[-\w]+)/$'), redirect_to, {'url': '/badges/%(id)s/', 'permanent': True}),
    ((r'^recenzja/(?P<id>[-\w]+)/spam/$'), redirect_to, {'url': '/review/%(id)s/spam/', 'permanent': True}),
    ((r'^odznaczenia/$'), redirect_to, {'url': '/badges/', 'permanent': True}),
    ((r'^onas/$'), redirect_to, {'url': '/about/', 'permanent': True}),
    ((r'^partnerzy/$'), redirect_to, {'url': '/partners/', 'permanent': True}),
    ((r'^wydarzenia/$'), redirect_to, {'url': '/events/', 'permanent': True}),
    ((r'^wydarzenia/dodaj/$'), redirect_to, {'url': '/events/add/', 'permanent': True}),
    ((r'^strona/(?P<id>[-\w]+)/$'), redirect_to, {'url': '/page/%(id)s/', 'permanent': True}),
    ((r'^wydarzenia/(?P<slug>[-\w]+)/$'), redirect_to, {'url': '/events/%(slug)s/', 'permanent': True}),
    ((r'^biz/mapa/(?P<slug>[-\w]+)/$'), redirect_to, {'url': '/biz/map/%(slug)s/', 'permanent': True}),
    ((r'^galeria/(?P<slug>[-\w]+)/$'), redirect_to, {'url': '/gallery/%(slug)s/', 'permanent': True}),
    ((r'^galeria/(?P<slug>[-\w]+)/dodaj/$'), redirect_to, {'url': '/gallery/%(slug)s/add/', 'permanent': True}),
    ((r'^szukaj/$'), redirect_to, {'url': '/search/', 'permanent': True}),
    ((r'^forum/kategoria/(?P<slug>[-\w]+)/$'), redirect_to, {'url': '/forum/c/%(slug)s/', 'permanent': True}),
    ((r'^forum/category/(?P<slug>[-\w]+)/$'), redirect_to, {'url': '/forum/c/%(slug)s/', 'permanent': True}),
    ((r'^kategoria/(?P<slug>[-\w]+)/$'), redirect_to, {'url': '/category/%(slug)s/', 'permanent': True}),
    ((r'^biz/nowy/$'), redirect_to, {'url': '/biz/add/', 'permanent': True}),
    ((r'^start/szukaj/$'), redirect_to, {'url': '/start/search/', 'permanent': True}),
)

urlpatterns += patterns('',
    # REDIRECTY
    ((r'^users/(?P<username>[-\w]+)/$'), redirect_to, {'url': '/profil/%(username)s/'}),
    ((r'^biz/krzyz-pod-paacem-prezydenckim-warszawa/$'), redirect_to, {'url': '/biz/krzyz-pod-palacem-prezydenckim-warszawa/'}),
    ((r'^page/recruit/$'), redirect_to, {'url': '/page/interhsip/', 'permanent': True}),
    ((r'^recruit/$'), redirect_to, {'url': '/page/internship/', 'permanent': True}),
    ((r'^praktyki/$'), redirect_to, {'url': '/page/internship/', 'permanent': True}),
    ((r'^internship/$'), redirect_to, {'url': '/page/internship/', 'permanent': True}),
    ((r'^category/(?P<category_name>[-\w]+)/$'), redirect_to, {'url': '/k/warszawa/%(category_name)s/', 'permanent': True}),
    ((r'^api/$'), redirect_to, {'url': 'http://code.google.com/p/dood-api/wiki/HowItWorks'}),

    ((r'^accounts/login/$'), auth_views.login, {'template_name': 'registration/login.html'}, 'auth_login'),

    # main
    ((r'^$'), home_search, {}, 'home'),
    ((r'^start/$'), home, {}, 'start'),
    ((r'^start/search/$'), home_search, {}, 'home_search'),
    ((r'^about/$'), about_us, {}, 'about_us'),

    # static page urls
    ((r'^mobile/emulator/$'), iphone_emulator, {}, 'iphone_emulator'),
    ((r'^page/(?P<page_id>[-\w]+)/$'), generic_page, {}, 'generic_page'),
    ((r'^robots.txt$'), robots_txt, {}, 'robots.txt'),
    ((r'^500error.html$'), error500test, {}, 'error500test'),
    ((r'^dynamicmedia/category.widget.js$'), category_widget_js, {}, 'dynamic_media'),
    ((r'^dynamicmedia/(?P<file>.+\..+)$'), dynamic_media, {}, 'dynamic_media'),
#    ((r'^sitemap.xml$'), 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),

    # BIZNES URLS W Z�YM MIEJSCU!
    ((r'^search/(?P<category_name>[-\w]+)/$'), business_search, {}, 'business_search_category'),
    ((r'^search/$'), business_search, {'category_name': 0}, 'business_search'),
    ((r'^k/(?P<city_slug>[-\w]+)/(?P<category_id>[-\d]+)/$'), business_city_category_old, {}, 'category_city_listing_old'),
    ((r'^k/(?P<city_slug>[-\w]+)/(?P<category_name>[-\w]+)/$'), business_city_category, {}, 'category_city_listing'),

    ((r'^faq/$'), 'businessownersguide.views.faq', {'group': 'use(r', 'template': 'faq/faq.html'}, 'faq'),

    # urlsy aplikacji alfabetycznie :)
#    ((r'^blog/'), include('articles.urls')),
    ((r'^api/'), include('api2.urls')),
    ((r'^biz/'), include('business.urls')),
#    ((r'^captcha/'), include('captcha.urls')),
    ((r'^feedback/'), include('feedback.urls')),
    ((r'^gallery/'), include('gallery.urls')),
    ((r'^inline-admin/'), include('inlineadmin.urls')),
    ((r'^json/'), include('api.urls')),
    ((r'^account/'), include('registration.urls')),
    ((r'^compliments/'), include('compliments.urls')),
    ((r'^newsletter/'), include('newsletter.urls')),
    ((r'^badges/'), include('badges.urls')),
    ((r'^partners/$'), include('partners.urls')),
    ((r'^notification/'), include('notification.urls')),
    ((r'^profile/'), include('community.urls')),
    ((r'^forum/'), include('forum.urls')),
    ((r'^mobile/'), include('mobile.urls')),
#    ((r'^rss/'), include('rss.urls')),
    ((r'^socialregistration/'), include('socialregistration.urls')),
    ((r'^messages/'), include('messages.urls')),
    ((r'^owner/'), include('ownership.urls')),
    ((r'^owner/guide/'), include('businessownersguide.urls')),
    ((r'^events/'), include('event.urls')),
    ((r'^invite/'), include('invite.urls')),
    ((r'^w/'), include('website.urls')),

    (r'^rosetta/', include('rosetta.urls')),    
    # admin urls
    #((r'^admin/doc/'), include('django.contrib.admindocs.urls')),
    ((r'^adm/'), include(admin.site.urls)),
    #((r'^panel/'), include('panel.urls')),
    #((r'^neibadmin/'), include('doodadmin.urls')),
    
    ((r'^'), include('location.urls')),

    ((r'^toolbox/category_tag_add.html$'),category_tag_chooser, {}),       
)

try:
    from localurls import local_patterns
    urlpatterns += local_patterns
except ImportError:
    pass
