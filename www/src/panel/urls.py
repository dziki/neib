"""
URLConf for neib project

Currently all urls are stored here.
TODO: separate urlconf for each application

"""

from django.conf.urls.defaults import *
from django.contrib import admin
from django.views.generic.simple import redirect_to

from panel.businesses.views import *


admin.autodiscover()

urlpatterns = patterns('',
    (r'^business/verify/$', business_verify, {}, 'business_verify'),
)