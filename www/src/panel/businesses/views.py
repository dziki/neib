"""
Views of photos and galleries. Also allows users to add files to gallery.

"""

from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.template import RequestContext

from business.models import *
from dynamicforms.forms import *
from business.forms import *

from django.forms.formsets import formset_factory
from business.forms import HoursForm

@login_required()
def business_verify(request):
    HoursFormSet = formset_factory(HoursForm, extra=7)
    saved = True

    if request.POST:
        business = Business.objects.get(id=request.POST['id'])
        form1 = BusinessForm(request.POST, instance = Business.objects.get(id=request.POST['id']))
        form2 = DynamicForm(InfoField, InfoFieldValue, instance = business) # Create the form
        formset = HoursFormSet(request.POST, prefix='form')

        form2.set_data(request.POST)
        if form1.is_valid() and formset.is_valid():
            for category in form1.cleaned_data["categories"]:
                form1.instance.categories.add(category)

            form1.save()
            if request.POST['submit'] == 'Nie odbiera':
                form1.instance.verified = 2
                form1.instance.save()

            Hours.objects.filter(business=business).delete()
            i=0
            for form in formset.forms:
                i = i+1
                if not form.cleaned_data['opened']:
                    form.instance.time_from = form.instance.time_to
                form.instance.business = business
                form.instance.day = i
                form.save()
        else:
            saved = False

    if saved:
        business = Business.objects.filter(verified = 0, phone__startswith = '22', id__lte = 1550)[0]
        form1 = BusinessForm(instance = business)
        formset = HoursFormSet()
        form2 = DynamicForm(InfoField, InfoFieldValue, instance = business) # Create the form

    return render_to_response('panel/businesses/verify.html',
        {"id": business.id,
         "phone": business.phone,
         "form1": form1,
         "form2": form2,
         "formset": formset, },
        context_instance=RequestContext(request))