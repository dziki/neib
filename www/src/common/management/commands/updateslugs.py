# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from business.models import Category
from common.string import unique_slug

import logging as log

log.basicConfig(level=log.INFO)

class Command(BaseCommand):
# DROP TABLE `business_fitting`
# CREATE TABLE `business_fitting` SELECT * FROM `business_fitting_view` WHERE 1
    args = '[]'
    help = 'Creates fitting cache table'
    
    option_list = BaseCommand.option_list

    def handle(self, *args, **options):
        cats = Category.objects.all()
        
        for cat in cats:
            slug = ''
            if cat.parent:
                slug += cat.parent.name + ' '
            slug += cat.name
            
            cat.slug = unique_slug(Category, 'slug', slug)
#            cat.save()
        return 