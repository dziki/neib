from django.template.defaultfilters import slugify

def unique_slug(model, slug_field, slug_value):
    orig_slug = slug = slugify(slug_value)
    
    index = 0
    
    while True:
        try:
            model.objects.get(**{slug_field: slug})
            index += 1
            slug = orig_slug + '-' + str(index)
        except model.DoesNotExist:
            return slug

