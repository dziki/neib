"""
View for main site

Lists almost all tables to use in main template

"""

from datetime import datetime

from django.shortcuts import render_to_response
from django.template import RequestContext

from django.db.models import Count
from django.contrib.auth.models import User

def iphone_emulator(request):
    return render_to_response('common/iphone-emulator.html',
        context_instance=RequestContext(request,
        {}))
    
def competition(request):
    start_date = (datetime(2010, 7, 19, 0, 0, 0)).date()
    users = User.objects.all().filter(review__date__gte = start_date).annotate(num_of_flags=Count('review__flags')).order_by('-num_of_flags')[:10]

    return render_to_response('common/competition.html',
        context_instance=RequestContext(request,
        {'users': users}))