# -*- coding: utf-8 -*- 

import re
from django.utils.html import strip_spaces_between_tags
class MinifyHTMLMiddleware(object):
    #noinspection PyUnusedLocal
    def process_response(self, request, response):
        if response['content-type'][:9] == 'text/html':
            response.content = strip_spaces_between_tags(response.content.strip())
        return response
    
class StripCookieMiddleware(object):
    STRIP_RE = re.compile(r'\b(_[^=]+=.+?(?:; |$))')

    def process_request(self, request):
        cookie = self.STRIP_RE.sub('', request.META.get('HTTP_COOKIE', ''))
        request.META['HTTP_COOKIE'] = cookie
        
class CsrfFuckMiddleware(object):
    #noinspection PyUnusedLocal
    def process_view(self, request, callback, callback_args, callback_kwargs):
        setattr(request, '_dont_enforce_csrf_checks', True)