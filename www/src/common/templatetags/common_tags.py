from django import template
from django.template.loader import render_to_string

import settings
import inspect
from common.helpers import generate_media_url


register = template.Library()

@register.filter
def removenewline(text):
    return text.replace("\n", " ")

@register.filter
def ADD_MEDIA_URL(value):
    host = generate_media_url(value)
    return host
ADD_MEDIA_URL.is_safe = True


def get_image_by_id(id, type="thumbnail"):
    """
    Method returns link to image of business with given id
    """
    return "%sphotos/%d.%s.jpg" % (settings.MEDIA_URL, id, type)

@register.simple_tag
def biz_thumbnail(id):
    return get_image_by_id(id)

@register.simple_tag
def biz_thumbnail_js(id):
    """
    Same as biz_thumbnail(id) but accepts string for id which is useful when
    using java script templates
    """
    type="thumbnail"
    return "%sphotos/%s.%s.jpg" % (settings.MEDIA_URL, id, type)

def get_generic_sidebar_template(obj):
    m = inspect.getmodule(obj)
    package = m.__package__
    model = obj.__class__.__name__.lower()
    return "%s/%s.sidebar.html" % (package, model)

def get_sidebar_template(obj):
    """
    Returns sidebar template for the model of the object. The template is taken
    from get_sidebar_template of the model or is assumed that the template will
    be placed in {package_name}/{model_name}.sidebar.html
    """    
    if 'get_sidebar_template' in dict(inspect.getmembers(obj.__class__)):
        return obj.get_sidebar_template()
    else:
        return get_generic_sidebar_template(obj) 
    

@register.simple_tag
def sidebar(obj):
    return render_to_string(get_sidebar_template(obj), \
                            dict(((obj.__class__.__name__.lower(),obj),)))    
    