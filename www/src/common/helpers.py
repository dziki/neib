def media_checksum(st):
    return 1 + (reduce(lambda x,y:x+y, map(ord, st)) % 5)

def generate_media_url(resource):
    host = "http://static" + media_checksum(resource).__str__() + ".lemonet.eu/"
    str = host + resource
    return str
