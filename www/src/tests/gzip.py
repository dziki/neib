import urllib2, gzip, StringIO

__author__ = "Mark Pilgrim (mark@diveintomark.org)"
__license__ = "Python"

def get(uri):
    request = urllib2.Request(uri)
    request.add_header("Accept-encoding", "gzip")
    usock = urllib2.urlopen(request)
    data = usock.read()
    return data

#s1 = get("http://neib.org/json/business/list")
#s2 = get("http://neib.org/json/session/start")
s1 = get("http://localhost:8000/json/business/list")
s2 = get("http://localhost:8000/json/session/start")

print "business/list"
print s1

print "session/start"
print s2

