#!/usr/bin/env python
# -*- coding: utf-8 -*- 
from django.db.models import signals
from django.conf import settings

if "notification" in settings.INSTALLED_APPS:
    from notification import models as notification

    #noinspection PyUnusedLocal
    def create_notice_types(app, created_models, verbosity, **kwargs):
        notification.create_notice_type("community_friend_invite", (u"Nowe zaproszenie"), (u"masz nowe zaproszenie do znajomych"), default=2)
        notification.create_notice_type("community_friend_accept", (u"Zaproszenie zaakceptowane"), (u"Twoje zaproszenie zostało przyjęte"), default=2)
        notification.create_notice_type("community_follower", (u"Dodanie do obserwowanych"), ("ktoś dodał Cię do obserwowanych"), default=2)

    signals.post_syncdb.connect(create_notice_types, sender=notification)
else:
    print "Skipping creation of NoticeTypes as notification app not found"
