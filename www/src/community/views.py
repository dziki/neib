# -*- coding: utf-8 -*-
"""
Views of community functionality:
- show user profile
- edit profile
- adding and setting locations
- friends etc.

- JSON views with the same functionality as above

"""

from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect

from dynamicforms.forms import DynamicForm
from django.contrib.auth.decorators import login_required

from community.forms import *
from business.models import Review, Business, BusinessList
from flagging.models import Flag, flag_translator, get_flagged_objects

from compliments.context_processors import compliment_types_list_processor
from compliments.models import Compliment

from forum.models import Post
from forum.models import add_comments

from notify.models import notify_user

@login_required()
def profile_details_edit(request):
    """
    Gives form to edit profile data

    **Required arguments**

    None

    **Context:**

    ``form``
        Profile edit form

    **Template:**

    community/profile.edit.html.

    """
    try:
        profile = Profile.objects.get(user=request.user)
    except Profile.DoesNotExist:
        profile = Profile()

    if request.POST:
        user_form = UserProfileForm(request.POST, instance=request.user)
        if user_form.is_valid():
            user_form.instance.username = user_form.cleaned_data['email']
            user_form.save()

        form = ProfileForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
    else:
        user_form = UserProfileForm(instance=request.user)
        form = ProfileForm(instance=profile)

    return render_to_response('community/profile.edit.html',
                              {"form": form,
                               "user_form": user_form,},
                               context_instance=RequestContext(request))

@login_required()
def profile_info_edit(request):
    """


    """
    form = DynamicForm(InfoField, InfoFieldValue, instance=request.user.get_profile()) # Create the form

    if request.POST:
        form.set_data(request.POST)


    return render_to_response('community/profile.edit.html',
                              {"form": form,},
                               context_instance=RequestContext(request))

@login_required()
def profile_preferences_edit(request):
    """


    """
    # Creating the form object and manipulating/validating it

    form = DynamicForm(PreferencesField, PreferencesFieldValue, instance=request.user.get_profile()) # Create the form

    if request.POST:
        form.set_data(request.POST)

    return render_to_response('community/profile.preferences.edit.html',
                              {"form": form,},
                               context_instance=RequestContext(request))

def location_list(request):
    locations = Location.objects.all().filter(user = request.user)

    return render_to_response('community/location.list.html',
        context_instance=RequestContext(request, {"locations": locations}))

@login_required()
def location_add(request):
    if request.POST:
        form = LocationForm(request.POST)
        if form.is_valid():
            form.instance.user = request.user
            form.save()
            return location_list(request)
    else:
        form = LocationForm()

    return render_to_response('community/location.add.html',
        context_instance=RequestContext(request, {'form': form}))

@login_required()
def location_edit(request, location_id):
    if request.POST:
        location = Location.objects.get(id__exact = request.POST['location_id'])
        form = LocationForm(request.POST, instance=location)
        if form.is_valid():
            form.save()
            return location_list(request)

    try:
        location = Location.objects.get(id__exact = location_id, user = request.user)
        form = LocationForm(instance = location)
    except Location.DoesNotExist:
        return location_list(request)

    return render_to_response('community/location.add.html',
        context_instance=RequestContext(request, {"form": form}))

@login_required()
def location_delete(request, location_id):
    try:
        location = Location.objects.get(id__exact = location_id, user = request.user)
        location.delete()
    except Location.DoesNotExist:
        pass

    return location_list(request)

@login_required()
def location_activate(request, location_id):
    try:
        location = Location.objects.get(id__exact = location_id, user = request.user)
        request.session['location'] = location
    except Location.DoesNotExist:
        pass

    return profile(request)

@login_required()
def password_edit(request):
    if request.POST:
        form = ChangePasswordForm(request.POST)
        if form.is_valid() and form.cleaned_data['pw1'] == form.cleaned_data['pw2']:
            request.user.set_password(form.cleaned_data['pw1'])
            request.user.save()
    else:
        form = ChangePasswordForm()

    return render_to_response('community/password.edit.html',
        context_instance=RequestContext(request, {"form": form}))

def profile_logged(request):

    return HttpResponseRedirect(reverse(request.GET.get('next')))

def profile(request, username=None):
    """
    Shows users profile

    **Required arguments**

    None

    **Optional arguments**

    ``username``
       User of whome we need profile.

    **Context:**

    ``profile``
        profile structure with additional user data
        (profile attached to given user)

    **Template:**

    community/profile.html.

    """
    if username is None:
        username = request.user.id

    # TODO: getting user by name not by id
    user_id = username
    reviews = Review.objects.select_related('user', 'business__gallery__representative')
    reviews = reviews.order_by("-userlang","-id").filter(user__id__exact=user_id).filter(active=True)[:10]

    business_list = BusinessList.objects.all().filter(user__id__exact = user_id)

    user = User.objects.select_related('profile__gallery__representative').get(id__exact=user_id)

    friends = Friendship.objects.get_friends(user)
    compliments = Compliment.objects.filter(recipient=user)[:3]

    add_comments(reviews)


    return render_to_response('community/profile.html',
        context_instance=RequestContext(request, {"reviews": reviews,
        "friends": friends,
        "compliments": compliments,
        "business_list" : business_list,
        "selecteduser": user,
        }, [compliment_types_list_processor]))

@login_required()
def profile_friendship_add(request, username=None):
    user_id = username
    if user_id is not None:
        user = User.objects.get(id=user_id)
        f = Friendship(user1=request.user, user2=user, accepted = False)
        f.save()

    return profile(request, username)

@login_required()
def profile_flag_add(request, username, flag):
    user_id = username
    user = User.objects.get(id = user_id)
    if (user != request.user):
        flag_obj = Flag(content_object=user, flag=flag_translator(flag), user=request.user)
        flag_obj.save()

    return profile(request, username)

def reviews_list(request, username):
    """

    """
    user_id = username
    user = User.objects.get(id=user_id)

    reviews = Review.objects.select_related('user', 'business__gallery__representative')
    reviews = reviews.order_by("-userlang","-id").filter(user__id__exact=user_id).filter(active=True)

    return render_to_response('community/reviews.html',
        context_instance=RequestContext(request,
            {"reviews": reviews,
             "selecteduser": user
             }, [compliment_types_list_processor]))

def friends_list(request, username):
    """

    """
    user_id = username
    user = User.objects.get(id=user_id)
    friends = Friendship.objects.get_friends(user)

    return render_to_response('community/friends.html',
        context_instance=RequestContext(request,
            {"friends": friends,
             "selecteduser": user
             }, [compliment_types_list_processor]))

def favorites_list(request, username):
    user_id = username
    user = User.objects.get(id=user_id)

    businesses = get_flagged_objects('ulubiony', user, Business)
    users = get_flagged_objects('ulubiony', user, User)
    lists = get_flagged_objects('ulubiony', user, BusinessList)

    return render_to_response('community/favorites.html',
        context_instance=RequestContext(request,
            {"businesses": businesses,
             "users": users,
             "lists": lists,
             "favorites_user": user,
             "selecteduser": user
             }, [compliment_types_list_processor]))

def favorites_del(request, model, id):
    if model == 'lokal':
        item = Business.objects.get(id=id)
    elif model == 'uzytkownika':
        item = User.objects.get(id=id)
    else:
        raise Exception

    flag = Flag.objects.get(object_id=item.id, content_type=ContentType.objects.get_for_model(item), flag=flag_translator('ulubiony'), user=request.user)
    flag.delete()

    return favorites_list(request, request.user.id)

def business_lists(request, username):
    user_id = username
    user = User.objects.get(id=user_id)

    business_lists = BusinessList.objects.all().filter(user=user)

    return render_to_response('community/lists.html',
        context_instance=RequestContext(request,
            {"business_lists": business_lists,
             "selecteduser": user
             }, [compliment_types_list_processor]))


@login_required()
def friends_edit(request):
    """

    """
    friends = Friendship.objects.get_friends(request.user)

    rel_list = Friendship.objects.filter(user2 = request.user, accepted=False)
    friend_requests = list()
    for item in rel_list:
        friend_requests.append(item.user1)

    return render_to_response('community/friends.edit.html',
        context_instance=RequestContext(request,
            {"friends": friends,
             "friend_requests": friend_requests
             }))

@login_required()
def friendship_delete(request, friend_id):
    f = Friendship.objects.get_friendship(request.user, User.objects.get(id=friend_id))
    f.delete()
    notify_user(request, 'Zaproszenie odrzucone', None)

    return friends_edit(request)

@login_required()
def friendship_accept(request, friend_id):
    f = Friendship.objects.get_friendship(request.user, User.objects.get(id=friend_id))
    if (f.user2 == request.user):
        f.accepted = True
        f.save()

    return friends_edit(request)

@login_required()
def friend_invitation(request):
    if request.POST:
        form = InvitationForm(request.POST)
        if form.is_valid():
            form.instance.user = request.user
            form.save()
    else:
        form = InvitationForm()

    return render_to_response('community/invitation.html',
        context_instance=RequestContext(request, {"form": form}))

def elite_info(request):
    users = get_flagged_objects('elita', None, User)

    return render_to_response('community/elite_info.html',
        context_instance=RequestContext(request, {"users": users}))

