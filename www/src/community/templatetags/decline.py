# -*- coding: utf-8 -*- 
from django import template
from math import floor

register = template.Library()

DECLINE_DICT = dict()
DECLINE_DICT[u''] = ['', '']
DECLINE_DICT[u'komplement'] = [u'komplementy', u'komplementów']
DECLINE_DICT[u'wiadomść'] = [u'widomości', u'wiadomości']
DECLINE_DICT[u'znajomy'] = [u'znajomych', u'znajomych']
DECLINE_DICT[u'punkt'] = [u'punkty', u'punktów']
DECLINE_DICT[u'recenzja'] = [u'recenzje', u'recenzji']
DECLINE_DICT[u'ulubiony'] = [u'ulubione', u'ulubionych']
DECLINE_DICT[u'lokal'] = [u'lokale', u'lokali']
DECLINE_DICT[u'ulubiony lokal'] = [u'ulubione lokale', u'ulubionych lokali']
DECLINE_DICT[u'użytkownik'] = [u'użytkowników', u'użytkowników']
DECLINE_DICT[u'lista'] = [u'listy', u'list']
DECLINE_DICT[u'nową wiadomość'] = [u'nowe wiadomości', u'nowych wiadomości']
DECLINE_DICT[u'nowy komplement'] = [u'nowe komplementy', u'nowych komplementów']

@register.simple_tag
def decline(word, num):
    word_list = DECLINE_DICT.get(word, ['', ''])
    d = floor((num/10)%10)
    sel = word_list[1]
    if num == 1:
        sel = word
    if num >= 2 and num <= 4 and d != 1:
        sel = word_list[0]
    return num.__str__() + ' ' + sel