# -*- coding: utf-8 -*- 
"""
Models for community

"""
from django.core.mail import send_mail

from flagging.models import Flag
from date.models import TimedModel
from messages.models import inbox_count_for

from location.models import GeoModel

from compliments.models import Compliment

from notify.models import notify_user

from notification import models as notification

from django.db.models import Count, Sum
from dynamicforms.models import DynamicField, DynamicFieldValue

from django.contrib.gis.db import models as geomodels
from django.db import models
from django.contrib.auth.models import User
from gallery.models import GalleryField, Photo, Gallery
from community.managers import ProfileManager, FriendshipManager
from datetime import datetime
from django.db.models.signals import post_save

from django.utils.translation import ugettext_lazy as _

from contrib.optimization import keep_result
from denorm import denormalized, depend_on_related

GENDER_CHOICES = (
    ('M', _('Male')),
    ('F', _('Female')),
)

PRIVILEGES_CHOICES = (
    (0, _('Normal user')),
    (1, _('Editor')),
    (2, _('Modarator')),
)

class PreferencesGroup(models.Model):
    name = models.CharField(max_length=100)
    
    def __str__(self):
        return self.name

class PreferencesField(DynamicField):
    group = models.ForeignKey(PreferencesGroup)

class InfoField(DynamicField):
    pass

class PreferencesFieldValue(DynamicFieldValue):
    field = models.ForeignKey(PreferencesField)
    obj = models.ForeignKey("Profile")

# Create your models here.
class Profile(TimedModel):
    """ Additional user profile """
    user = models.OneToOneField(User, primary_key=True)
    supervisor = models.ForeignKey(User, null=True, related_name='interns')

    nickname = models.CharField(max_length=20, verbose_name = _("Nickname"), null=True, blank=True,)
    #birth_date = models.DateField(verbose_name = _("Birth date"), null=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, verbose_name = _("Gender"), default='M')

    motto = models.CharField(max_length=150, verbose_name = _("Motto"), null=True)

    city = models.CharField(max_length=150, verbose_name = _("City"), null=False)

    gallery = GalleryField("profile")

    registered = models.BooleanField(verbose_name = _("Registered"), default=True)

    fields = models.ManyToManyField(InfoField, through='InfoFieldValue')
#    preferences_fields = models.ManyToManyField(PreferencesField, through='PreferencesFieldValue')

    export_fields = ('name', 'surname', 'nickname', 'gallery_id', 'photo', 'num_of_friends', 'num_of_reviews', 'num_of_photos')
    export_fields_ex = ('gender',)
    
    is_active = None
    
    privileges = models.IntegerField(verbose_name=_("Permissions"), choices=PRIVILEGES_CHOICES, default=0)

    objects = ProfileManager()
    
    reputation = models.IntegerField(verbose_name=_("Reputation"), default=0)

    def version(self):
        return int(time.mktime(self.date.timetuple())).__str__()

    def id(self):
        return self.user.id
    
    def name(self):
        return self.user.first_name
    def surname(self):
        return self.user.last_name

    def info(self):
        return InfoFieldValue.objects.filter(obj__user__id = self.id)
    
    def fullname(self):
        name = self.user.first_name
        if self.nickname:
            if len(self.nickname) > 0:
                name += ' "' + self.nickname + '"'
        if len(self.user.last_name) > 0:
            name += ' ' + self.user.last_name
        if len(name) <= 1:
            return "Anonim"
        return name

    invited_tmp = None
    def invited_by(self):
        if self.invited_tmp is None:
            self.invited_tmp = Friendship.objects.invited_by(self.user)
        return self.invited_tmp

    def num_of_friends(self):
        return len(Friendship.objects.get_friends(self.user))

    def num_of_reviews(self):
        return self.user.review_set.count()

    def num_of_lists(self):
        return self.user.businesslist_set.count()

    def num_of_compliments(self):
        return Compliment.objects.filter(recipient=self).count()

    def num_of_new_compliments(self):
        return Compliment.objects.filter(recipient=self, read_at=None).count()

    def num_of_bookmarks(self):
        return Flag.objects.user_flaged_by(user=self.user, flag='bookmark').count()

    def num_of_biz_bookmarks(self):
        from business.models import Business
        return Flag.objects.user_flags(user=self.user, flag='bookmark', Model=Business).count()

    def num_of_bookmarked_by(self):
        return Flag.objects.get_flags(object=self.user, flag='bookmark').count()
    
    def num_of_new_messages(self):
        return inbox_count_for(self.user)

    def num_of_photos(self):
        return Photo.objects.num_of_gallery_photos(self.gallery)

    def photo(self):
        photo = Photo.objects.get_gallery_photo(self.gallery)
        if photo is None:
            photo = Photo()
            photo.disp = 'male'
            if self.gender == 'F':
                photo.disp = 'female'
        return photo

    def has_photo(self):
        photo = Photo.objects.get_gallery_photo(self.gallery)
        return photo

    def __str__(self):
        return self.fullname()
    
    def __unicode__(self):
        return self.__str__()

    @models.permalink
    def get_absolute_url(self):
        return ('profile', (), {
        'username': self.id})

    def get_user_edit_url(self):
        return '/profil/podstawowe/'

    def get_details_edit_url(self):
        return '/profil/szczegoly/'

    def get_friendship_add_url(self):
        return '/profil/%d/dodajznajomego/' % self.user.id

    def get_bookmark_url(self):
        return '/profil/%d/obserwuj/' % self.user.id

    def get_business_list_add_url(self):
        return '/bizlist/nowy/' # TODO: how to pass user id .. isn't it already in request?
    
    def get_current_points(self):
        start_date = (datetime(2010, 7, 19, 0, 0, 0)).date()
        res = User.objects.all().filter(id=self.user.id).filter(review__date__gte = start_date).aggregate(Count('review__flags'))

        if len(res.keys()) > 0:
            return res['review__flags__count']
        return 0
    
    @keep_result
    def business_owner(self):
        from business.models import Business
        owned = Business.objects.all().filter(owner = self.user)
        if (owned.count() > 0):
            return owned[0]
        return None
    
    def is_elite(self):
        if Flag.objects.user_flaged_by(user=self.user, flag='elite').count() > 0:
            return True
        return False

    def get_preference(self, pref):
        pref_field = PreferencesField(id=pref)
        pref, v = PreferencesFieldValue.objects.get_or_create(field=pref_field, obj=self.user.get_profile())
        return pref.value

    @classmethod
    def get_sidebar_template(cls):
        return "community/profile.sidebar.html"

#noinspection PyUnusedLocal
def profile_post_save(sender, **kwargs):
    """
        
    """
    instance = kwargs['instance']
    
    if instance.gallery is None:
        gal = Gallery(user=instance.user)
        gal.save()
        instance.gallery = gal
        instance.save()

post_save.connect(profile_post_save, sender=Profile)

#noinspection PyUnusedLocal
def user_post_save(sender, **kwargs):
    user = kwargs['instance']
    try:
        profile = user.get_profile()
    except Profile.DoesNotExist:
        user.profile = Profile()
        user.profile.user = user
        user.profile.save()
        user.save()

post_save.connect(user_post_save, sender=User)

class InfoFieldValue(DynamicFieldValue):
    field = models.ForeignKey(InfoField)
    obj = models.ForeignKey(Profile)
    
    class Meta:
        ordering = ['field']

class Location(GeoModel):
    """ User location """
    name = models.CharField(max_length=50, verbose_name = "Nazwa")

    user = models.ForeignKey(User, blank=True, null=True)
    address = models.CharField(max_length=50, verbose_name = "Adres")
    city = models.CharField(max_length=100, verbose_name = "Miasto")
    zip = models.CharField(max_length=10, verbose_name = "Kod pocztowy")
    
    # position = inherited from GeoModel

    default_city = ""

    default_city_used = False

    export_fields = ('id', 'default_city_used', 'city', 'longitude', 'latitude', 'version', 'activity')
    export_fields_ex = ()
    
    def activity(self):
        return 1

    def __str__(self):
        if len(self.name) > 1:
            return self.name
#        if len(self.city) > 1 and len(self.address) > 1:
#            return self.city + ', ' + self.address
        if len(self.address) > 1:
            return self.address
        return self.city

    def __unicode__(self):
        return self.__str__()
    
    def get_display_address(self):
        """ Returns address in display version """
        # TODO
        return self.address

    def get_position_from_address(self):
        """ Use geopy to convert address to position """
        addr = u'%s %s, %s' % (self.zip, self.city, self.address)
        self.get_from_string(addr)
        return self.position

    def get_from_string(self, address):
        """ Use geopy to convert address to position """
        addr = address
        address = 'Polska, ' + address
        
        super(Location, self).get_position_from_address(address)

        self.address = addr
#        self.address = self.address[0:self.address.rfind(", Warszawa")]

    def save(self, *args, **kwargs):
        """ Before saving the object get point it referes to """
        self.get_from_string(self.address)
        self.radius = 0
#        self.position = GEOSGeometry('POINT(%f %f)' % (1.0,1.0))

        super(Location, self).save(*args, **kwargs)

    def location_from_string(self, location_str):
        """ Parse human-input-string to get proper location (TODO) """
        # TODO: prowizorka
        self.address = location_str
        self.city = ''
        self.zip = ''
        
        super(Location, self).get_position_from_address(location_str)

#        raise Exception(location_str, 'POINT(%f %f)' % (self.position.x,self.position.y))


class Area(models.Model):
    """ Describes area such us city, country etc. """
    parent = models.ForeignKey('self', null=True)

    name = models.CharField(max_length=50)
    polygon = geomodels.PolygonField()
    
class Friendship(models.Model):
    """ Model for symetrical friendship relation """
    user1 = models.ForeignKey(User, unique=False, related_name='inviter_set')
    user2 = models.ForeignKey(User, unique=False, related_name='guest_set')

    accepted = models.BooleanField(default=False)
    facebook = models.BooleanField(default=False)
    
    objects = FriendshipManager()

    def save(self, *args, **kwargs):
        # if it is only accept save than save
        if self.id:
            super(Friendship, self).save(*args, **kwargs)
            notification.send([self.user1], "community_friend_accept", {'action_user': self.user2, })
            return

        # if not check if users are already friends
        if (self.user1 == self.user2):
            return
        f = Friendship.objects.get_friendship(self.user1, self.user2)
        
        # add friendship if new
        if not f:
            super(Friendship, self).save(*args, **kwargs)
            notification.send([self.user2], "community_friend_invite", {'action_user': self.user1, })
            
class FriendshipProposition(TimedModel):
    user1 = models.ForeignKey(User, unique=False, related_name='proposition_post_set')
    user2 = models.ForeignKey(User, unique=False, related_name='proposition_get_set')

    used = models.BooleanField(default=False, verbose_name="Sprawdzone")
    
    level = models.IntegerField(verbose_name="Poziom podobieństwa")

    def save(self, *args, **kwargs):
        # if it is only accept save than save
        if self.user1.id > self.user2.id:
            raise Exception
        super(FriendshipProposition, self).save(*args, **kwargs)
    
    def __init__(self, *args, **kwargs):
        super(FriendshipProposition, self).__init__(*args, **kwargs)
        if self.user1.id > self.user2.id:
            u = self.user1
            self.user1 = self.user2
            self.user2 = u
        
class Invitation(models.Model):
    """ Model for symetrical friendship relation """
    user = models.ForeignKey(User, null=True, blank=True)
    email = models.CharField(max_length=100, verbose_name="E-mail znajomego", null=True, blank=True)
    body = models.TextField(verbose_name="Wiadomość", null=True, blank=True, default='Cześć!\n\nChciałbym zaprosić Cię do nowego rewelacyjnego serwisu społecznościowego, gdzie oceniamy wszystkie polskie lokale, salony, sklepy itp. Wejdź na http://neib.org/ i zarejestruj się!\n\nPozdrawiam')

    def save(self, *args, **kwargs):
        send_mail('Zaproszenie do serwisu neib.org', self.body, self.user.email, (self.email,),
              fail_silently=True)
        #notify_user('Zaproszenie zostało wysłane','')
        # check if users are already friends
        super(Invitation, self).save(*args, **kwargs)
        

