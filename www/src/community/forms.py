"""
Profile form. There will be more fields. TODO
"""
from django import forms
from django.forms import ModelForm, Form

from community.models import *

class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = (
            'nickname',
#            'birth_date',
            'gender',
            'city',
            'motto'
        )

class UserProfileForm(ModelForm):
    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
        )
  
class InfoForm(ModelForm):
    class Meta:
        model = Profile
        fields = (
            'nickname',
#            'birth_date',
            'gender',
            'motto',
        )

class PasswordForm(ModelForm):
    class Meta:
        model = User
        fields = (
            'password',
        )

class InvitationForm(ModelForm):
    class Meta:
        model = Invitation
        fields = (
            'body',
            'email',
        )

class LocationForm(ModelForm):
    class Meta:
        model = Location
        fields = (
            'name', 'address', 
        )
    address = forms.CharField(max_length = 100, label = "Adres", initial = None)
        
    def __init__(self, *args, **kwargs):
        if kwargs.has_key('instance'):
            kwargs['instance'].address = kwargs['instance'].get_display_address()

        super(LocationForm, self).__init__(*args, **kwargs)

    def save(self):
        self.instance.location_from_string(self.cleaned_data['address'])
        super(LocationForm, self).save()
        
class ChangePasswordForm(Form):
    pw1 = forms.CharField( widget=forms.PasswordInput, label="Nowe haslo" )
    pw2 = forms.CharField( widget=forms.PasswordInput, label="Powtorz nowe haslo" )