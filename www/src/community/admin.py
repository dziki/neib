from django.contrib import admin
from community.models import PreferencesGroup,PreferencesField,InfoField,Location,Profile

admin.site.register(PreferencesGroup)
admin.site.register(PreferencesField)
admin.site.register(InfoField)
admin.site.register(Location)
admin.site.register(Profile)