from django.contrib.gis.db import models as geomodels
from django.db.models import Q
from django.contrib.auth.models import User 

class FriendshipManager(geomodels.Manager):
    """ Get best businesses in your neighborhood (TODO) """
    def are_friends(self, user1, user2):
        rel1 = len(super(FriendshipManager, self).get_query_set().filter(accepted=True).filter(user1__id=user1.id, user2__id=user2.id))
        rel2 = len(super(FriendshipManager, self).get_query_set().filter(accepted=True).filter(user2__id=user1.id, user1__id=user2.id))
        if rel1 > 0 or rel2 > 0:
            return True
        return False

    #noinspection PyUnusedLocal
    def get_friendship(self, user1, user2, **kwargs):
        qs = super(FriendshipManager, self).get_query_set()
   
        rel1 = qs.filter(user1__id=user1.id, user2__id=user2.id)
        rel2 = qs.filter(user2__id=user1.id, user1__id=user2.id)
       
        if len(rel1) > 0:
            return rel1[0]
        if len(rel2) > 0:
            return rel2[0]
        return None

    def get_friends(self, user, **kwargs):
        accepted = kwargs.get('accepted', True)

        rel_list = super(FriendshipManager, self).get_query_set().filter(accepted = accepted)
        rel_list1 = rel_list.filter(user1__id=user.id).values('user2__id')
        rel_list2 = rel_list.filter(user2__id=user.id).values('user1__id')

        return User.objects.select_related('profile__gallery__representative').filter(Q(id__in = rel_list1) | Q(id__in = rel_list2)).exclude(id=user.id)
    
    def invited_by(self, user):
        return super(FriendshipManager, self).get_query_set().filter(user2__id=user.id, accepted=False)

class ProfileManager(geomodels.Manager):
    """ Get best businesses in your neighborhood (TODO) """

    #noinspection PyUnusedLocal
    def adjust_kwargs(self, *args, **kwargs):
        """ small hack for api_generic_get """
        if kwargs.has_key('id__in'):
            kwargs['user__id__in'] = kwargs['id__in']
            kwargs.pop('id__in')
            
        if kwargs.has_key('id__exact'):
            kwargs['user__id__exact'] = kwargs['id__exact']
            kwargs.pop('id__exact')

        return kwargs
    
    def filter(self, *args, **kwargs):
        kwargs = self.adjust_kwargs(*args, **kwargs)
        return super(ProfileManager, self).filter(*args, **kwargs)
    
    def get(self, *args, **kwargs):
        kwargs = self.adjust_kwargs(*args, **kwargs)
        return super(ProfileManager, self).get(*args, **kwargs)
    
    