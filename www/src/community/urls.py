from django.conf.urls.defaults import patterns
from registration.decorators import set_login_cookie, set_logout_cookie

from django.contrib import admin
from community.views import *
from django.contrib.auth.views import login,logout

from django.utils.translation import ugettext_lazy as _

admin.autodiscover()

urlpatterns = patterns('',
    ((r'^login/$'), set_login_cookie(login), {'template_name': 'registration/login.html'}, 'profile_login'),
    ((r'^logout/$'), set_logout_cookie(logout), {}, 'profile_logout'),
    ((r'^login/$'), profile_logged, {}, 'profile_logged'),
    
    ((r'^location/$'), location_list, {}, 'location_list'),
    ((r'^location/add/$'), location_add, {}, 'location_add'),
    ((r'^location/edit/(?P<location_id>[-\w]+)/$'), location_edit, {}, 'location_edit'),
    ((r'^location/remove/(?P<location_id>[-\w]+)/$'), location_delete, {}, 'location_delete'),

    ((r'^preferences/edit/$'), profile_preferences_edit, {}, 'profile_preferences_edit'),
    ((r'^details/edit/$'), profile_details_edit, {}, 'profile_details_edit'),
    ((r'^info/edit/$'), profile_info_edit, {}, 'profile_info_edit'),

    ((r'^friends/edit/$'), friends_edit, {}, 'friends_edit'),
    ((r'^friends/(?P<friend_id>[-\w]+)/remove/$'), friendship_delete, {}, 'friendship_delete'),
    ((r'^friends/(?P<friend_id>[-\w]+)/accept/$'), friendship_accept, {}, 'friendship_accept'),
    ((r'^friends/(?P<username>[-\w]+)/add/$'), profile_friendship_add, {}, 'profile_friendship_add'),
    
    ((r'^(?P<username>[-\w]+)/news/$'), profile, {}, 'profile'),
    ((r'^(?P<username>[-\w]+)/reviews/$'), reviews_list, {}, 'reviews_list'),
    ((r'^(?P<username>[-\w]+)/friends/$'), friends_list, {}, 'friends_list'),
    ((r'^(?P<username>[-\w]+)/favorites/$'), favorites_list, {}, 'favorites_list'),

    ((r'^password/edit/$'), password_edit, {}, 'password_edit'),
    ((r'^bookmarks/remove/(?P<model>[-\w]+)/(?P<id>[-\w]+)/$'), favorites_del, {}, 'favorites_del'),

    ((r'^(?P<username>[-\w]+)/lists/$'), business_lists, {}, 'business_lists'),
    ((r'^(?P<username>[-\w]+)/flag/(?P<flag>[-\w]+)/$'), profile_flag_add, {}, 'profile_flag_add'),

    ((r'^forum/(?P<username>[-\w]+)/$'), 'forum.views.forum_user_list', {}, 'forum_user_list'),
)