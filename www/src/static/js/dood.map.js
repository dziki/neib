	function findMarker(markers, id){
		for (var i=0;i<markers.length;i++)
			if (markers[i].data['id'] == id)
				return markers[i]
	    return null;
	}
	function showMapTooltip(id)
	{
		if (window.google === undefined) return;
		google.maps.event.trigger(findMarker($("#map_canvas").getMap().markers,id), "mouseover");
	}
	function hideMapTooltip(id)
	{
		if (window.google === undefined) return;
		google.maps.event.trigger(findMarker($("#map_canvas").getMap().markers,id), "mouseout");
	}
	
	function fitZoom()
	{
	    return this.each(function(i, obj){
		    var map = obj.map;

	    	// TODO: Naprawić brzydki hack.
		    if (!map.zoomFited){

				if (map.markers.length <= 1){
				    var newCenter = map.bounds.getCenter();
				    map.setCenter(newCenter);
				    map.setZoom(15);
				}
				else{
			    	map.zoomFited++;
				    map.fitBounds(map.bounds);
				}
				    
			}
		});
	}	
	
	function fixPosition(enable){
		var self = this;
		self.startTop = self.position().top;
		if (enable == undefined)
			enable = true;
		if (enable){
			$(window).scroll(function() {
				var pos = $(this).scrollTop() - self.startTop + 10;
				if (pos < 0) pos = 0;
 	    		self.css('top', (self.startTop + pos) + "px");
			});
		}
		return this;
	}

	function _addPoint(map, point, data) {
		var id = 0;
		if (data)
			id = data['id'];

		var markerIcon = MEDIA_URL + "gfx/marker.png";
		map.bounds.extend(point);
		var marker = new google.maps.Marker({
        	position: point,
        	map: map,
    	    icon: markerIcon
	    });			
        marker.data = data;
        map.markers.push(marker);
        
        $.each(map.markerListeners, function(i, obj){
	        google.maps.event.addListener(marker,obj.event,function(){obj.callback(marker);});
        });
       
       google.maps.event.addListener(marker, "click", function() {
		  document.location='http://dood.pl/biz/' + this.data["id"] +'/';
      });
    }
    
	function addPoint(latitude, longitude, data)
	{
		var point = null;
	    return this.each(function(i, obj){
			if (longitude*longitude + latitude*latitude > 1)
				point = new google.maps.LatLng(latitude, longitude);
			if (point!= null){
		       	_addPoint(obj.map, point, data);
		    }
	    });
	}
	
	function getMap(){
		return this[0].map;
	}
	
	function initGoogleMap(){
	    return this.each(function(i, obj){
	        var self = obj;
	
			  self.map = null;
	
			  if (window.google){
			  if (window.google.maps){
			    var myOptions = {
			      zoom: 15,
			      mapTypeId: google.maps.MapTypeId.ROADMAP,
			      streetViewControl: false,
			      mapTypeControl: false
			    }
			    self.map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
			    self.map.markers = new Array();
				self.map.bounds = new google.maps.LatLngBounds();
				
				self.map.overlay = new google.maps.OverlayView();
				self.map.overlay.draw = function() {};
				self.map.overlay.setMap(self.map);
				self.map.markerListeners = new Array();
				self.map.zoomFited = 0;
				
				self.map.clearOverlays = function(){
					while(self.map.markers.length > 0){
						var marker = self.map.markers.pop();
						marker.setMap(null);
					}
					self.map.bounds = new google.maps.LatLngBounds();
				}
			  }}
	    });
	}
	
	function addMarkerListeners(event, callback)
	{
	    return this.each(function(i, obj){
	    	obj.map.markerListeners.push({
	    		event: event,
	    		callback: callback
	    	});
		});
	}	

	function mapResized(event, callback)
	{
	    return this.each(function(i, obj){
	    	google.maps.event.trigger(obj.map, "resize");
		});
	}	
	
	function showTooltipCallback(marker){
		  var el = $("#biz-" + marker.data["id"]);
		  el.css('background-color', '#FFF0F0');

		  var map = $("#map_canvas").getMap();
		  var map_pt = marker.getPosition();
		  var data = marker.data;
		  var markerOffset = map.overlay.getProjection().fromLatLngToContainerPixel(map_pt);
		  x = markerOffset.x - $("#map-tooltip").width() - 15;
		  y = markerOffset.y;
    	  $("#map-tooltip").show().css({ top:y, left:x, 'z-index':200 });
    	  $("#map-tooltip-name").text(data['name']);
    	  $("#map-tooltip-street").text(data['address']);
    	  $("#map-tooltip-city").text(data['zip'] + ' ' + data['city']);
    	  photo_id = data['photo']['id'];
    	  if (photo_id == null)
    	  	photo_id = 'building';
    	  $("#map-tooltip-photo").attr("src", MEDIA_URL + "photos/" + photo_id + ".thumbnail.jpg");
		
		  marker.setIcon(MEDIA_URL + "gfx/marker.checked.png");
    	  
    	  review_tmp = marker.data['review_body'].split(" ",30);
    	  review = review_tmp.join(" ");
    	  if (review_tmp.length == 30)
    	  	review += "...";
    	  $("#map-tooltip-review").text(review);
	}
	
	function hideTooltipCallback(marker) {
		var el = $("#biz-" + marker.data["id"]);
		el.css('background-color', '#FFFFFF');
		$("#map-tooltip").hide();
		marker.setIcon(MEDIA_URL + "gfx/marker.png");
    }

(function($){
    $.fn.initGoogleMap = initGoogleMap;
    $.fn.addPoint = addPoint;
    $.fn.fitZoom = fitZoom;
    $.fn.getMap = getMap;
    $.fn.fixPosition = fixPosition;
    $.fn.addMarkerListeners = addMarkerListeners;
    $.fn.mapResized = mapResized;
})(jQuery);

// ON READY:
$(document).ready(function () {
	$("#map_canvas").initGoogleMap();
	$("#map_canvas").css({position: 'relative'});
});