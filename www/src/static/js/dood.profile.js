// favorite business (used on business view)
function addFav()
{
	addFlag('business', selectedBusiness.id, 'bookmark', function(){
		$('#favorite-add').html("<img src=\"" + MEDIA_URL + "gfx/icons/favorite-add.gif\" />Ulubiony!");
	});
}

// Warning: function added as inline to flag review <A> element (TODO?)
function addReviewFlag(id, flag){
	addFlag('review', id, flag, function(){
		var elId = '#numflags-' + id + '-' + flag;
		var val = $(elId).html();
		$(elId).html(parseInt(val)+1);
	});
}

// Favorite user - used on profile page
function addObserve()
{
	addFlag('user', selectedUser.id, 'bookmark', function(){
		$('#observe-add').html("<img src=\"" + MEDIA_URL + "gfx/icons/observe-add.gif\">Obserwowany!");
	});
}

// Add to friend used on profile page
function addFriend()
{
	addFlag('user', selectedUser.id, 'friend/add', function(){
		$('#friend-add').html("<img src=\"" + MEDIA_URL + "gfx/icons/friend-add.gif\">Zaproszenie wysłane!");
		messageBox('Użytkownik pojawi się na liście znajomych, gdy tylko potwierdzi zaproszenie.','Zaproszenie wysłane');
	});
}

// Managing friend suggestions (on users home page)
function acceptFriendProposition(id)
{
	addFlag('friend-proposition', id, '/add', function(){
		$('#friend-proposition-' + id).html("Zaproszenie wysłane!");
		messageBox('Użytkownik pojawi się na liście znajomych, gdy tylko potwierdzi zaproszenie.','Zaproszenie wysłane');
	});
}
function removeFriendProposition(id)
{
	addFlag('friend-proposition', id, '/remove', function(){
		$('#friend-proposition-' + id).html("");
	});
}

// Adding compliments
var newCompliment = {
	id: null,
	title: null,
	body: null,
	photoUrl: null,
}
function complimentCallback()
{
	messageBox("Skomplementowałeś doodera. I ten komplement pojawił się właśnie po prawej stronie profilu. Fajnie.","doodKomplement");
	
	if ($('#profile-compliments').html().length < 10)
		$('#profile-compliments').append("<h2>Komplementy</h2>");
	var html = "	<div class=\"full-rev\">" +
	"<div style=\"float: left; width: 70px; text-align: center;\">" +
	"<a href=\"" + currentUser.profileUrl + "\"><img width=\"50\" height=\"50\" alt=\"\" title=\"\" src=\"" + MEDIA_URL + "photos/" + currentUser.photoId + ".thumbnail.jpg\"></a>" +
	"</div>" +
	"<div class=\"title\"><div class=\"compliment-title\"><img src=\"" + newCompliment.photoUrl + "\" /> " + newCompliment.title + "</div></div>" +
    "<div class=\"date\">przed chwilą</div>" +
    newCompliment.body + "<br />" +
    "<div class=\"clean\"></div>" +
    "<div class=\"hr\"><!-- --></div>" +
    "</div>";
	$('#profile-compliments').append(html);
}
function complimentSubmit()
{
	var type = $('#compliment-box').find(":radio:checked").val();
	var body = $('#compliment-box').find(":input[name=body]").val();
    newCompliment.id = type;
    newCompliment.title = $('#compliment-box').find("#compliment-title-"+type).html();
    newCompliment.photoUrl = $('#compliment-box').find("#compliment-img-"+type).attr('src');
    newCompliment.body = body;

	$.post("/json/compliment/add/", { user_id: selectedUser.id,
		    compliment_id: type,
		    body: body},
			function(data, textStatus){
				if (data['error'] == -4)
                    document.location = '/profil/zaloguj';
				else
                    complimentCallback();
			}, "json");
  
	return false;
}

$(document).ready(function () {
	$('#compliment-box').doodPopup('#compliment-add');
	$('#compliment-form').submit(function() {
	  $('#compliment-box').toggle();//.html("Wysłano!");
      event.preventDefault();
	  complimentSubmit();
	});
});