if (currentSite == 'business'){
function select_tab(tab){
	$('#reviews').hide();
	$('#local-info').hide();

	$('#reviews-tab').removeClass('selected');
	$('#local-info-tab').removeClass('selected');

	$('#reviews-tab').addClass('normal');
	$('#local-info-tab').addClass('normal');
	
	$('#' + tab + '-tab').removeClass('normal');
	$('#' + tab + '-tab').addClass('selected');
	$('#' + tab).show();
}

$(document).ready(function () {
	$('#map_canvas').addPoint(selectedBusiness.latitude, selectedBusiness.longitude).fitZoom();

	$('.rating').rating();
	
	$('#show-hidden-reviews').click(function() {
		$('#hidden-reviews').show();
	});
	
	$('#reviews-tab').click(function() {
		select_tab('reviews');
	});
	$('#local-info-tab').click(function() {
		select_tab('local-info');
	});
});
}