function commentsSender(){
    return this.each(function(i, obj){
        var self = $(this);

        if(currentUser.id){
            self.submit(function(evtObj){
                var me = $(this);
                var input = me.find("input[name='comment']");
                var comment = input.val();
                me.find("input[type=submit]").attr("disabled", true);
                input.attr("disabled", true);
                                
                $.post(me.attr('action'),
                    {'comment': comment},
                    function(data, textStatus){
                        if(textStatus === "success" && data['error'] === 0){
                            var new_comment = $("<div>" + comment + "</div>");
                            new_comment.addClass("comment")
                                       .addClass("ui-corner-all");

                            var html =  '<a href="/profil/' + currentUser.id + '/aktualnosci/">' +
                                            '<img width="20" height="20" src="' + getPhotoUrl(currentUser.photoId,'micro') + '"/>' +
                                        '</a>' +
                                        '<div class="content">' +
                                            '<span class="user">' +
                                                '<a href="/profil/' + currentUser.id + '/aktualnosci/">' +
                                                currentUser.fullname + '</a></span>' +
                                            comment +
                                        '</div>' +
                                        '<div class="clean"></div>';

                            new_comment.html(html);

                            new_comment.appendTo(me.parents('.comments').find('.posted')).hide().slideDown("slow");

                            input.val('');
                        }
                        else {
                            alert("Nie udało się wysłać komentarza. Jeśli błąd będzie się powatarzał proszę skontaktować się" +
                                    "z administratorem");
                        }

                        input.attr("disabled", "");
		                me.find("input[type=submit]").attr("disabled", false);
                    });
                return false;
            });
        }else{
            self.find("input[type='text']").attr('disabled', 'true').css('color', 'gray').val("Aby dodać komentarz musisz się najpierw zalogować");
            self.find(".submit").hide();
        }
    });
}

(function($){
    $.fn.commentsSender = commentsSender;
})(jQuery);