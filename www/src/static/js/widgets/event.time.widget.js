$(document).ready(function () {
	var now = new Date();
	var date_now = $.datepicker.formatDate('dd.mm.yy', now);
	var time_now = now.getHours() + ':' + now.getMinutes();
	$("#id_event_date_0").datepicker();
	$("#id_event_date_0").datepicker($.datepicker.regional['pl']);
	$("#id_event_date_0").datepicker( "option", "dateFormat", 'dd.mm.yy' );
	$("#id_event_date_0").datepicker( "setDate", date_now);
	$("#id_event_date_1").val(time_now);
});