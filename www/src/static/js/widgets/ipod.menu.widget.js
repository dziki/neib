$(document).ready(function(){
    $('#ipod-menu').children('div').hide();
    var results = $('<div id="ipod-menu-result-list"/>').appendTo('#ipod-menu');
	results.hide();
    
    $('#ipod-menu').children('a').menu({
        content: $('#ipod-menu').children('div').html(), // grab content from this page
        showSpeed: 400,
        backLink: true,
		backLinkText: "Wróć",
		crumbDefaultText: ' ',
        //crumbDefaultText: ' ',
        width: 350,
        //maxHeight: 200,
        result_id: 'ipod-menu-hidden',
        result_list_id: 'ipod-menu-result-list'
    });
    
    $("#ipod-menu").parents("form").submit(function(e){
        var elems = ""
        $("#ipod-menu-result-list").children().each(function(){
            elems = elems + $(this).find(".ipod-menu-result-item-id").text() + ";";
        });
        $('#ipod-menu-hidden').val(elems);
        //return false;
    }); 
});
