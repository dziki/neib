$(document).ready(function () {
	$("#cities").doodPopup('#change-city');
	$("#langs").doodPopup('#change-lang');
	$("#countries").doodPopup('#change-country');

    $(".rate .comments form").commentsSender();
    $('#s').doodWarsawDistrictSearch();
	$("input[name='co']").doodSearchHints();
	$(".notification").delay(5000).slideUp(800);
    $("#transalte-all").click(function(){ $(".translatable").translate(LANGUAGE_CODE); });
});
