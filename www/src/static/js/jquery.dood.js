/**
 * @author Adam Kaliński
 * @author Łukasz
 */

function getPhotoUrl(id,type){
	if (id == '')
		id = "male";
	return MEDIA_URL + 'photos/' + id + '.' + type + '.jpg';
}

function messageBox(content, title) {
	$('<div/>').text(content).appendTo('body').dialog({
				    draggable: false,
				    modal: true,
				    position: 'center',
				    resizable: false,
				    title: title,
				    width: 300,
				    height: 150,
					buttons: {
						"Ok": function() {
							$(this).dialog("close");
						}
					}
				});
}

function messageBox(content, title, width, height) {
	$('<div/>').html(content).css("text-align","left").appendTo('body').dialog({
				    draggable: false,
				    modal: true,
				    position: 'center',
				    resizable: false,
				    title: title,
				    width: width,
				    height: height,
					buttons: {
						"Ok": function() {
							$(this).dialog("close");
						}
					}
				});
}

function confirmBox(content, title) {
	$('<div/>').text(content).appendTo('body').dialog({
				    draggable: false,
				    modal: true,
				    position: 'center',
				    resizable: false,
				    title: title,
				    width: 300,
				    height: 150,
					buttons: {
                        "Tak": function() {
                            $(this).dialog("close");
                        },
                        "Nie": function() {
                            $(this).dialog("close");
                        }
					}
				});
}

/* Element on which it is run is hidden and showed when user clicks on 
 * the trigger element. It is hidden again on click on the trigger element
 * or the element with class ".close" within the element
 */
function doodPopup(trigger){
    var content = $(this)
    content.toggle(false);
    content.find('.close').click(function(){
        content.slideUp();
    });
    var res = this.each(function(i, obj){
        $(trigger).click(function(e){
            $(obj).slideToggle('slow');
            e.preventDefault();
        });
    });
    return res;
};
/* 
 * Suggest some Warsaw districts
 */
function doodWarsawDistrictSearch(){
    var loc = ["Warszawa", "Warszawa, Bemowo", "Warszawa, Białołęka", "Warszawa, Bielany", "Warszawa, Centrum", "Warszawa, Mokotów", "Warszawa, Ochota", "Warszawa, Powiśle", "Warszawa, Praga Płd.", "Warszawa, Praga Płn.", "Warszawa, Rembertów", "Warszawa, Śródmieście", "Warszawa, Targówek", "Warszawa, Ursus", "Warszawa, Ursynów", "Warszawa, Wawer", "Warszawa, Wesoła", "Warszawa, Wilanów", "Warszawa, Włochy", "Warszawa, Wola", "Warszawa, Żoliborz"];
    var res = this.each(function(i, obj){
        $(obj).autocomplete({
            source: loc
        });
    });
    return res;
}

function doodSearchHints(){
    return this.each(function(i, obj){
		var gdzie = $("input[name='gdzie']").val();
		var field = $(obj);
        field.autocomplete({
            source: function(request, response){
                $.getJSON("/json/search/hint/?what=" + request.term + "&where=" + gdzie, function(data){
					if(data.error === 0)
                    	response(data.result)
					else{
						response();
					}                    
                });
            },
			select: function() {
				field.parents("form").submit();
			},
            minLength: 2
        });
    });
}

(function($){
    $.fn.doodPopup = doodPopup;
    $.fn.doodWarsawDistrictSearch = doodWarsawDistrictSearch;
    $.fn.doodSearchHints = doodSearchHints;
})(jQuery);
