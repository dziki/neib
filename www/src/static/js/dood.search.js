var dood = {
	search: {
		results: [],
		mapEnlarged: false,
		lastDrag: 0,
		mapStayPut: false,
		requestId: 0
	}
};

if (currentSite == 'search')
{
	// --- SEARCH FUNCTIONS --- //
	function showRating(rating)
	{
	 var ret = '<div style="margin: 0px; float: left; width: 85 px; height: 18 px; background-image: url(\'' + MEDIA_URL +'gfx/stars-small-0.gif\')">';
	 ret += '<div style="float: left; background-image: url(\'' + MEDIA_URL +'gfx/stars-small-';
	 ret += Math.floor(rating).toString();;
	 ret += '.gif\'); width: ';
	 ret += Math.floor(85 * rating/5.0).toString();;
	 ret += 'px; height: 18px"><!-- --></div><div style="float: right; width: ';
	 ret += Math.floor(85 * (1-rating/5.0)).toString();;
	 ret += 'px; height: 18px"></div></div>';
	 return ret;
	}
	
	function formatResult(res)
	{
	var photo = 'building.thumbnail.jpg';
    var nres = res['review_body'].substring(0,200).search(/(\s+)\w*$/);
    var text = '';
    if (nres <= 0)
        text = res['review_body'];
	else    
        text = res['review_body'].substring(0,nres) + "...";;
    if (text.length <= 0){
        text = '<a style="color: #baa;" href="/biz/' + res['slug'] + '/#new_review">' + "Dodaj pierwszą recenzję" + '</a>' ;
    }
	if (res['photo'])
		photo = res['photo'] + '.thumbnail.jpg';
	var newPar = $("<div/>").hover(
		function(){showMapTooltip(res['id'])}, 
		function(){hideMapTooltip(res['id'])}
	);
	newPar.attr('id','biz-' + res['id']);
	newPar.addClass('full-biz');
//'<div onmouseover=\"\" onmouseout=\"hideMapTooltip(' + res['id'] + ')\" id="" class="full-biz">' +
		
	newPar.html('	<div style="float: left; width: 70px; text-align: center; ">' +
'	<a href="/biz/' + res['slug'] + '/"><img alt="' + res['name'] + '" title="' + res['name'] + '" src="' + MEDIA_URL + 'photos/' + photo + '" width="50" height="50"></a>' +
'	</div>' +
'	<div style="float: right; padding-right: 10px; text-align: right;">' +
'		' + res['address'] + '<br />' +
'		<strong>' + res['phone'] + '</strong>' +
'	</div>' +
'	<a href="/biz/' + res['slug'] + '/">' + res['name'] + '</a><br />' +
'	' + showRating(res['rating']) + '<br />' + '<br />'+
'   <div style="margin: 10px 5px 0px 10px; text-align: justify;" class="reduced-review">' +
text +
'   </div>' +
'    <div class="clean"></div>');

		return newPar;
	}

	function showResults(data, requestId)
	{
		// return if request expired 
		if (requestId < dood.search.requestId)
			return 0;
         var resultsDiv = $("#results");

		// if not stay-put clear map, results array, and results pane
		if (!dood.search.mapStayPut){
            $("#map_canvas").getMap().clearOverlays();
			resultsDiv.html('');
			dood.search.results = new Array();
		}

		// update navigation
        var results = data['result'];
        $("#results-navigation").children().remove();
        var num_of_results = parseFloat(data['num_of_results']);
        var num_of_pages = Math.min(Math.ceil(num_of_results / 10),10);
        
        if (num_of_pages > 1){
	        var i = 0;
	        var html = '';
	        while (i<num_of_pages){
				i++;
				if (i == searchParams["page"])
					html = html + '<strong>' + i + '</strong>';
				else
					html = html + '<a href="javascript:getResults(' + i + ')">' + i + '</a>';
			}
			$("#results-navigation").html(html);
        }
        
        // hide loader
	    $("#loader").css("display","none");
		
		// add results
	    for (i = 0; res = results[i]; i++)
	    {
	    	// ommit result if it is already on the map
	    	var found = false;
	    	$.each(dood.search.results, function(index, value){
	    		if (value['id'] == res['id'])
					found = true;
	    	});
	    	if (found)
				continue;
	    	dood.search.results.push(res);
            resultsDiv.append($("<div/>").html(formatResult(res)));
			resultsDiv.append('<div class="hr-nomargin"><!-- --></div>')
			$("#map_canvas").addPoint(parseFloat(res['latitude']), parseFloat(res['longitude']), res);
	    }
	    
	    if (dood.search.results.length == 0)
	    	$("#search-no-results").show();
	    else
	    	$("#search-no-results").hide();
	    
	    // if map is enlraged hide reviews
		if (dood.search.mapEnlarged){
			$(".reduced-review").hide();
		}

		// fit map if it hasn't been touched already
		if (results.length > 0)
		    $("#map_canvas").fitZoom();

		// store current settings for history back
		updateBounds();
		window.location.hash = $.param(searchParams);
    }
    
    function showLoader()
    {
	    
//	    newPara.css("position: relative; z-intex: 100");
	    $("#loader").css("display","block");
	    $("#loader").css("position","absolute");
	    $("#loader").css("z-index","100");

		$("#loader").css('width', $("#results").css('width'));
		$("#loader").css('height', $("#results").css('height'));
		$("#loader").css('background-color', '#ffffff');
		$("#loader").css('opacity', '0.6');
		$("#loader").css('filter', 'alpha(opacity=60)');
    }
    
	
      function _searchBusinesses(page){
			params = searchParams;
			dood.search.requestId++;
			var reqId = dood.search.requestId;
  			params_str = "";
  			
  			var bl=params["bottom"];
  			params["bottom"]=params["left"];
  			params["left"]=bl;
  			var tr=params["top"];
  			params["top"]=params["right"];
  			params["right"]=tr;
  			
  			var keys = [];
		    for (key in params) {
		        params_str += "&" + key + "=" + params[key];
		    }
		    $.getJSON('/json/business/list/?extend=1' + params_str,
            {},
            function(data){ showResults(data,reqId);});
      }

      function getResults(page) {
		showLoader();

		searchParams["pagination"] = 1;
		searchParams["page"] = page;
		_searchBusinesses(searchParams);
      }

      function searchBusinesses(params)
      {
      	for (key in params)
	      	searchParams[key] = params[key];
		showLoader();
		_searchBusinesses(searchParams);
      }
      
      function updateBounds(){
		var map = $("#map_canvas").getMap();
	    map.bounds = map.getBounds();
	    if (map.bounds){
		    sw = map.bounds.getSouthWest();
		    ne = map.bounds.getNorthEast();
		    searchParams["right"] = ne.lng();
		    searchParams["top"] = ne.lat();
		    searchParams["left"] = sw.lng();
		    searchParams["bottom"] = sw.lat();
	    	if ($("#map-research:checked").val() != null)
			    searchParams["bounds"] = 1;
	    	else {
			    searchParams["bounds"] = 0;
			    return;
			}
	    }
      }
      
    function updateLocations ()
    {
    	var map = $("#map_canvas").getMap();
    	
    	// TODO: Naprawić brzydki hack.
    	if (map.zoomFited < 3){
    		map.zoomFited++;
    		return;
    	}
    	if ($("#map-research:checked").val() == null)
		    return;
		updateBounds();
        if (searchParams["page"])
		    getResults(searchParams["page"]);
        else
		    getResults(1);
	}
	
	function selectOrdering(mode) {
		$("#select-ordering").children().removeClass("selected");
		$("#select-ordering").children("#select-ordering-"+mode).addClass("selected");
		searchBusinesses({'order_by': mode});
	}
	
	function mapStayPut(){
		dood.search.mapStayPut = $("#map-stay-put").is(':checked');

	}
	function enlargeMap(){
		var enlarge = $("#map-enlarge").is(':checked');
		var time = 1000;
		
		dood.search.mapEnlarged = enlarge;
		
		if (enlarge){
			$("#search-results-pane").animate({
    	        width: "290px"
	        }, time);
			$("#search-map-pane").animate({
    	        width: "670px"
	        }, time);
			$("#map_canvas").animate({
    	        width: "665px",
    	        height: "500px",
	        }, time);
	        $(".reduced-review").fadeOut(time/2);
		}
		else{
			$("#search-results-pane").animate({
    	        width: "670px"
	        }, time);
			$("#search-map-pane").animate({
    	        width: "290px"
	        }, time);
			$("#map_canvas").animate({
    	        width: "285px",
    	        height: "300px",
	        }, time);
	        $(".reduced-review").fadeIn(time/2);
		}
    	window.setTimeout(function(){google.maps.event.trigger($("#map_canvas").getMap(), "resize");}, time-50)
	}
	
	function parseParamArray(url){
		var arr = url.split('&');
		var res = {};
		$.each(arr, function(index,val){
			var ar = val.split('=');
			var s =decodeURIComponent(ar[1]).replace('+',' ');
			res[ar[0]] = s;
		});
		return res;
	}

	$(document).ready(function () {
//		google.maps.event.addListener($("#map_canvas").getMap(), 'bounds_changed', updateLocations);
//		google.maps.event.addListener($("#map_canvas").getMap(), 'zoom_changed', updateLocations);
		google.maps.event.addListener($("#map_canvas").getMap(), 'dragend', updateLocations);
		google.maps.event.addListener($("#map_canvas").getMap(), 'bounds_changed', function(){
			var d = new Date();
			var mil = d.getTime();
			if (mil > dood.search.lastDrag + 500){
				dood.search.lastDrag = mil;
				updateLocations();
			}
		});
		
		$("#select-ordering-fitting").children(0).click(function(){selectOrdering("fitting")});
		$("#select-ordering-rank").children(0).click(function(){selectOrdering("rank")});
		$("#select-ordering-reviews").children(0).click(function(){selectOrdering("reviews")});

//		$("#map-show-all").click(updateLocations);
		$("#map-enlarge").click(enlargeMap);
		$("#map-stay-put").click(mapStayPut);
		$("#map_canvas").parent().fixPosition(true);

		$("#map_canvas").addMarkerListeners("mouseover",showTooltipCallback);
		$("#map_canvas").addMarkerListeners("mouseout",hideTooltipCallback);
		
		$("#form-search-top").submit(function(){
			searchParams["what"] = $("#search-co").val();
			if ($("#search-gdzie").val() != searchParams["where"]){
				searchParams["where"] = $("#search-gdzie").val();
				$("#map_canvas").getMap().zoomFited = 0;
				searchParams["bounds"] = 0;
			}
			$("input[name='gdzie']").autocomplete("close");
			$("input[name='co']").autocomplete("close");
            var page = searchParams["page"];
            if (!page) page = 1;
			getResults(page);
			return false;
		});
		
		
//		if ($.cookie('mapEnlarge'))
//			$("#map-enlarge").attr('checked', true);
		
		// Manage history
		

		var lastSearchParams = parseParamArray(window.location.hash.substring(1));
		$.each(lastSearchParams, function(index,value){
			searchParams[index] = value;
		});
		if (lastSearchParams["where"])
			$("#search-gdzie").val(lastSearchParams["where"]);
		if (lastSearchParams["what"])
			$("#search-co").val(lastSearchParams["what"]);

		if ($("#map-enlarge").is(':checked'))
			enlargeMap();
		if ($("#map-stay-put").is(':checked'))
			mapStayPut();

		if (window.location.hash){
			$("#map_canvas").getMap().zoomFited = 3;
			var ratio = 0.3;
			var w = (parseFloat(searchParams["right"]) - parseFloat(searchParams["left"]))*ratio;
			var h = (parseFloat(searchParams["top"]) - parseFloat(searchParams["bottom"]))*ratio;
			$("#map_canvas").getMap().fitBounds(
				new google.maps.LatLngBounds(
					new google.maps.LatLng(parseFloat(searchParams["top"]) - h,parseFloat(searchParams["left"]) + w),
					new google.maps.LatLng(parseFloat(searchParams["bottom"]) + h,parseFloat(searchParams["right"]) - w)
				)
			);
			getResults(searchParams["page"]);
		}
	});
}