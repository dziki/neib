// MANAGING FLAGS
DOOD_API_URL = ""

function addFlag(model, id, flag, callback){
    $.getJSON(DOOD_API_URL + '/json/' + model + '/' + flag + '/?id=' + id,
                {},
	            function(data, textStatus){
			         var results = data['result'];
                     /* TODO: error table */ 
	                 if (data['error'] == -4)
	                    messageBox('Tylko zarejestrowani użytkownicy mogą oceniać recenzje.','Uwaga');
	                 else if (data['error'] == -106)
                        messageBox('Flaga jest juź dodana!','Uwaga');
	                 else if (data['error'] == -107)
                        messageBox('Nie możesz dodawać flagi sobie!','Uwaga');
                     else if (data['error'] == 0 && callback != null){
                        callback(id, flag);
                     }
	            });
}

