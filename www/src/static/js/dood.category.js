// CATEGORY MAP CODE IS INLINED BECOUSE OF INTEGRATION WITH DJANGO QUERY RESULTS
if (currentSite == 'category'){
	$(document).ready(function () {
		$("#map_canvas").addMarkerListeners("mouseover",showTooltipCallback);
		$("#map_canvas").addMarkerListeners("mouseout",hideTooltipCallback);
	});
}