import hashlib

def set_login_cookie(func):
    """
    Make another a function more beautiful.
    """
    def _decorated(*args, **kwargs):
        sha = hashlib.sha1()
        request = args[0]
        response = func(*args, **kwargs)
        if request.user.is_authenticated():
            #sha.update(request.user.id.__str__())
            #d = sha.hexdigest()
            response.set_cookie('neibuser',request.user.id.__str__(), max_age=60*60*24*30*6)
        return response
    return _decorated

def set_logout_cookie(func):
    """
    Make another a function more beautiful.
    """
    def _decorated(*args, **kwargs):
        response = func(*args, **kwargs)
        response.delete_cookie('neibuser')
        return response
    return _decorated