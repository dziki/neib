from django.db import models
from date.models import TimedModel
from business.models import Business
from django.contrib.auth.models import User

class Order(TimedModel):
    user = models.ForeignKey(User, verbose_name="Wlasciciel")
    business = models.ForeignKey(Business, verbose_name="Lokal")

    payed = models.BooleanField(verbose_name="Zapłacony", default=0)
    

    def save(self, *args, **kwargs):
        pass
            
class Item(TimedModel):
    order = models.ForeignKey(Order, verbose_name="Zamówienie")
    name = models.CharField(verbose_name="Nazwa")
    quantity = models.CharField(verbose_name="Liczba")
    
    price = models.FloatField(verbose_name='Cena')