# -*- coding: utf-8 -*- 
"""
View for main site

Lists almost all tables to use in main template

"""

from django.shortcuts import render_to_response
from django.template import RequestContext
from faq.models import Question

def faq(request, group="guide", template="businessownersguide/czeste-pytania.html"):
    faq = Question.objects.filter(group=group)

    return render_to_response(template,
        context_instance=RequestContext(request,
        {'faq': faq}))