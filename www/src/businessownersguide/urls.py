"""

"""

from django.conf.urls.defaults import patterns
from django.contrib import admin
from businessownersguide.views import *

from django.utils.translation import ugettext_lazy as _

admin.autodiscover()

urlpatterns = patterns('django.views.generic.simple',
    ((r'^what-is-neib/$'), 'direct_to_template', {'template': 'businessownersguide/co-to-neib.html'}, 'guide_whatisneib'),
    ((r'^claim-ownership/$'), 'direct_to_template', {'template': 'businessownersguide/przejecie-strony.html'}, 'guide_unlock'),
    ((r'^how-to/$'), 'direct_to_template', {'template': 'businessownersguide/jak-korzystac.html'}, 'guide_using'),
    ((r'^faq/$'), faq, {}, 'guide_faq'),
    ((r'^ads/$'), 'direct_to_template', {'template': 'businessownersguide/reklama.html'}, 'guide_advs'),
)
