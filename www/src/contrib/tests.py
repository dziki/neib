# -*- coding: utf-8 -*-
import unittest
from optimization import keep_result

class OptimizationTestCase(unittest.TestCase):

    def test_keep_result_decorator(self):
        class Tmp(object):
            val = 0

            @property
            @keep_result
            def value(self):
                self.val += 1
                return self.val

        t = Tmp()

        self.assertEqual(t.value, 1)
        self.assertEqual(t.value, 1)


if __name__ == '__main__':
    unittest.main()