# -*- coding: utf-8 -*-
from functools import wraps

def keep_result(func):
    """
    Keeps the value of the function so that it is not calculated twice.
    """
    cache_name = "_%s_cache" % func.func_name
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        if not hasattr(self, cache_name):
            setattr(self, cache_name, func(self, *args, **kwargs))
        return getattr(self, cache_name)
    return wrapper
