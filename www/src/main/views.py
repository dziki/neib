# -*- coding: utf-8 -*-
"""
View for main site

Lists almost all tables to use in main template

"""
from django.db.models.aggregates import Avg, Count

from django.utils.decorators import decorator_from_middleware
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.template import RequestContext
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.sitemaps import ping_google
from django.views.generic.simple import direct_to_template
from django.middleware.gzip import GZipMiddleware
from django.contrib.auth.decorators import login_required

from event.models import Event, Promotion
from business.models import Business, Review, Category, BusinessList
from community.models import Friendship, FriendshipProposition, Profile
from forum.models import Thread, Post
from flagging.models import get_flagged_objects

from newsletter.models import Newsletter

#def render_to_response(*args, **kwargs):
#    """
#    Returns a HttpResponse whose content is filled with the result of calling
#    django.template.loader.render_to_string() with the passed arguments.
#    """
#    httpresponse_kwargs = {'mimetype': kwargs.pop('mimetype', None)}
#    loader.render_to_string(a, **kwargs)
#    loader.render_to_string(*args, **kwargs)
#    a = list('base.head.html')
#    return HttpResponse(, **httpresponse_kwargs)
from datetime import datetime
from django.contrib.auth.models import User
from django.conf import settings
import facebook
import json
from django.db.models.query_utils import Q

@decorator_from_middleware(GZipMiddleware)
def dynamic_media(request, file):
    return direct_to_template(request, "js/%s" % file,  mimetype="application/javascript")

def robots_txt(request):
    return HttpResponse('User-agent: *\nDisallow: /w/\nAllow: /\n')

def error500test(request):
    return HttpResponse(1/0)

def home_temp(request):
    return HttpResponse('Tu bedzie strona.')

def home(request):
    if request.user.is_authenticated():
        return home_logged(request)
    return home_search(request)

def about_us(request):
    users = User.objects.all().exclude(id__in=[2,4,63,93,99,296,459,460]).filter(badge__badge=1).order_by('?')[:100]
    _employees = Profile.objects.filter(id__in=[2,4,63,93,99,296,459,460]).order_by('user__id')
    employees = []
    descs = {}
    descs[2] = "Cechuje się przerostem ambicji nad możliwościami. neib jest wynikiem tych ambicji. Absolwent informatyki i niebawem matematyki UW (JSIM). Główny programista. Pociągacz neibowych sznurków. Ciepły, czuły, wrażliwy. Prywatnie prywaciarz."
    descs[4] = "Absolwent ekonomii (WNE) i student V roku informatyki (MIM) na Uniwersytecie Warszawskim. W neibie od samego początku - zarania dziejów i pomysłów. Odpowiedzialny za rozwój aplikacji na platformy mobilne: wcześniej J2ME a obecnie Android. Prywatnie instruktor narciarstwa na emeryturze i organizator młodzieżowych obozów międzynarodowych. "
    descs[63] = "Prawie-absolwent kulturoznawstwa na Uniwersytecie Warszawskim (ma już absolutorium, nie jest źle!), kończy pracę magisterską o YouTube i nowych mediach. Studiuje podyplomowo Public Relations na ISNS UW, PR-em zajmuje się też w neib. Od kilku lat dziennikarz filmowy, prowadzi również szkolenia z wiedzy o filmie dla uczniów gimnazjów i liceów. Zafascynowany muzyką rockową i klasyczną, teorią muzyki oraz kompozycją. W wolnych chwilach gra w bilard i ogląda amerykańskie seriale."
    descs[93] = "Młody, zdolny, sympatyczny, samouk. Specjalizuje się w brandingu, typografii oraz projektowaniu systemów interakcji z użytkownikiem. Szczerze wierzy, że projektując może zmienić świat na lepsze. Do neiba trafił bardzo wcześnie i został już na stałe zajmując się jego częścią wizualną i projektowaniem usability. Prywatnie grywa w kosza, jeździ na nartach i robi zdjęcia. Jego projekty zobaczyć można na www.filip.lysyszyn.pl."
    descs[99] = "Absolwent Wydziału Matematyki i Nauk Informacyjnych Politechniki Warszawskiej. Ojciec chrzestny dood’a - nie brałem udziału w jego powstawaniu, ale wychowuje jak własne dziecko. Jestem jednym z tych co dużo mówią i niewiele robią. Z zamiłowania szalony programista, który po nocach siedzi w piwnicy i … tworzy neiba."
    descs[296] = "Absolwentka politologii i prawie administracji. Doktorantka nauk o polityce. Humanistka z umysłem analitycznym. Perfekcjonistka w każdym calu. Lubi mieć wszystko dopięte “na ostatni guzik”. Aktywistka, społecznościowiec ze skłonnościami do pracoholizmu. Z pasją zgłębia tajniki teorii komunikowania społecznego, etyki biznesu, CSR-u, HR-u. Jednak jej główną pasją jest PR, którym zajmuje się też w doodzie. Odpowiedzialna jest także za koordynowanie programu praktyk, staży, moderację, generowanie treści, współpracę z partnerami i służenie dobrymi radami."
    descs[459] = "Studentka II roku Studiów Menedżerskich na UW. Prężnie działa w organizacji AIESEC. Otwarta, towarzyska i zawsze uśmiechnięta. Uwielbia podróżować; marzy jej się rejs dookoła świata! Ostatnio zafascynowała się fotografią i spędza z aparatem każdą wolną chwilę (których ostatnio coraz mniej). Latem kocha wypoczywać nad morzem, zimą – szaleć na górskich stokach! W neib przede wszystkim opiekuje się cudownymi Praktykantami! :)"
    descs[460] = "Student Studiów Menedżerskich na UW. Z zamiłowania żeglarz, maratończyk, muzyk. Lubi spotykać się ze znajomymi w klubach snookerowych lub we włoskich restauracjach. Na studiach większość czasu poświęca na projekty naukowe (AIESEC; SIFE; SKNF), bądź na pracę charytatywną, organizując zbiórki krwi na całym Uniwersytecie. Wraz z profilem studiów, w neibie również zajmuje się HR-em."

    for item in _employees:
        item.description = descs[item.user.id]
        employees.append(item)

    return render_to_response('page/about.html',
        context_instance=RequestContext(request, {"employees": employees, "users": users}))


def main_context(request):
    # location = request.session.get('location', None)

    # get one item. there must be better way but it is not important here
    # because there will be special function to get best review
    users = get_flagged_objects('elita', None, User)[:10]

    best_users = []

    area = request.session['area']
    businesses = Business.objects.filter(position__within=area.poly).select_related('main_review', 'gallery__representative')
    businesses = businesses.select_related('gallery__representative')
    businesses = businesses.exclude(review__active=0)
    businesses = businesses.filter(rating__gte=3.5).annotate(reviews=Count('review__rating')).order_by('-reviews')
    slugs = ["piwiarnia-warki-warszawa-1","centralny-klub-studentow-politechniki-warszawskiej-stodoa-warszawa",]
    businesses = businesses.exclude(slug__in=slugs)

    best = {}
    best["restaurants"] = businesses.filter(categories=1)[:7]
    best["entertainment"] = businesses.filter(categories=43)[:7]
    best["shops"] = businesses.filter(categories=44)[:7]
    best["nightlife"] = businesses.filter(categories=54)[:7]

    categories = Category.objects.all().filter(parent__id=0).annotate(businesses=Count('business__id')).order_by('-businesses')
    reviews = Review.objects.order_by("-userlang","-id").filter(active=1).filter(business__position__within=area.poly)[:3]
    reviews = reviews.select_related('user__profile__gallery__representative', 'business__gallery__representative')

    business_lists = [] #BusinessList.objects.all().extra(order_by = ['-id'])[:3]

    if request.session['area'].slug == "warszawa":
        letter = Newsletter.objects.all()[0]
        best_reviews = letter.reviews.all()[:1]
    else:
        best_reviews = reviews[:1]


    events = Event.objects.all().filter(verified=1).filter(event_date__gt=datetime.now()).filter(business__position__within=area.poly).order_by('event_date')[:5]
    events = events.select_related('gallery__representative')

    promotions = Promotion.objects.filter(business__position__within=area.poly).order_by('?')[:3]
    promotions = promotions.select_related('business__gallery__representative')

    business_sponsored = None
    spns = Business.advert_objects.with_active_advert("sponsored_link_main").order_by("?")
    if (len(spns)):
        business_sponsored = spns[0]


    threads = Thread.objects.all().filter(position__within=area.poly).values_list("id", flat=True)[:5]
    posts = Post.objects.select_related('user__profile__gallery__representative').filter(active=1).filter(object_id__in = [t for t in threads])[0:5]

    return {"best": best,
        "business_lists": business_lists,
        "reviews": reviews,
        "users": users,
        "best_users": best_users,
        "posts": posts,
        "events": events,
        "promotions": promotions,
        "business_sponsored": business_sponsored,
        "best_reviews": best_reviews,
        "categories": categories,}

def home_search(request):
    """
    Returns main page of neib site

    **Required arguments**

    None

    **Context:**

    ``businesses``
        Best businesses from given location (TODO)

    ``businesses_week``
        Best businesses this week (TODO)

    ``reviews``
        Best reviews this week (TODO)

    ``users``
        Users nearby (TODO)

    ``review_best``
        Best review today (TODO)

    ``categories``
        Categories (level 1)

    **Template:**

    index.html

    """
    cont = main_context(request)
    cont["index_page"] = True

    return render_to_response('index.html',
        context_instance=RequestContext(request, cont))

def home_search_test(request):
    """
    Returns main page of neib site

    **Required arguments**

    None

    **Context:**

    ``businesses``
        Best businesses from given location (TODO)

    ``businesses_week``
        Best businesses this week (TODO)

    ``reviews``
        Best reviews this week (TODO)

    ``users``
        Users nearby (TODO)

    ``review_best``
        Best review today (TODO)

    ``categories``
        Categories (level 1)

    **Template:**

    index.html

    """
    cont = main_context(request)

    return render_to_response('index.html',
        context_instance=RequestContext(request, cont))

@login_required()
def home_logged(request):
    # GET FRIENDS FROM FACEBOOK
    user = facebook.get_user_from_cookie(request.COOKIES, settings.FACEBOOK_API_KEY, settings.FACEBOOK_SECRET_KEY)
    if user:
        graph = facebook.GraphAPI(user["access_token"])
        friends = graph.get_connections("me", "friends")
        friends = friends["data"]
        ids = [friend['id'] for friend in friends]
        friends = User.objects.all().filter(facebookprofile__uid__in = ids)

        friends.exclude(id__in = [fd.id for fd in Friendship.objects.get_friends(request.user)])
        for friend in friends:
            f = Friendship()
            f.user1 = request.user
            f.user2 = friend
            f.accepted = True
            f.facebook = True
            f.save()

    cont = main_context(request)

    friends_reviews = Review.objects.filter(user__in=Friendship.objects.get_friends(request.user))[:3]

    props = list()
    props1 = FriendshipProposition.objects.filter(user1 = request.user, used=False)
    for prop in props1:
        props.append(prop.user2)
    props1 = FriendshipProposition.objects.filter(user2 = request.user, used=False)
    for prop in props1:
        props.append(prop.user1)
    props = props[:6]

    cont["friends_reviews"] = friends_reviews
    cont["props"] = props

    return render_to_response('community/home.html',
        context_instance=RequestContext(request,cont))

def main_ping_google(request):
    try:
        ping_google()
    except Exception:
        # Bare 'except' because we could get a variety
        # of HTTP-related exceptions.
        pass
    return HttpResponse('Operacja powiodla sie!')

