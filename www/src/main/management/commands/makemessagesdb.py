# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError

import logging as log
from business.models import Category
from business.models import InfoField
from community.models import InfoField as CommunityInfoField
from community.models import PreferencesField
from event.models import Category as EventCategory
from compliments.models import ComplimentType

log.basicConfig(level=log.INFO)

class Command(BaseCommand):
    args = '[]'
    help = 'Creates fitting cache table'
    
    option_list = BaseCommand.option_list

    def get_dict_for_model(self, Model, property):
        dict = {}
        for obj in Model.objects.all():
            dict[getattr(obj, property)] = ""

        return dict

    def get_dict_from_file(self, filename):
        dict = {}
        id = ""
        val = ""
        try:
            f = open(filename)
        except IOError:
            return dict
        for line in f.readlines():
            if (len(line) <= 1) and (line[0] != "m"):
                continue
            a,b = line.split(" \"")
            if a == "msgid":
                id = b[:-2]
            else:
                val = b[:-2]
                dict[id] = val
        f.close()
        return dict
    
    def produce_dict(self, Model, property, lang, filename):
        dict_old = self.get_dict_from_file(filename)
        dict = self.get_dict_for_model(Model, property)
        for k in dict.keys():
            if k in dict_old.keys():
                dict[k] = dict_old[k]
        return dict
#        self.save_dict(dict, path)
        
    def save_dict(self, dict, path):
        import codecs
        f = codecs.open(path,'w', 'utf-8')
        for k in dict.keys():
            f.write("msgid \"%s\"\n" % k)
            f.write("msgstr \"%s\"\n\n" % dict[k].decode('utf8'))
        f.close()
        pass

    def handle(self, *args, **options):
        filename = "/var/neib-data/locale/db/" + args[0] + "/LC_MESSAGES/django.po"
        dict = self.produce_dict(Category,"name",args[0],filename)
        dict.update(self.produce_dict(InfoField,"name",args[0],filename))
        dict.update(self.produce_dict(CommunityInfoField,"name",args[0],filename))
        dict.update(self.produce_dict(PreferencesField,"name",args[0],filename))
        dict.update(self.produce_dict(EventCategory,"title",args[0],filename))
        dict.update(self.produce_dict(ComplimentType,"full_name_male",args[0],filename))
        
        self.save_dict(dict,filename)
    
        return 