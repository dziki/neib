(function($){
	var tree = {{ category_tree }};
	var dictCats = [];
	
	function keys(obj)
	{
	    var keys = [];
	    for(var key in obj)
	        keys.push(key);
	    return keys;
	}
	
	function getList(dict){
		var ks = keys(dict);
		var resDict = [];
		for (var i= 0; i<ks.length; i++){
			var item;
			resDict[ks[i]] = dict[ks[i]]["name"];			
			if (dict[ks[i]]["recursion"]){
				var rec = getList(dict[ks[i]]["recursion"]);
				for (attrname in rec) { resDict[attrname] = rec[attrname]; }
			}
		}	
		return resDict;
	}
	
	function removeFromArray(arr,val){
		return $.grep(arr, function(value) {
        	return value != val;
		});
    }
	
	dictCats = getList(tree);
		
	var selectedList = $("#category-widget-list");
	
	var select1 = $("#category-widget-select-1");
	var select2 = $("#category-widget-select-2");
	var select3 = $("#category-widget-select-3");
	var output = $("input[name='categories']");
	var selected = [];
	
	function setUpOptions(elem){
		elem.children().tsort();
		elem.children(":first").before('<option value="">' + 'Wybierz' +'</option>');
		elem.val("");
	}
	
	function addCategory(catId){
		if (($.inArray(catId, selected) != -1) || catId == 0)
			return 0;
		selected.push(catId);
        output.val(selected.join(","));

		var div = $("<div>").html(dictCats[catId] + " ");
		var a = $("<a>").html(" x ");
	    a.click(function(){
			var r = $(this).parent().attr("id").split("-");
			var id = r[1];
			selected = removeFromArray(selected, id);
			$(this).parent().remove();
	        output.val(selected.join(","));
		});
        div.append(a);

        div.attr("id","cat-" + catId);
        div.attr("class","cat-item");
        selectedList.append(div);
	}

    function init(){
        cats = output.val().split(',');
        var i;
        
        for (i=0; i<cats.length; i++){
	        addCategory(cats[i]);
        }
    }
	
 	$.fn.extend({ 
 		//This is where you write your plugin's name
 		categorySelect: function() {			
			select2.hide();
			select3.hide();

            //init();

			select1.change(function(){
				var val = select1.val();
				addCategory(val);
				select2.empty();
				select3.empty();
				select3.hide();
				if (val !== "" && tree[val].recursion !== null){
					select2.show();
					$.each(tree[val].recursion, function(ix, val){
						select2.append('<option value="'+ ix +'">'+val.name+'</option>');
					});
					setUpOptions(select2);
					select2.show();
				}
				else {
					select2.hide();
				}
			});
			
			select2.change(function(){
				var val1 = select1.val();
				var val2 = select2.val();
				addCategory(val2);
				select3.empty();
				if (val2 !== ""){
					if (tree[val1].recursion[val2].recursion !== null) {
						select3.show();
						$.each(tree[val1].recursion[val2].recursion, function(ix, val){
							select3.append('<option value="' + ix + '">' + val.name + '</option>');
						});
						setUpOptions(select3);
					}
					else {
						select3.hide();
					}
				}
				else {
					select3.hide();
				}
			});
			
			select3.change(function(){
				var val = select3.val();
				addCategory(val);
			});
			
			//Iterate over the current set of matched elements
    		return this.each(function() {
				$.each(tree, function(ix, val){
					select1.append('<option value="'+ ix +'">'+val.name+'</option>');
				});		
				setUpOptions(select1);
                init();
    		});
    	}
	});
})(jQuery);

$(document).ready(function(){
	$("#category-widget-main").categorySelect();
});