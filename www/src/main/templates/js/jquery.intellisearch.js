/**
 * {% load common_tags %}
 * @author Adam Kalinski
 */
(function($){
    $.fn.searchDelay = 150;
    
    $.fn.intellisearch = function(){
        var resultSelected = -1;
        var resultCount = 0;
        var currResults = null;
        
        var lookAndFeel = {
			mainDiv : "",
			selectField : "",
            searchBox: "",
            searchResultId: "",
            resultsArea: "",
            resultFieldPrefix: "",
            selectClass: ""
        }
        
        var searchBoxEvents = {
			keydown : function(e){
                var keyCode = e.keyCode || window.event.keyCode;
                // check for an ENTER
                if (keyCode == 13) {
					e.preventDefault();
					hidePopup();
					$.prevSearchPhrase = $(lookAndFeel.searchBox).val();
                    return;
                }				
			},
            keyup: function(e){
                var keyCode = e.keyCode || window.event.keyCode;
                
                // check an treat up and down arrows
                if (keyCode == 40 || keyCode == 38) {
                    // remove old selection
                    unselectResult(resultSelected);
                    
                    if (keyCode == 38) { // keyUp
                        if (resultSelected == 0 || resultSelected == -1) 
                            resultSelected = resultCount - 1;
                        else 
                            resultSelected--;
                    }
                    else { // keyDown
                        if (resultSelected == resultCount - 1) 
                            resultSelected = 0;
                        else 
                            resultSelected++;
                    }
                    
                    // add new selection
                    selectResult(resultSelected);
                    
                    return;
                }
                
                // check for an ESC
                if (keyCode == 27) {
                    $(lookAndFeel.searchBox).val($.prevSearchPhrase);
                    $(lookAndFeel.searchResultId).val("");
                    hidePopup();
                    return;
                }
                
                // variables needed to decide whether to fetch data or not
                $.lastSearchPhrase = $(lookAndFeel.searchBox).val();
                $.prevSearchPhrase = "";
				// delay untill user stops writing
                setTimeout(fetchBizJSON, $.fn.searchDelay, 'Warszawa');
            }
        }
		
        var resultFieldEvents = {
            mouseenter: function(event){
                selectResult(getIndex(event));
            },
            mouseleave: function(event){
//                unselectResult(getIndex(event));
            },
            click: function(event){
                //acceptSelected(getIndex(event));
				hidePopup();
				$.prevSearchPhrase = $(lookAndFeel.searchBox).val();
            }
        }
		
		function getIndex(event){
			var arr = $(event.currentTarget).attr('id');
			return arr[arr.length-1];
		}
        
        function renderSearchHint(response){
            $(lookAndFeel.resultsArea).empty();
            $(lookAndFeel.resultsArea).css('display', 'block');
            $(lookAndFeel.resultsArea).css('z-index', 100)
            $(lookAndFeel.resultsArea).css('cursor', 'pointer')
            resultCount = response.result.length
            currResults = response.result;
			var results = $(lookAndFeel.resultsArea);
            $.each(response.result, function(i, biz){
				var div = $("<div/>").attr('id', lookAndFeel.resultFieldPrefix.substring(1) + i).appendTo(results);
				
				var photo = $("<div/>").attr('className', "photo").appendTo(div);
	                var photo_id = biz.photo ? biz.photo : 'building';
					$("<img/>").attr('src', "{% biz_thumbnail_js '" + photo_id + "' %}").appendTo(photo);
                
				$("<div/>").attr('className', 'name').html(biz.name).appendTo(div);
				$("<div/>").attr('className', 'address').html(biz.address).appendTo(div);
				$("<div/>").attr('className', 'city').html(biz.city).appendTo(div);
				
				div.bind(resultFieldEvents);
            })
        }
        
        function fetchBizJSON(where){
            var what = $(lookAndFeel.searchBox).val();
            if (what == $.lastSearchPhrase && $.prevSearchPhrase != what) {
                if (what != "") {
					$.prevSearchPhrase = what;
				}
                $.getJSON("/json/business/list/", {
                    'extend': 1,
                    'name': what,
                    'where': searchParams["where"],
                }, function(response){
                    renderSearchHint(response);
                });
            }
        }
        
        function selectResult(i){
            $(lookAndFeel.resultFieldPrefix + resultSelected).removeClass(lookAndFeel.selectClass.substring(1));
            $(lookAndFeel.resultFieldPrefix + i).addClass(lookAndFeel.selectClass.substring(1));
            $(lookAndFeel.searchBox).val($(lookAndFeel.resultFieldPrefix + i).find("div[class='name']").text());
            resultSelected = i;
            $(lookAndFeel.searchResultId).val(currResults[i].id);
        }
        
        function unselectResult(i){
            $(lookAndFeel.resultFieldPrefix + i).removeClass(lookAndFeel.selectClass.substring(1));
            $(lookAndFeel.searchBox).val($.prevSearchPhrase);
        }
        
        function hidePopup(){
//            $(lookAndFeel.resultsArea).empty();
            $(lookAndFeel.resultsArea).css('display', 'none');
        }
		
		var HTML = {
			renderHidden : function(orygId, oryg){
				oryg.attr('id', '');
				var hidd = $("<input/>").attr({
					id: orygId,
					type : "hidden",
					value : ''
				}).insertAfter( oryg );
				return hidd;
			},
			renderSearchField : function(element){
				var id = lookAndFeel.searchBox.substring(1);
				var div = $("<input/>").attr({
					id: id,
					type : "text",
					autocomplete : "off"
				}).appendTo( element );
				return div;
			},
			renderMainDiv : function(element){
				var div = $("<div/>").attr('id', lookAndFeel.mainDiv.substring(1)).insertAfter( element );
				return div;
			},
			renderResultsBox : function(element){
				var div = $("<div/>").attr('id', lookAndFeel.resultsArea.substring(1)).appendTo( element );
            	div.bind({
					mouseleave: function(){
						$(lookAndFeel.searchBox).val($.prevSearchPhrase);
//						$(lookAndFeel.searchResultId).val("");
					},
					blur : function(){
						hidePopup();
					}
				});
				return div;
			}	
		};
		
		function setLookAndFeel(ownerName){
				lookAndFeel.mainDiv = "#" + ownerName + "-intellisearch-main",
				lookAndFeel.selectField = "#" + ownerName,
	            lookAndFeel.searchBox = "#" + ownerName + "-intellisearch-search",
	            lookAndFeel.searchResultId = "#" + ownerName,
	            lookAndFeel.resultsArea = "#" + ownerName + "-intellisearch-results",
	            lookAndFeel.resultFieldPrefix = "#" + ownerName + "-intellisearch-result-",
	            //lookAndFeel.selectClass =  "#" + ownerName + "-intellisearch-selected",
				lookAndFeel.selectClass = ".selected"
		}
		
		// run function
        return this.each(function(){
			//Hide the selectBox
			$(this).css("display", "none");

			var id = $(this).attr("id");
			
			if( "" === id ) { 
				return;
			}
			
			setLookAndFeel(id);
			
			var main = HTML.renderMainDiv(this);
			var searchField = HTML.renderSearchField(main);
			var results = HTML.renderResultsBox(main);
			var hidd = $(this);//HTML.renderHidden(id, $(this));
			searchField.bind(searchBoxEvents);
        });
    };
})(jQuery);
