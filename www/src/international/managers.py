from django.db import models
from django.contrib.gis.db import models as geomodels
from django.utils import translation

class TransManager(geomodels.GeoManager):
    def get_query_set(self):
        return super(TransManager, self).get_query_set().extra(
                    select={'userlang': "lang = '" + translation.get_language() + "'"})