from django.utils import translation
from django.http import HttpResponse
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

SUPPORTED_LOCALES = dict(settings.LANGUAGES)

def get_path_info(request):
    pathinfo = request.META.get('REQUEST_URI','')
    if pathinfo[-1] != '/':
        pathinfo = pathinfo + '/'
    return pathinfo

class SubdomainLangMiddleware:
    def process_request(self, request):
        if settings.DEBUG:
            return
        """Parse out the subdomain from the request"""
        host = request.META.get('HTTP_X_FORWARDED_HOST', '')
        host_s = host.replace('www.', '').split('.')
        
        if (host == "neib.org" or host == "www.neib.org") and request.META.get('PATH_INFO') != '/google24afc633668495b8.html':
            locale = None
            if request.META.has_key('HTTP_ACCEPT_LANGUAGE'):
                lang = request.META['HTTP_ACCEPT_LANGUAGE'].split(',')[0]
                if lang in SUPPORTED_LOCALES:
                    locale = lang
            if not locale:
                locale = settings.LANGUAGE_CODE
                
            response = HttpResponse(content="", status=303)
            response["Location"] = "http://" + locale[:2] + ".neib.org" + get_path_info(request)
            return response
        
        
        if not host_s[0] in SUPPORTED_LOCALES:
            response = HttpResponse(content="", status=303)
            response["Location"] = "http://en.neib.org" + get_path_info(request)
            return response

        lang = host_s[0]
        translation.activate(lang)
