# Django settings for dood project.
from os import path

DEBUG = False
TEMPLATE_DEBUG = DEBUG

PROJECT_PATH = path.realpath(path.dirname(__file__))

ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)

MANAGERS = ADMINS

DATABASE_ENGINE = 'mysql'           # 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
DATABASE_NAME = 'dood'             # Or path to database file if using sqlite3.
DATABASE_USER = 'kidzik'             # Not used with sqlite3.
DATABASE_PASSWORD = 'aw3dw1'         # Not used with sqlite3.
DATABASE_HOST = 'dood.pl'             # Set to empty string for localhost. Not used with sqlite3.
DATABASE_PORT = ''             # Set to empty string for default. Not used with sqlite3.

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Warsaw'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'pl'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = PROJECT_PATH + '/static/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/media/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'xw*a*k9h+2b3m574jmr97cv6r=1-!k^rna=n2mz7(1#3qm35!-'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.load_template_source',
    'django.template.loaders.app_directories.load_template_source',
    'django.template.loaders.eggs.load_template_source',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.core.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
    "django.core.context_processors.csrf",
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
)

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    PROJECT_PATH + '/templates',
    PROJECT_PATH + '/templates/messages',
    PROJECT_PATH + '/invite/templates',
    PROJECT_PATH + '../src/templates',
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.gis',
    'django.contrib.sitemaps',
    'business',
    'community',
#    'invite',
#    'blog',
) 

AUTH_PROFILE_MODULE = 'community.Profile'

DEFAULT_CHARSET = 'utf-8'

DEFAULT_FROM_EMAIL = 'biuro@dood.pl'

DEFAULT_PAGE_TITLE = 'dood.pl'

PHOTOS_PATH = 'photos/'
PHOTOS_URL = 'photos/'

SERIALIZATION_MODULES = {
    'json2': 'serializers.json'
}
FIRST_DAY_OF_WEEK = 1

LOGIN_URL = '/profil/zaloguj/'
LOGIN_REDIRECT_URL = '/start/'

ACCOUNT_ACTIVATION_DAYS = 7

GOOGLE_API_KEY =''

try:
    from localsettings import *
except ImportError:
    pass
