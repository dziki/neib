'''
Created on 2010-02-12
'''
from django.conf.urls.defaults import *
import os.path
from django.contrib import admin
from django.views.generic.simple import redirect_to

from doodadmin.views import *
from doodadmin.business_admin.views import *
from doodadmin.business_admin.views import * 
from doodadmin.double_admin.views import *

urlpatterns = patterns('',
    url(r'^$', 'django.contrib.auth.views.login', {'template_name': 'doodadmin/login.html'}),
    url(r'^start/$', admin_start, {}, 'doodadmin_start'),
    url(r'^business/start/$', business_list, {}, 'doodadmin_business_list'),
    url(r'^business/$', business_list, {}, 'doodadmin_business'),
    url(r'^business/details/(?P<business_id>[-\d]+)/$', business_details, {}, 'doodadmin_business_details'),
    url(r'^double/$',double_list, {}, 'doodadmin_double'),
    url(r'^double/merge/$',double_merge, {}, 'doodadmin_double'),
    url(r'^profil/wyloguj/$', 'django.contrib.auth.views.login', {'template_name': 'doodadmin/login.html'}, 'doodadmin_profile_logout'),
    url(r'^profil/logowanie/$', admin_start, {}, 'doodadmin_profile_logged'),
    url(r'^profil/$', admin_start, {}, 'doodadmin_profile_logged'),
)
