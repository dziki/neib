'''
Created on 2010-02-12
'''

from django.contrib.auth import logout
from django.shortcuts import render_to_response
from properties.models import Property
from django.template import RequestContext
from ai.navigation import *
from doodadmin.decorators import superuser_only

@superuser_only
def admin_start(request):
    last_double_count =  Property.objects.filter(key__exact="last_double_count")
    last_activity = Property.objects.filter(key__exact="last_activity")
    pending_doubles=Similarity.objects.filter(checked__exact=False).count()
    pending_verify=(Business.objects.filter(verified__exact=0)).count()
    links = create_initial_links()
    pending_moderate=(Moderation.objects.filter(checked__exact=False).count())
    first_not_verified=(Business.objects.order_by('date').filter(verified__exact=0).reverse()[:1])
    if len(first_not_verified)>0:
        first_not_verified=first_not_verified[0].id
    return render_to_response('doodadmin/admin_start.html',
                              context_instance=RequestContext(request,
                              {'last_activity':last_activity,
                               'pending_doubles':pending_doubles,
                               'pending_verify':pending_verify,
                               'pending_moderate':pending_moderate,
                               'links':links,
                               'last_double_count':last_double_count,})
                               )


def logout_view(request):
    logout(request)
            