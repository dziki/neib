FLAG_DICTIONARY = {
    'spam': 1,
    'bookmark': 2,
    'funny': 3,
    'cool': 4,
    'interesting': 5,
    'useful': 6,
    'master': 8,
    'elite': 10,
}
FLAG_CHOICES = tuple(FLAG_DICTIONARY)

flags_dict_pl = {
    'mistrzowska': 'master',
    'uzyteczna': 'useful',
    'fajna': 'cool',
    'interesujaca': 'interesting',
    'smieszna': 'funny',
    'spam': 'spam',
    'ulubiony': 'bookmark',
    'obserwuj': 'bookmark',
    'elita': 'elite',
}

def flag_translator(flag_name):
    if flag_name is None:
        return None
    return FLAG_DICTIONARY[flags_dict_pl[flag_name]]