from django.contrib.gis.db import models as geomodels
from django.contrib.contenttypes.models import ContentType

from flagging.flags import FLAG_DICTIONARY

class FlagManager(geomodels.Manager):
    """ Get best businesses in your neighborhood (TODO) """
    def get_flags(self, object, flag):
        ctype = ContentType.objects.get_for_model(object)
        
        falgs = self.filter(content_type__pk=ctype.id, object_id=object.id, flag=FLAG_DICTIONARY[flag])
        return falgs

    def user_flaged_by(self, user, flag):
        ctype = ContentType.objects.get_for_model(user)
        
        falgs = self.filter(content_type__pk=ctype.id, object_id=user.id, flag=FLAG_DICTIONARY[flag])
        return falgs
    
    def num_of_flags(self, object, flag):
        return self.get_flags(object, flag).count()

    def user_flags(self, user, flag, Model = None):
        if Model:
            ctype = ContentType.objects.get_for_model(Model)
        return self.filter(content_type__pk=ctype.id, user=user, flag=FLAG_DICTIONARY[flag])
