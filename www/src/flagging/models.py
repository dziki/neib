"""
Models for generic flags.
"""
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.contrib.auth.models import User

from flagging.managers import FlagManager
from flagging.flags import *
from date.models import TimedModel

def get_flagged_objects(flag_name, user, model):
    business_ct = ContentType.objects.get_for_model(model)
    flags = Flag.objects.filter(flag=flag_translator(flag_name), content_type=business_ct)
    if user is not None:
        flags = flags.filter(user=user)
    flags = flags.values_list('object_id')

    return business_ct.model_class().objects.filter(pk__in=flags)

# Create your models here.
class Flag(TimedModel):
    """ A flag on the item. """
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    flag = models.PositiveSmallIntegerField()
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()

    user = models.ForeignKey(User, blank=False, null=False, verbose_name="Uzytkownik")

    item = None

    objects = FlagManager()

    class Meta:
        ordering = ["flag"]

    def get_from_name(self, name):
        self.flag = FLAG_DICTIONARY[name]
        return self.flag

    def save(self, *args, **kwargs):
        """ user attaches this flag (to item selt.item) """
        if self.content_object is None or self.flag is None:
            return

        ctype = ContentType.objects.get_for_model(self.content_object)
        self.content_type = ctype

        flags = Flag.objects.filter(content_type__pk=self.content_type.id, object_id=self.content_object.id, user=self.user.id, flag=self.flag)
        if len(flags) != 0:
            return -106
            # TODO error table

        super(Flag, self).save(*args, **kwargs)
        return 0

    def get_users(self):
        """ get all user who set flag self.flag to item self.item """
        flags = Flag.objects.filter(content_type__pk=self.content_type.id,
                                    object_id=self.content_object.id,
                                    flag=self.flag).select_related('user__profile__gallery__representative')

        return [flag.user for flag in flags]

