# -*- coding: utf-8 -*-
import datetime as dtime

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth import logout, login, authenticate

from django.contrib.auth.decorators import login_required

from ownership.models import Request
from business.views import business_detail
from business.models import InfoField, InfoFieldValue, Review, Avg

from notify.models import notify_user

from business.forms import BusinessOwnerForm, HoursFormSet

from dynamicforms.forms import DynamicForm

from event.forms import *

from django.shortcuts import redirect
from gallery.forms import PhotoFormShort

from adverts.models import SubscriptionOrder

from django.views.decorators.csrf import csrf_view_exempt

from django.contrib.auth.forms import AuthenticationForm

from registration.forms import RegistrationForm

def ownership_intro(request):
    user = request.user

    if request.POST:
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = authenticate(username=form.cleaned_data["username"], password=form.cleaned_data["password"])
            login(request, user)
    else:
        form = AuthenticationForm()

    if user.is_authenticated():
        if user.business_set.count() > 0:
            businesses = user.business_set.all()
            return redirect("ownership_home", businesses[0].slug)
    
    return render_to_response('ownership/start.html',
                              {"form": form},
                               context_instance=RequestContext(request))

def ownership_register(request):
    if request.POST:
        form = RegistrationForm(data=request.POST)
        if form.is_valid():
            form.save()
    else:
        form = RegistrationForm()

    return render_to_response('ownership/register.html',
                              {"form": form},
                               context_instance=RequestContext(request))

def ownership_logout(request):
    logout(request)
    return redirect('ownership_intro')

def business(f):
    def new_func(request, business_slug):
        business = Business.objects.get(slug=business_slug, owner = request.user)
        businesses = Business.objects.filter(owner = request.user).exclude(slug = business_slug)
        return f(request, business, businesses)
    return new_func

@login_required()
@business
def ownership_home(request, business, businesses):
    if not request.user.is_authenticated():
        return
    
    r = []
    for i in xrange(1,6):
        r.append(business.review_set.filter(rating=i).count())
    mx = max(r)
    if sum(r) > 0:
        r = map(float,r)
        r = map(lambda x: int(((x/mx)*100)), r)
        r = map(str,r)
        r = ",".join(r)

    pos = {}
    if business.rating:
        for c in business.categories.all():
            lower = c.business_set.filter(rating__lt=business.rating).count()
            pos.update({c.name:lower})
    reviews_avg = business.rating
    reviews_count = business.review_set.count()

    #visits_count = UserActivity.objects.get_for_business(business).count()

    #visits_last_month = UserActivity.objects.get_business_monthly_stats(business, now.year, now.month)

    return render_to_response('ownership/home.html',
                              {"business":  business,
                               "businesses":businesses,
                               "rating":    r,
                               "max":       mx,
                               "position":  pos,
                               "reviews_avg":reviews_avg,
                               "reviews_count":reviews_count,
                               "visits_count":0,
                               "visits_last_month":0,
                               },
                               context_instance=RequestContext(request))


@login_required()
def ownership_request(request, business_slug):
    business = Business.objects.get(slug=business_slug)

    return render_to_response('ownership/request.html',
        context_instance=RequestContext(request,
            {"business": business}))

@login_required()
def ownership_request_sent(request, business_slug):
    business = Business.objects.get(slug=business_slug)

    req = Request(business = business, user = request.user)
    req.save()

    notify_user(request, u"Zgłoszenie zostało wysłane", None)

    return business_detail(request, business_slug)

@login_required()
def business_statistics(request, business_slug):
    business = Business.objects.get(slug=business_slug, owner = request.user)
    businesses = Business.objects.filter(owner = request.user).exclude(slug = business_slug)

    return render_to_response('ownership/statistics.html',
        context_instance=RequestContext(request,
            {"business": business,
             "businesses": businesses, }))

@login_required()
def business_details_edit(request, business_slug):
    business = Business.objects.get(slug=business_slug, owner = request.user)
    businesses = Business.objects.filter(owner = request.user).exclude(slug = business_slug)

    if request.POST:
        form = BusinessOwnerForm(request.POST, instance=business)
        if form.is_valid():
            form.save()
            notify_user(request, 'Dane zostały zaktualizowane, dziękujemy.', None)
            return redirect("ownership_home", business_slug=business.slug)
    else:
        form = BusinessOwnerForm(instance=business)

    return render_to_response('ownership/details.edit.html',
        context_instance=RequestContext(request,
            {"ownership": True,
             "business": business,
             "businesses": businesses,
             "form": form}))

@login_required()
def business_hours_edit(request, business_slug):
    business = Business.objects.get(slug=business_slug, owner = request.user)
    businesses = Business.objects.filter(owner = request.user).exclude(slug = business_slug)

    if request.POST:
        hfs = HoursFormSet(business, request.POST)
        if hfs.is_valid():
            hfs.save()
            notify_user(request, 'Dane zostały zaktualizowane, dziękujemy.', None)
    else:
        hfs = HoursFormSet(business)


    return render_to_response('ownership/hours.edit.html',
        context_instance=RequestContext(request,
            {"business": business,
             "businesses": businesses,
             "formset": hfs.formset}))

@login_required()
def business_extrafields_edit(request, business_slug):
    business = Business.objects.get(slug=business_slug, owner = request.user)
    businesses = Business.objects.filter(owner = request.user).exclude(slug = business_slug)

    form = DynamicForm(InfoField, InfoFieldValue, instance = business) # Create the form

    if request.POST:
        form.set_data(request.POST)

    return render_to_response('ownership/details.edit.html',
                              {"business": business,
                               "businesses": businesses,
                               "form": form,},
                               context_instance=RequestContext(request))


@login_required()
@business
def business_main_review(request, business, businesses):
    if 'review' in request.REQUEST:
        review = request.REQUEST.get('review')
        if review == "none":
            business.main_review = None
        else:
            review = Review.objects.get(id=int(review))
            business.main_review = review
        business.save()

    return render_to_response('ownership/main.review.html',
                              {"business": business,
                               "businesses": businesses},
                               context_instance=RequestContext(request))

@login_required()
@business
def business_promotions(request, business, businesses):
    promotions = Promotion.objects.filter(business=business)

    return render_to_response('ownership/promotions.html',
                              {"business": business,
                               "businesses": businesses,
                               "promotions": promotions},
                               #"form": form,},
                               context_instance=RequestContext(request))

@login_required()
@business
def business_photo(request, business, businesses):
    user_photos = business.gallery.photo_set.filter(user=request.user)
    other_photos = business.gallery.photo_set.exclude(user=request.user)

    if 'main' in request.REQUEST:
        main = request.REQUEST.get('main')
        gallery = business.gallery

        photo = gallery.photo_set.filter(id=main)
        if photo:
            gallery.representative = photo[0]
            gallery.save()

            notify_user(request, "Zdjęcie główne zapisane","")
        else:
            notify_user(request, "Nie udało się zapisać głównego zdjęcia","")

    return render_to_response('ownership/photo.html',
                              {"business": business,
                               "businesses": businesses,
                               "user_photos": user_photos,
                               "other_photos": other_photos,
                               "photo_form": PhotoFormShort()},
                               context_instance=RequestContext(request))

@login_required()
@business
def business_promotion_add(request, business, businesses):
    if request.POST:
        form = PromotionForm(request.POST)
        if form.is_valid():
            form.instance.business = business
            form.instance.user = request.user
            form.save()
            notify_user(request, 'Promocja dodana, dziękujemy.', None)
            return redirect('business_promotions', business_slug=business.slug)
    else:
        form = PromotionForm()

    return render_to_response('ownership/promotion.add.html',
                              {"business": business,
                               "businesses": businesses,
                               "form": form},
                               context_instance=RequestContext(request))

@login_required()
@business
def business_subscription(request, business, businesses):

    return render_to_response('ownership/subscription.html',
                              {"business": business,
                               "businesses": businesses,
                               },
                               context_instance=RequestContext(request))

@login_required()
def business_subscription_buy(request, business_slug, subscription, months):

    price = 85
    if subscription == "pro":
        price = 150

    business = Business.objects.get(slug=business_slug, owner = request.user)
    businesses = Business.objects.filter(owner = request.user).exclude(slug = business_slug)

    months = int(months)
    quantity = months
    if (months == 10):
        months = months+2
    if quantity <= 0:
        raise Exception

    price_netto = quantity * price
    price_brutto = float(price_netto) * 1.22

    so = SubscriptionOrder(business = business, type = subscription, months = months, price = price)
    so.save()

    return render_to_response('ownership/subscription.buy.html',
                              {"business": business,
                               "businesses": businesses,
                               "months": months,
                               "subscription": subscription,
                               "quantity": quantity,
                               "price_brutto": price_brutto,
                               "price_netto": price_netto,
                               "price_brutto_gr": int(price_brutto * 100),
                               "order_id": so.id,
                               },
                               context_instance=RequestContext(request))

@csrf_view_exempt
@login_required()
def business_subscription_buy_error(request, business_slug):
    business = Business.objects.get(slug=business_slug, owner = request.user)
    businesses = Business.objects.filter(owner = request.user).exclude(slug = business_slug)

    return render_to_response('ownership/subscription.buy.error.html',
                              {"business": business,
                               "businesses": businesses,
                               },
                               context_instance=RequestContext(request))


@csrf_view_exempt
@login_required()
def business_subscription_buy_ok(request, business_slug):
    business = Business.objects.get(slug=business_slug, owner = request.user)
    businesses = Business.objects.filter(owner = request.user).exclude(slug = business_slug)

    sid = request.POST["p24_session_id"]
    sidstr = int(sid[5:])
    so = SubscriptionOrder.objects.get(id = sidstr)
    so.complete()

    return render_to_response('ownership/subscription.buy.ok.html',
                              {"business": business,
                               "businesses": businesses,
                               },
                               context_instance=RequestContext(request))


@login_required()
def business_promotion_del(request, business_slug, promotion_id):
    promotion = Promotion.objects.get(id=promotion_id)
    promotion.delete()

    return redirect('business_promotions', business_slug=business_slug)

@login_required()
def business_promotion_edit(request, business_slug, promotion_id):
    business = Business.objects.get(slug=business_slug, owner = request.user)
    businesses = Business.objects.filter(owner = request.user).exclude(slug = business_slug)
    promotion = Promotion.objects.get(id=promotion_id)

    if request.POST:
        form = PromotionForm(request.POST, instance=promotion)
        if form.is_valid():
            form.instance.business = business
            form.instance.user = request.user
            form.save()
            notify_user(request, 'Promocja zmieniona, dziękujemy.', None)
            return redirect('business_promotions', business_slug=business_slug)
    else:
        form = PromotionForm(instance=promotion)

    return render_to_response('ownership/promotion.add.html',
                              {"business": business,
                               "businesses": businesses,
                               "form": form},
                               context_instance=RequestContext(request))