"""
URLConf for ownership app

"""

from django.conf.urls.defaults import *
from django.contrib import admin

from ownership.views import *

admin.autodiscover()

urlpatterns = patterns('django.views.generic.simple',
    ((r'^logout/$'), ownership_logout, {}, 'ownership_logout'),
    ((r'^$'), ownership_intro, {}, 'ownership_intro'),
    ((r'^ownership_register/$'), ownership_register, {}, 'ownership_register'),
    
    ((r'^(?P<business_slug>[-\w]+)/$'), ownership_home, {}, 'ownership_home'),
    ((r'^(?P<business_slug>[-\w]+)/claim/$'), ownership_request, {}, 'ownership_request'),
    ((r'^(?P<business_slug>[-\w]+)/claim/sent/$'), ownership_request_sent, {}, 'ownership_request_sent'),
    
    ((r'^(?P<business_slug>[-\w]+)/stats/$'), business_statistics, {}, 'business_statistics'),
    ((r'^(?P<business_slug>[-\w]+)/edit/$'), business_details_edit, {}, 'business_details_edit'),
    ((r'^(?P<business_slug>[-\w]+)/details/$'), business_extrafields_edit, {}, 'business_extrafields_edit'),
    ((r'^(?P<business_slug>[-\w]+)/hours/$'), business_hours_edit, {}, 'business_hours_edit'),
    ((r'^(?P<business_slug>[-\w]+)/review/$'), business_main_review, {}, 'business_main_review'),
    ((r'^(?P<business_slug>[-\w]+)/promotions/$'), business_promotions, {}, 'business_promotions'),
    ((r'^(?P<business_slug>[-\w]+)/promotions/add/$'), business_promotion_add, {}, 'business_promotion_add'),
    ((r'^(?P<business_slug>[-\w]+)/promotions/edit/(?P<promotion_id>[-\w]+)/$'), business_promotion_edit, {}, 'business_promotion_edit'),
    ((r'^(?P<business_slug>[-\w]+)/promotions/remove/(?P<promotion_id>[-\w]+)/$'), business_promotion_del, {}, 'business_promotion_del'),
    ((r'^(?P<business_slug>[-\w]+)/photos/$'), business_photo, {}, 'business_photo'),
    ((r'^(?P<business_slug>[-\w]+)/subscription/$'), business_subscription, {}, 'business_subscription'),
    ((r'^(?P<business_slug>[-\w]+)/subscription/(?P<subscription>[-\w]+)/(?P<months>[-\w]+)/$'), business_subscription_buy, {}, 'business_subscription_buy'),
    
    ((r'^(?P<business_slug>[-\w]+)/subscription/error/$'), business_subscription_buy_error, {}, 'business_subscription_buy_error'),
    ((r'^(?P<business_slug>[-\w]+)/subscription/ok/$'), business_subscription_buy_ok, {}, 'business_subscription_buy_ok'),
    
)