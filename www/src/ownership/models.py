from django.db import models
from date.models import TimedModel
from business.models import Business
from django.contrib.auth.models import User

class Request(TimedModel):
    business = models.ForeignKey(Business, verbose_name="Lokal")
    user = models.ForeignKey(User, verbose_name="Wlasciciel")

    accepted = models.BooleanField(verbose_name="Potwierdzony", default=0)

    def save(self, *args, **kwargs):
        super(Request, self).save()
                
        if self.accepted is True:
            self.business.owner = self.user
            self.business.save()

        if self.accepted == False and self.business.owner == self.user:
            self.business.owner = None
            self.business.save()