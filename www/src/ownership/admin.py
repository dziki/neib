from django.contrib import admin
from ownership.models import *

class RequestAdmin(admin.ModelAdmin):
    list_display = ('business','user','accepted')

admin.site.register(Request, RequestAdmin)
