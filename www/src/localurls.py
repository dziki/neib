from django.conf.urls.defaults import patterns
import settings

__author__ = 'kalinskia'

local_patterns = patterns('',

    (r"static/(?P<path>.*)$", "django.views.static.serve", {
            "document_root": settings.MEDIA_ROOT,
    }),
)