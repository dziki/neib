#-*- coding: utf-8 -*-
'''
Created on 2010-04-15

@author: Adam Kaliński
'''

from django.forms import SplitDateTimeWidget

class EventTimeWidget(SplitDateTimeWidget):
    
    def __init__(self, *args, **kwargs):
        super(EventTimeWidget,self).__init__(*args, **kwargs)
    class Media:
        js = ('js/widgets/event.time.widget.js',)
        css = {
               'all': ('style/screen/widgets/event.time.widget.css',)
              }
        
class EventDateWidget(SplitDateTimeWidget):
    
    def __init__(self, *args, **kwargs):
        super(EventDateWidget,self).__init__(*args, **kwargs)
    class Media:
        js = ('js/widgets/event.time.widget.js',)
        css = {
               'all': ('style/screen/widgets/event.time.widget.css',)
              }