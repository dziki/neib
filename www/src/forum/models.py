# -*- coding: utf-8 -*- 
"""
Models for generic flags.
"""
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
from django.contrib.contenttypes import generic
from django.db.models.signals import post_save

from location.models import GeoModel
from date.models import TimedModel
from location.managers import GeoModelManager
from notification import models as notification

from django.utils.translation import ugettext_lazy as _
from international.managers import TransManager

class Category(models.Model):
    """ Thread category. If parent is null then that is topmost category """
    name = models.CharField(max_length=100, verbose_name=_('Name'))

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

class PostManager(GeoModelManager):
    def get_posts(self, object):
        ctype = ContentType.objects.get_for_model(object)
        
        return self.filter(content_type__pk=ctype.id, object_id=object.id).filter(active=1)

    def get_in(self, objects):
        if len(objects) == 0:
            return []
        
        ctype = ContentType.objects.get_for_model(objects[0])
        
        return self.filter(content_type__pk=ctype.id, object_id__in=objects)

    def add_comment(self, obj, body, user):
        p = Post()
        p.set_target(obj)
        p.body = body
        p.user = user
        return p.save()


class Post(TimedModel):
    thread = generic.GenericForeignKey('content_type', 'object_id')

    user = models.ForeignKey(User, related_name='forum_post_set')

    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    
    body = models.TextField(verbose_name=_('Opinion'))
    
    objects = PostManager()
    
    active = models.BooleanField(verbose_name=_('Active'), default=1)
    lang = models.CharField(max_length=6, verbose_name=_("Language"), null=False, blank=False, default='pl')

    def save(self, *args, **kwargs):

        super(Post, self).save(*args, **kwargs)

    def set_target(self, object):
        self.content_type = ContentType.objects.get_for_model(object)
        self.object_id = object.id

    class Meta:
        ordering = ["-id"]

#noinspection PyUnusedLocal
def post_post_save(sender, **kwargs):
    """
        
    """
    instance = kwargs['instance']
    if instance.active == 0:
        return

    # find other users who commented given post
    commented_by = Post.objects.filter(content_type__pk=instance.content_type.id, object_id=instance.object_id).values_list('user_id', flat=True).distinct()
    commented_by = set([k for k in commented_by])
    if instance.thread.__class__.__name__ == 'Review':
        commented_by = (commented_by | set([instance.thread.user.id,])) - set([instance.user.id,])
    notification.send(User.objects.filter(id__in = commented_by), "comment_new", {'comment': instance,}, True, 10)

post_save.connect(post_post_save, sender=Post)

class Thread(GeoModel):
    category = models.ForeignKey(Category, verbose_name=_('Category'), null=True, blank = True)
    name = models.CharField(max_length=60, verbose_name=_('Subject'))
    lang = models.CharField(max_length=6, verbose_name=_("Language"), null=False, blank=False, default='pl')

    objects = TransManager()

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    def first_post(self):
        posts = Post.objects.get_posts(self).reverse()
        try:
            return posts[0]
        except IndexError:
            return None

    def last_update(self):
        return Post.objects.get_posts(self)[0].date

    def replies(self):
        return Post.objects.get_posts(self).count() - 1

    @models.permalink
    def get_absolute_url(self):
        return ('forum_thread_details', (), {
            'thread_id': self.id})

    class Meta:
        ordering = ["-id"]
        
def add_comments(reviews):
    reviews_dict = dict([(review.id, review) for review in reviews])
    for review in reviews:
        review._comments = []
    
    posts = Post.objects.filter(content_type__id=25,object_id__in=[review.id for review in reviews]).order_by('id').select_related('user__profile__gallery__representative')
    post_dict = {}
    
    for post in posts:
        post_dict.setdefault(post.object_id, []).append(post)
    for id, related_items in post_dict.items():
        reviews_dict[id]._comments = related_items
