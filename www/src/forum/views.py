from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.views import login as login_view

from forum.forms import *

from location.models import get_position_from_address

from django.contrib.auth.decorators import login_required


def forum_user_list(request, username):
    user_id = username
    user = User.objects.get(id = user_id)

    posts = Post.objects.all().filter(user = user, content_type = ContentType.objects.get_for_model(Thread))
    ids = [post.object_id for post in posts]
    threads = Thread.objects.all().filter(id__in = ids).distinct(True)[:10]

    return render_to_response('community/talks.html',
        context_instance=RequestContext(request,
            {"threads": threads,
             "selecteduser": user
             }))

def forum_thread_list(request, category_id=None):
    area = request.session['area']
    threads = Thread.objects.all().filter(position__within = area.poly)
    categories = Category.objects.all()

    if category_id is not None:
        threads = threads.filter(category__id = category_id)

    return render_to_response('forum/thread.list.html',
        context_instance=RequestContext(request,
            {"threads": threads,
             "categories": categories,
             }))

def forum_thread_details(request, thread_id):
    thread = Thread.objects.get(id=thread_id)
    posts = Post.objects.get_posts(thread)
    categories = Category.objects.all()

    if request.POST:
        form = PostForm(request.POST)
        if form.is_valid():
            form.instance.set_target(thread)
            if request.user.is_authenticated():
                form.instance.user = request.user
                form.save()
                form = PostForm()
            else:
                return login_view(request)
    else:
        form = PostForm()

    return render_to_response('forum/thread.details.html',
        context_instance=RequestContext(request,
            {"posts": posts,
             "thread": thread,
             "form": form,
             "categories": categories,
             }))

@login_required()
def forum_thread_add(request):
    if request.POST:
        form = ThreadForm(request.POST)
        form_post = PostForm(request.POST)
        if form.is_valid() and form_post.is_valid():
            am = get_position_from_address(form.cleaned_data['location'])
            form.instance.position = am.position
            thread = form.save()
            return forum_thread_details(request, thread.id)
    else:
        form = ThreadForm(initial={'location': request.session['area'].name});
        form_post = PostForm()

    return render_to_response('forum/thread.add.html',
        context_instance=RequestContext(request,
            {"form": form,
             "form_post": form_post,}))