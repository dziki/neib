from django.conf.urls.defaults import patterns
from forum.views import *

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    ((r'^new/$'), forum_thread_add, {}, 'forum_thread_add'),
    ((r'^$'), forum_thread_list, {}, 'forum_thread_list'),
    ((r'^(?P<thread_id>[-\d]+)/$'), forum_thread_details, {}, 'forum_thread_details'),
    ((r'^c/(?P<category_id>[-\d]+)/$'), forum_thread_list, {}, 'forum_category'),
)