# -*- coding: utf-8 -*- 
"""
Profile form. There will be more fields. TODO
"""
from django import forms
from django.forms import ModelForm
from forum.models import *
from django.utils.translation import ugettext_lazy as _

class ThreadForm(ModelForm):
    class Meta:
        model = Thread
        fields = (
            'name',
            'category',
        )
    location = forms.CharField(max_length = 100, label=_("Location"))

class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = (
            'body',
        )
