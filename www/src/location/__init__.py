from location.models import Area
from django.http import Http404

def update_area(request, city_slug):
    try:
        area = Area.objects.exclude(parent=None).get(slug=city_slug)
    except Area.DoesNotExist:
        raise Http404
    
    request.session['area'] = area
    request.session['country'] = area.parent

    return area
