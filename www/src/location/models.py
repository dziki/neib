"""
Models for generic flags.
"""
from django.db import models
from django.contrib.gis.db import models as geomodels
from django.contrib.gis.geos import GEOSGeometry
from date.models import TimedModel

from location.managers import GeoModelManager

from geopy import geocoders

from settings import GOOGLE_API_KEY

class AddressMapping(geomodels.Model):
    address = models.CharField(max_length=200, verbose_name="Adres", primary_key = True)
    position = geomodels.PointField(default=GEOSGeometry('POINT(%f %f)' % (0,0)))
    box = geomodels.PolygonField(default=None, null=True)
    
    objects = geomodels.GeoManager()
    
def get_poly(top, right, bottom, left):
    geostr = ('POLYGON (( %s %s, %s %s, %s %s, %s %s, %s %s))' %
                        (right, top, 
                         left, top, 
                         left, bottom, 
                         right, bottom, 
                         right, top,))
    return GEOSGeometry(geostr)
    
def get_position_from_address(address):
    """ Use geopy to convert address to position """
    am, b = AddressMapping.objects.get_or_create(address=address)
    if not b:
        if am.position.x > 1 and am.position.y > 1 and am.box:
            return am
        
    am.address = address
    
    g = geocoders.Google(GOOGLE_API_KEY)

    try:
        res = g.geocode(address, exactly_one=False)
        l = [x for x in res]
        place, (lat, lng), box = l[0]
        am.box = get_poly(box["east"],box["north"],box["west"],box["south"])
    except:
        lat = 0
        lng = 0
        am.box = None

    am.position = GEOSGeometry('POINT(%f %f)' % (lat,lng))
    am.save()

    return am

class GeoModel(TimedModel):
    position = geomodels.PointField(default=GEOSGeometry('POINT(%f %f)' % (0,0)))
    objects = GeoModelManager()
    box = None
    
    def get_position_from_address(self, address):
        """ Use geopy to convert address to position """
        am = get_position_from_address('Polska, ' + address)
        self.position = am.position
        self.box = am.box
        return self.position
    
    def latitude(self):
        #noinspection PyUnresolvedReferences
        return self.position.x
    def longitude(self):
        #noinspection PyUnresolvedReferences
        return self.position.y

    class Meta:
        abstract = True
        
AREA_TYPES = [(1, 'Country'),
              (2, 'State'),
              (3, 'District'),
              (4, 'City'),
              (5, 'Neighbourhood'),
              (6, 'Estate'),
              ]

class Area(geomodels.Model):
    name = geomodels.CharField(max_length=100, verbose_name="Nazwa")
    slug = geomodels.SlugField(max_length=100, verbose_name="Slug", primary_key = True)
    parent = geomodels.ForeignKey('Area', null=True, blank=True)
    type = geomodels.IntegerField('Type', choices=AREA_TYPES)
    
    position = geomodels.PointField(default=GEOSGeometry('POINT(%f %f)' % (0,0)))
    radius = geomodels.FloatField()
    polygon = geomodels.PolygonField(srid=32140, null=True, blank=True)
    
    objects = GeoModelManager()
    
#    def area_set(self):
#        return Area.objects.all().filter(parent = self)
    
    def __str__(self):
        return self.name
    def __unicode__(self):
        return self.name
    
    def child(self):
        child = Area.objects.filter(parent=self)[0]
        return child
    
    def get_poly(self):
        top = self.position.y + self.radius
        right = self.position.x + self.radius
        bottom = self.position.y - self.radius
        left = self.position.x - self.radius

        poly = GEOSGeometry('POLYGON (( %s %s, %s %s, %s %s, %s %s, %s %s))' %
                            (right, top, 
                             left, top, 
                             left, bottom, 
                             right, bottom, 
                             right, top,))        
        return poly
    
    poly = property (get_poly)
    
    def full_name(self):
        ret = self.name
        if self.parent:
            ret = self.parent.name + ", " + ret
        return ret

    class Meta:
        ordering = ["name"]
