from location.models import Area
from django.contrib.gis.geos import GEOSGeometry
from django.shortcuts import redirect
from django.http import HttpResponse

from django.http import Http404

class SetCurrentLocationInSession(object):
    def process_request(self, request):
        if not request.session.get('area', None):
            if request.COOKIES.get('area', None):
                try:
                    request.session['area'] = Area.objects.get(slug=request.COOKIES.get('area'))
                except Area.DoesNotExist:
                    raise Http404
            else:
                city = "warszawa"
                lang = "pl"
                if request.META.has_key('HTTP_ACCEPT_LANGUAGE'):
                    lang = request.META['HTTP_ACCEPT_LANGUAGE'].split(',')[0][:2]
                if lang == "en":
                    city = "warszawa"
                request.session['area'] = Area.objects.get(slug='warszawa')
                
        if request.META.get('PATH_INFO') == '/':
            city = request.session['area'].slug
            if not city in ['warszawa']:
                response = HttpResponse(content="", status=303)
                response["Location"] = '/' + city + '/'
                return response
            
        if request.session['area'].parent == None:
            request.session['area'] = Area.objects.get(slug=request.session['area'].slug)

        request.session['country'] = request.session['area'].parent
        if (request.session['country']  == None):
            request.session['area'] = Area.objects.get(slug='warszawa')
            request.session['country'] = request.session['area'].parent

        return