# -*- coding: utf-8 -*-
from django.conf.urls.defaults import patterns
from django.contrib import admin
from location.views import home_city
from community.views import location_activate

admin.autodiscover()

urlpatterns = patterns('',
    (r'^(?P<location_id>[-\w]+)/set/$', location_activate, {}, 'location_activate'),
    (r'^(?P<city_slug>[-\w]+)/$', home_city, {}, 'home_city'),
)