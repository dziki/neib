from location.models import Area
from django.core.cache import cache

#noinspection PyUnusedLocal
def areas(request):
    cslug = ''
    try:
        country = request.session["country"]
    except KeyError:
        country = Area.objects.get(slug="polska")
    cslug = country.slug
        
    areas = cache.get("areas-" + cslug)
    if not areas:
        areas = Area.objects.filter(parent=country).order_by("name")
        cache.set("areas-" + cslug, list(areas), 60*60*24*7)

    countries = cache.get("countries")
    if not countries:
        countries = Area.objects.filter(parent=None).order_by("name")
        cache.set("countries", list(countries), 60*60*24*7)
        
    return {"areas": areas,
            "countries": countries }