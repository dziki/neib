from django.shortcuts import render_to_response
from django.template import RequestContext
from location.models import Area
from location import update_area

from main.views import home_search
    
def home_city(request, city_slug):
    update_area(request, city_slug)

    resp = home_search(request)
    resp.set_cookie('area',request.session["area"].slug)
    return resp
