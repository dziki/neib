'''

'''
from django.contrib import admin
from location.models import Area

class AreaAdmin(admin.ModelAdmin):
    pass

admin.site.register(Area,AreaAdmin)
