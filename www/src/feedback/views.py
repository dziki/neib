# -*- coding: utf-8 -*-
'''
Created on 2010-05-05 at 10:25:41

@author: kalinskia
'''
from feedback.forms import SearchFeedbackCompactForm, FeedbackForm
from api.shortcuts import api_response_error,api_response_ok
from django.shortcuts import render_to_response
from ajax.shortcuts import render_form_as_p
from django.template.context import RequestContext

def feedback(request):
    if request.method == "POST":
        form = FeedbackForm(request.POST)
        if form.is_valid():
            if request.user.is_authenticated():
                form.instance.user = request.user
            form.instance.path = request.get_full_path()
            form.save()
            return render_to_response('feedback/feedback.done.html',
                              context_instance=RequestContext(request,{}))
        return render_to_response('feedback/feedback.html',
                          context_instance=RequestContext(request,{'form':form}))
            
    else:
        form = FeedbackForm()
        return render_to_response('feedback/feedback.html',
                              context_instance=RequestContext(request,{'form':form}))

def receiveSearchFeedback(request):
    if request.method == "POST":
        form = SearchFeedbackCompactForm(request.POST)
        if form.is_valid():
            form.save(commit=False)
            form.user = request.user;
            form.save()
            return api_response_ok()
        else:
            return api_response_error(form.errors)
    else:
        form = SearchFeedbackCompactForm()
        return render_form_as_p(form)
    