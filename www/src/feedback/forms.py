# -*- coding: utf-8 -*-
'''
Created on 2010-05-05 at 10:49:00

@author: kalinskia
'''

from django import forms
from feedback.models import SearchFeedback,Feedback

class FeedbackForm(forms.ModelForm):
    def clean_email(self):
        email = self.cleaned_data['email']
        if self.cleaned_data['type'] == 'QUESTION' and email == '':
            raise forms.ValidationError(u"Musisz podać swój e-mail jeśli chcesz otrzymać od nas odpowiedź")
        return email
             
    
    class Meta:
        model = Feedback
        exclude = ('path','user')

class SearchFeedbackCompactForm(forms.ModelForm):
    class Meta:
        model = SearchFeedback
        exclude = ('phrase','user','report_date',)