# -*- coding: utf-8 -*-
'''
Created on 2010-05-05 at 10:25:04

@author: kalinskia
'''

from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

FEEDBACK_TYPES = (
                    ('ERROR',u'Błąd'),
                    ('SUGGEST',u'Sugestia'),
                    ('ATT',u'Uwaga'),
                    ('QUESTION',u'Pytanie'),
                  )

class Feedback(models.Model):
    type = models.CharField(choices=FEEDBACK_TYPES, max_length=8, verbose_name=u"Typ")
    feedback = models.TextField(verbose_name=u'Opis')
    path = models.CharField(max_length=256)
    user = models.ForeignKey(User, verbose_name=u"Użytkownik", blank=True, null=True)
    email = models.EmailField(blank=True, verbose_name=u"E-mail")
    report_date = models.DateTimeField(verbose_name=u"Report date and time", default=datetime.now(), editable=False)
    

class SearchFeedback(models.Model):
    phrase = models.CharField(max_length=100, verbose_name=u"Szukana fraza")
    where = models.CharField(max_length=100, verbose_name=u"Przeszukiwana okolica")
    feedback = models.TextField(blank=True, verbose_name=u"Dodatkowe uwagi")
    user = models.ForeignKey(User, verbose_name=u"Użytkownik", blank=True)
    report_date = models.DateTimeField(verbose_name=u"Report date and time", default=datetime.now(), editable=False)