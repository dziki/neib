# -*- coding: utf-8 -*-
'''
Created on 2010-05-05 at 10:38:06

@author: kalinskia
'''
from django.contrib import admin
from feedback.models import Feedback

admin.site.register(Feedback)
