# -*- coding: utf-8 -*-
from django.conf.urls.defaults import *
from django.contrib import admin
from feedback.views import feedback

admin.autodiscover()

urlpatterns = patterns('',
    (r'^.*$', feedback, {}, 'feedback')
)