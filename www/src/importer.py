from business.models import Business, Category, CategoryTag
import codecs

def update_cat(bus):
    for cat in bus.categories.all():
        if cat.parent != 0 and cat.id != 0:
            bus.categories.add(cat.parent)
            
def update_cats():
    for bus in Business.objects.all():
        update_cat(bus)
        
def update_category_tags():
    for cat in Category.objects.all():
        tag = CategoryTag(category=cat, tag=cat.name.lower())
        tag.save()
    pass

def update_location(id):
    buss = Business.objects.get(id=id)
    buss.update_pos()
    buss.save()

def business_loader(category_id, filename_in):
    catdict = dict()
    file_handle = codecs.open("csv/kategorie1.csv", 'r', 'utf-8-sig')
    lines = file_handle.readlines()
    for line in lines:
        a,b = line.split(';')
        catdict[a] = b[:len(b)-2]
    file_handle.close()
    
    file_handle = codecs.open(filename_in, 'r', 'utf-8-sig')
    lines = file_handle.readlines()
    file_handle.close()

    title = lines[0]
    keys = title.split(';')
    
    
    for line in lines[1:]:
        vals = line.split(';')
        item = {}
        for i in range(0, len(keys)-1):
            item[keys[i]] = vals[i]
        buss = Business()
        name = item['name']
        buss.name = name[0] + name[1:].lower()
        buss.address = item['street']
        buss.zip = item.get('zip',None)
        buss.city = item.get('city',None)
        buss.phone = item.get('phone',None)
        buss.email = item.get('email',None)
        buss.www = item.get('www',None)
        cat = item.get('category',None)
        buss.save() 
        
        try:
            if cat and catdict[cat]:
                cat = Category.objects.get(slug=catdict[cat])
                buss.categories.add(cat)
        except:
            pass
        
#        for cat in categories:
#            if len(cat.strip()) == 0:
#                continue
#            cid = int(cat.strip())
#            tmp = Category.objects.filter(id=cid)
#            if len(tmp) == 0:
#                continue
#            cat = tmp[0]
#            buss.name.replace(cat.id.__str__(), cat.name)
#            buss.categories.add(cat)
            
        update_cat(buss)

def business_loader_old(category_id, filename_in):
    file_handle = codecs.open(filename_in, 'r', 'utf-8-sig')
    lines = file_handle.readlines()
    file_handle.close()

    title = lines[0]
    keys = title.split(';')
    cat = Category.objects.get(id=category_id)
    
    for line in lines[1:]:
        vals = line.split(';')
        item = {}
        for i in range(0, len(keys)-1):
            item[keys[i]] = vals[i]
            
        buss = Business()
        name = item['name']
        buss.name = name[0] + name[1:].lower()
        buss.address = item['street']
        buss.zip = item.get('zip',None)
        buss.city = 'Warszawa'
        buss.phone = item.get('phone',None)
        buss.email = item.get('email',None)
        buss.www = item.get('www',None)
        buss.save() 
        buss.categories.add(cat)
        update_cat(buss)
