#noinspection PyUnusedLocal
def notify(request):
    retset = {}
    if request.session.has_key('notifies') and len(request.session['notifies']) > 0:
        retset = {"notify": request.session['notifies'].pop() }
    request.session['notifies'] = list()
    return retset