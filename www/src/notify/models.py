def notify_user(request, title, body):
    if not request.session.has_key('notifies'):
        request.session['notifies'] = list()
        
    note = dict()
    note['title'] = title
    if body:
        note['body'] = body

    request.session['notifies'].append(note)
    return