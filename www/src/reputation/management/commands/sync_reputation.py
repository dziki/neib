# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
from flagging.models import Flag

import logging as log
from reputation.models import ReputationEvent
from business.models import Review

log.basicConfig(level=log.INFO)

class Command(BaseCommand):
    args = '[]'
    help = 'adding reputation'
    
    option_list = BaseCommand.option_list

    def handle(self, *args, **options):
#        if len(args) < 1:
#            raise CommandError("Not enough arguments")
        
        queryset = Flag.objects.all().filter(flag__in=[3,4,5,6],content_type__id=25)
        for item in queryset:
            try:
                review = Review.objects.select_related('user__profile').get(id=item.object_id)
            except Review.DoesNotExist:
                continue
            rep_event = ReputationEvent(profile=review.user.get_profile(),
                            points=5,
                            reputation=1,
                            object=review)
            rep_event.save()
        return 