from django.db import models

from date.models import TimedModel
from django.contrib.auth.models import User
from notification import models as notification
from community.models import Profile
from flagging.models import Flag
from business.models import Review
from django.db.models import Count, Sum

from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

from django.utils.translation import ugettext_lazy as _

class ReputationType:
    def __init__(self, msg, points):
        self.msg = msg
        self.points = points

REPUTATION_TYPES = {
    1: ReputationType(_("review upvote"), 10),
    2: ReputationType(_("comment upvote"), 10),
    3: ReputationType(_("five spam votes"), -50),
}


class ReputationEvent(TimedModel):
    profile = models.ForeignKey(Profile, verbose_name=_("Profile"))
    points = models.IntegerField()
    
    reputation = models.IntegerField()

    object = generic.GenericForeignKey('content_type', 'object_id')
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    
    def save(self, *args, **kwargs):
        res = super(ReputationEvent, self).save(*args, **kwargs)

        d = self.profile.reputationevent_set.aggregate(sum = Sum('points'))
        self.profile.reputation = d["sum"]
        if d["sum"] == None:
            self.profile.reputation = 0
        self.profile.save()
        
        return res
    
def add_reputation_for_flag(sender, instance, created, *args, **kwargs):
    if not created:
        return
    if not instance.flag in [3,4,5,6]:
        return
    rep_event = ReputationEvent(profile=instance.content_object.user.get_profile(),
                    points=5,
                    reputation=1,
                    object=instance.content_object)
    rep_event.save()
    print "Rep for flag"

models.signals.post_save.connect(add_reputation_for_flag, sender=Flag)
