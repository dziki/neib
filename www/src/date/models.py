"""
Models for generic flags.
"""
import time

from django.db import models
from django.utils.translation import ugettext_lazy as _

DAYS_OF_THE_WEEK = (
    (1, 'Poniedzialek'),
    (2, 'Wtorek'),
    (3, 'Sroda'),
    (4, 'Czwartek'),
    (5, 'Piatek'),
    (6, 'Sobota'),
    (7, 'Niedziela'),
)

class TimedModel(models.Model):
    date = models.DateTimeField(_("Last change"),  auto_now_add=True)

    def version(self):
        return int(time.mktime(self.date.timetuple())).__str__()

    class Meta:
        abstract = True
