"""Splits query results list into multiple sublists for template display."""

from django.template import Library
from date.models import DAYS_OF_THE_WEEK

register = Library()

@register.simple_tag
def dayword(n):
    for k,v in DAYS_OF_THE_WEEK:
        if k == n:
            return v
    return ""