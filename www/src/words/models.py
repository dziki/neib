# -*- coding: utf-8 -*-

from django.db import models

class Word(models.Model):
    word = models.CharField(max_length=50, db_index=True)
    similar = models.ManyToManyField("self")
    representative = models.BooleanField(default=True)
    
    def get_representative(self):
        if self.representative:
            return self
        else:
            for dec in self.similar.all():
                if dec.representative:
                    return dec
    
    def get_similar(self):
        if self.representative:
            return self.similar.all()
        else:
            return self.get_representative().similar.all()
    
    def add_similar(self, obj):
        rep = self.get_representative()
        rep.similar.add(obj)
        print "Added: %s" % obj
        
    def add_similar_word(self, word):
        rep = self.get_representative()
        #noinspection PyUnresolvedReferences
        rep.similar.add(type(self).objects.create(word=word, representative=False))
            
    def __unicode__(self):
        return self.word
    
    class Meta:
        abstract = True

class Declension(Word):
    def get_synonyms(self):
        res = []
        for w in self.get_similar():
            syn = Synonym.objects.filter(word__iexact=w)
            for s in syn:
                res += [sw.word for sw in s.get_similar()]
        return res

class Synonym(Word):
    pass

class Thesaurus(models.Model):
    word = models.CharField(max_length=50)
    similar = models.ManyToManyField("self")
    
class PathItem(models.Model):
    search = models.CharField(max_length=255, primary_key=True)
    position = models.IntegerField()
    what = models.CharField(max_length=100, null=True, blank=True)
    where = models.CharField(max_length=100, null=True, blank=True)
    
    class Meta:
        unique_together = (("search", "position"),)