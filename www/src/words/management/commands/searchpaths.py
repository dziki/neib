# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

import logging as log
from optparse import make_option
log.basicConfig(level=log.INFO)

class Command(BaseCommand):
    args = '[]'
    help = 'Gets paths from search'
    
    option_list = BaseCommand.option_list + (
           make_option('--print',
               action='store_true',
               dest='print_tags',
               default=False,
               help='Print all tags'),
           make_option('--fitting',
               action='store',
               type="float",
               dest='fitting',
               default=1.0,
               help='Fitting value to set (default 1.0)'),
       )

    def handle(self, *args, **options):
        pass