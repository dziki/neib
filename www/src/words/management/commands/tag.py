# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
from django.db.utils import IntegrityError

from business.models import Business, Category
from ...models import Synonym
from tagging.models import Tag

import re

import logging as log
from optparse import make_option
log.basicConfig(level=log.INFO)


def _clean_name_tag(name):
    name = name.strip()
    #extract quoted
    quoted = re.findall('\"[^\"]+\"', name)
    if quoted or ' ' in name or '\t' in name:
        name = '"%s"' % name.replace('"', '')
    
    return quoted + [name]

def _complete_with_synonyms(words):
    if isinstance(words, str):    
        words = [words]
    res = []
    for word in words:
        res = res + ['"%s"' % syn.word for syn in Synonym.objects.filter(word__iexact=word)]
    print res
    return res + words    

def print_tags_for_model(model):
    for o in model.objects.all():
        log.info("%s has tags %s" % (o,[t.name for t in Tag.objects.get_for_object(o)]))
        
def tag_model_by_name(model, fitting=1.0):
    for obj in model.objects.all():
            tags = _clean_name_tag(obj.name)
            log.info("%s <%s>: adding tags %s with fitting %.2f" % (model.__name__, obj.name, str(tags), fitting))
            try:
                for tag in tags:
                    Tag.objects.add_tag(obj, tag, fitting)
            except IntegrityError:
                log.warning("%s %s already has name tag" % (model.__name__,obj.name)) 

def tag_model_by_mainform(model, fitting=0.9):
    for obj in model.objects.all():
        if not obj.main_form:
            continue
        ts = obj.main_form.split(',')
        tags = []
        for tag in ts:
            tags += _clean_name_tag(tag)
        if len(tags) == 0:
            continue
        if tags[0] == '':
            continue
        log.info("%s <%s>: adding tags %s with fitting %.2f" % (model.__name__, obj.name, str(tags), fitting))
        try:
            for tag in tags:
                Tag.objects.add_tag(obj, tag, fitting)
        except IntegrityError:
            log.warning("%s %s already has name tag" % (model.__name__,obj.name)) 

def tag_model_by_synonym(model, fitting=0.4):
    for obj in model.objects.all():
        name = obj.name
        tags = name.split(" i ")
        try:
            for word in _complete_with_synonyms(tags):
                print "Adding tag: ", word
                Tag.objects.add_tag(obj, word.lower(), fitting)
        except IntegrityError:
            log.warning("%s %s already has name tag" % (model.__name__,obj.name)) 
            
class Command(BaseCommand):
    args = '[business (biz) | category (cat)]'
    help = 'Manages tags for models'
    
    option_list = BaseCommand.option_list + (
           make_option('--print',
               action='store_true',
               dest='print_tags',
               default=False,
               help='Print all tags'),
           make_option('--fitting',
               action='store',
               type="float",
               dest='fitting',
               default=1.0,
               help='Fitting value to set (default 1.0)'),
       )

    def handle(self, *args, **options):
        if len(args) < 1:
            raise CommandError("Not enough arguments")
        cmd = args[0]
        if cmd == 'business' or cmd == 'biz':
            self._business(*tuple(args[1:]), **options)
        elif cmd == 'category' or cmd == 'cat':
            self._category(*tuple(args[1:]), **options)
        elif cmd == 'mainform' or cmd == 'mfs':
            self._mainform(*tuple(args[1:]), **options)
        else:
            raise CommandError("Command unknown. Possible options: business (biz), category (cat)")
        
    def _business(self, *args, **opts):
        """
        Adds name of Business as a tag
        """
        print opts
        if opts['print_tags']:
            print_tags_for_model(Business)
        else:
            tag_model_by_name(Business, opts['fitting'])
            
    def _category(self, *args, **opts):
        """
        Adds name of Category as tag
        """
        if opts['print_tags']:
            print_tags_for_model(Category)
            return
        if args and args[0] == "synonyms":
            if "fitting" in opts:
                tag_model_by_synonym(Category, fitting=opts["fitting"])
            else:
                tag_model_by_synonym(Category)
        else:
            if "fitting" in opts:
                tag_model_by_name(Category, fitting=opts["fitting"])
            else:
                tag_model_by_name(Category)
                
    def _mainform(self, *args, **opts):
        """
        Adds mainform of Category as tag
        """
        tag_model_by_mainform(Category)