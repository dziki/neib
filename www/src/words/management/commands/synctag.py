# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.db import connection

import logging as log

log.basicConfig(level=log.INFO)

class Command(BaseCommand):
# DROP TABLE `business_fitting`
# CREATE TABLE `business_fitting` SELECT * FROM `business_fitting_view` WHERE 1
    args = '[]'
    help = 'Creates fitting cache table'
    
    option_list = BaseCommand.option_list

    def handle(self, *args, **options):
        cursor = connection.cursor()
    
        try:
            cursor.execute('''DROP TABLE IF EXISTS `business_fitting`;''')
        except:
            pass
        cursor.execute('''CREATE TABLE `business_fitting` SELECT * FROM `business_fitting_view` WHERE 1;''')
