#!/bin/bash

BASEDIR=$(dirname $0)

cd $BASEDIR
for dir in *
do
        if [ -d "$dir" ]; then
        	mkdir $dir/locale
        	mkdir $dir/locale/$1
        	(cd $dir ; django-admin.py makemessages --locale=$1)
        fi
done