from localsettings import GOOGLE_API_KEY

#noinspection PyUnusedLocal
def googleapi(request):
    return {"GOOGLE_API_KEY": GOOGLE_API_KEY }
