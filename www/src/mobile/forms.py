from django import forms

class SearchForm(forms.Form):
    what = forms.CharField(label="What?")
    where = forms.CharField(label="Where?")
