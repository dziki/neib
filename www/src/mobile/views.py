# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from mobile.forms import SearchForm
from search.models import SearchEngine
from business.models import Business


def business_details(request, business_slug):
    business = get_object_or_404(Business, slug=business_slug)
    
    return render_to_response('mobile/business.details.html',
                              {"business": business},
                               context_instance=RequestContext(request))
    

def mobile_search(request):
    search_dict = dict()
    search_dict["what"] = request.GET.get('what',"")
    search_dict["where"] = request.GET.get('where',request.session['area'].name)
    search_dict["category"] = category = request.GET.get('category',None)

    search = SearchEngine()
    if (len(search_dict["what"])):
        search.filter_what(search_dict["what"])

    if (category is not None) and (category != 0) and (category != '0') and (category != ''):
        search.filter_category(category)

    search.filter_where(search_dict["where"])
    
    businesses = search.businesses[:10]

    return render_to_response('mobile/search.results.html',
                              {"businesses": businesses},
                               context_instance=RequestContext(request))
def mobile_home(request):
    if request.GET:
        form = SearchForm(request.GET)
        if form.is_valid():
            return mobile_search(request)
    else:
        form = SearchForm()
        
    return render_to_response('mobile/base.html',
                              {"form": form},
                               context_instance=RequestContext(request))