"""
URLConf for ownership app

"""

from django.conf.urls.defaults import *
from django.contrib import admin

from mobile.views import *

admin.autodiscover()

urlpatterns = patterns('django.views.generic.simple',
    ((r'biz/(?P<business_slug>[-\w]+)/$'), business_details, {}, 'mobile_business_details'),
    ((r'$'), mobile_home, {}, 'mobile_home'),
    
)