from django.contrib.gis.db import models as geomodels
from django.db.models import Count

class PhotoManager(geomodels.GeoManager):
    def get_gallery_photo(self, gallery):
        return gallery.get_representative_photo()
    
    def num_of_gallery_photos(self, gallery):
        if gallery is None:
            return None
        qs = super(PhotoManager, self).get_query_set().filter(gallery=gallery)
        photos = qs.aggregate(Count('id'))
        return photos['id__count']
    
class GalleryManager(geomodels.GeoManager):
    pass