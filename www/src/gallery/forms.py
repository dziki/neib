"""
Gallery forms. Currently only add photo
"""
from django.forms import ModelForm
from gallery.models import *

class PhotoForm(ModelForm):
    class Meta:
        model = Photo
        fields = (
            'image',
            'caption',
        )


class PhotoFormShort(ModelForm):
    class Meta:
        model = Photo
        fields = (
            'image',
        )
