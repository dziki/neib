# -*- coding: utf-8 -*- 
"""
Models to manage generic gallery
"""
import os
import Image

from django.contrib.auth.models import User
from django.db import models
from django.conf import settings
from django.db.models.signals import post_save, post_init
from django.core.exceptions import ObjectDoesNotExist
from django.utils.functional import curry

from django.contrib.contenttypes import generic
from flagging.models import Flag
from gallery.managers import PhotoManager, GalleryManager
from date.models import TimedModel
from common.helpers import generate_media_url
from django.core.cache import cache

PHOTOS_PATH = settings.PHOTOS_PATH
PHOTOS_URL = settings.PHOTOS_URL

# Quality options for JPEG images
JPEG_QUALITY_CHOICES = (
    (30, ('Very Low')),
    (40, ('Low')),
    (50, ('Medium-Low')),
    (60, ('Medium')),
    (70, ('Medium-High')),
    (80, ('High')),
    (90, ('Very High')),
)

def GalleryField(name, blank=True, null=True, verbose_name="Galeria", **kwargs):
    related_name = "owner_%s" % name    
    return models.ForeignKey(Gallery,
                                blank=blank,
                                null=null,
                                verbose_name=verbose_name,
                                related_name=related_name,
                                **kwargs)

#noinspection PyUnusedLocal
def get_storage_path(instance, filename):
    return PHOTOS_PATH

#def get_size_path(instance, filename, size):
#    return MEDIA_ROOT + '/photos/%s.%d.jpg' % (size, instance.id)

# Create your models here.
class Gallery(TimedModel):
    """
    Generic gallery - attached to models: User, Business
    
    In each gallery we store photos (indipendly for each model).
    Each gallery has its date_added

    """
    user = models.ForeignKey(User, blank=True, null=True, verbose_name = "Dodajacy")
    representative = models.ForeignKey('Photo',
                                       blank=True,
                                       null=True,
                                       default=None,
                                       related_name="representative_for",
                                       verbose_name="Główne zdjęcie")
    objects = GalleryManager()
    
    def __str__(self):
        return self.id.__str__()

    def __unicode__(self):
        return str(self.id)

    def get_photo_add_url(self):
        return "/galeria/%i/dodaj/" % self.id

    def get_representative_photo(self):
        return self.representative
#        res = self.representative
#        if res:
#            return res
#        photos = self.photo_set.all()[:1]
#        if len(photos) > 0:
#            self.representative = photos[0]
#            if self.representative:
#                self.save()
#        return res or None

    def get_owner(self):
        owners = [w for w in dir(self) if w.startswith("owner_")]
        for owner in owners:
            try:
                obj = getattr(self, owner)
                if obj.all().count()>0:
                    return obj.all()[0]
            except ObjectDoesNotExist:
                pass

    owner = property(fget=get_owner)


    class Meta:
        ordering = ["-id"]

#noinspection PyUnusedLocal
def gallery_create(sender, **kwargs):
    """
    After adding file to table we need to calculate
    and save all versions of file (in each size).
        
    """
    instance = kwargs['instance']
    
    # new path for file in original format
    new_path = PHOTOS_PATH + '%d.jpg' % instance.id
    if os.path.exists(new_path):
        os.remove(new_path)

    # move new file to it's new path
    os.rename(instance.image.path, new_path)

    # for all sizes resize our photo and save
    sizes = PhotoSize.objects.all()
    orginal = Image.open(new_path)
    for size in sizes:
        im = instance.resize_image(orginal, size)
        new_path = instance._get_SIZE_path(size)
        if os.path.exists(new_path):
            os.remove(new_path)
        im.save(new_path)

class Photo(TimedModel):
    """
    Photo attached to given gallery.
    User describes who added it.
    If flag ``default`` is set to ``True`` then photo is
    used as the main photo in gallery. (TODO)

    """
    gallery = models.ForeignKey(Gallery, blank=True, null=True, verbose_name = "Galeria")
    user = models.ForeignKey(User, blank=True, null=False, verbose_name = "Dodajacy")
    image = models.ImageField(('Zdjęcie'), upload_to=get_storage_path)
    caption = models.TextField(('Opis'), blank=True)
    # deprecated
    default = models.BooleanField(('Zdjęcie profilowe'), default=False)

    flags = generic.GenericRelation(Flag)

    export_fields = ()
    export_fields_ex = ('user', 'caption',)

    def delete(self, *args, **kwargs):
        self.gallery = None
        #noinspection PyUnresolvedReferences
        self.representative_for.clear()
        super(Photo,self).delete(*args, **kwargs)

    def __str__(self):
        return self.id.__str__()
    
    disp = None
    def display_id(self):
        if self.disp != None:
            return (self.disp)
        return self.id
    
    objects = PhotoManager()
    
    @models.permalink
    def get_absolute_url(self):
        if not self.id:
            if not self.gallery:
                return "/"
            return ('photo_add', (), {'gallery_id': self.gallery.id})
        return ('photo_detail', (), {'photo_id': self.display_id()})

    @models.permalink
    def get_delete_url(self):
        return ('photo_delete', (), {'photo_id': self.id})

    def _get_SIZE_url(self, size):
        """
        gives link to photo file of given size
        
        """
        return generate_media_url(PHOTOS_PATH + '%s.%s.jpg' % (self.display_id(), size.name))

    def _get_SIZE_path(self, size):
        """
        gives UNIX path to photo with given size
        
        """
        return settings.MEDIA_ROOT + PHOTOS_PATH + '%s.%s.jpg' % (self.display_id(), size.name)


    #noinspection PyUnusedLocal
    def add_accessor_methods(self, *args, **kwargs):
        key = "gallery_sizes"
        if key in cache:
            sizes = cache.get(key)
        else:
            sizes = PhotoSize.objects.all()
            cache.set(key, list(sizes), 60*60*24*7)

        for size in sizes:
            setattr(self, 'get_%s_path' % size.name,
                curry(self._get_SIZE_path, size=size))
            setattr(self, 'get_%s_url' % size.name,
                curry(self._get_SIZE_url, size=size))
    
    def get_urls(self):
        """
        gives links to photos in all photo sizes
        
        """
        key = "gallery_sizes"
        if key in cache:
            sizes = cache.get(key)
        else:
            sizes = PhotoSize.objects.all()
            cache.set(key, list(sizes), 60*60*24*7)
        
        dict = {}
        for size in sizes:
            dict[size.name] = self._get_SIZE_url(size)
        return dict

    def resize_image(self, im, photosize):
        """
        resize photo to given photosize
        
        """
        cur_width, cur_height = im.size
        new_width, new_height = photosize.size
        if photosize.crop:
            ratio = max(float(new_width)/cur_width,float(new_height)/cur_height)
            x = (cur_width * ratio)
            y = (cur_height * ratio)
            xd = abs(new_width - x)
            #yd = abs(new_height - y)
            x_diff = int(xd / 2)
            y_diff = 0 #int(yd / 2)
            box = (int(x_diff), int(y_diff), int(x_diff+new_width), int(y_diff+new_height))
            im = im.resize((int(x), int(y)), Image.ANTIALIAS).crop(box)
        else:
            if not new_width == 0 and not new_height == 0:
                ratio = min(float(new_width)/cur_width,
                            float(new_height)/cur_height)
            else:
                if new_width == 0:
                    ratio = float(new_height)/cur_height
                else:
                    ratio = float(new_width)/cur_width
            new_dimensions = (int(round(cur_width*ratio)),
                              int(round(cur_height*ratio)))
            if new_dimensions[0] > cur_width or \
               new_dimensions[1] > cur_height:
                if not photosize.upscale:
                    return im
            im = im.resize(new_dimensions, Image.ANTIALIAS)
        return im

    class Meta:
        ordering = ["-id"]

#        super(Photo, self).save(*args, **kwargs)
#        sizes = PhotoSize.objects.all()
#        for size in sizes:
#            im = resize_image(self.image, size)
#            im.save()

#noinspection PyUnusedLocal
def photo_post_save(sender, **kwargs):
    """
    After adding file to table we need to calculate
    and save all versions of file (in each size).
        
    """
    instance = kwargs['instance']
    old_path = instance.image.path
    # new path for file in original format
    new_path = settings.MEDIA_ROOT + PHOTOS_PATH + '%d.jpg' % instance.id
    if os.path.exists(new_path):
        os.remove(new_path)
        
    orginal = Image.open(old_path)

    if orginal.mode != "RGB":
        orginal = orginal.convert("RGB")
    # move new file to it's new path
    orginal.save(new_path, "JPEG")

    # for all sizes resize our photo and save
    sizes = PhotoSize.objects.all()
    for size in sizes:
        im = instance.resize_image(orginal, size)
        new_path = instance._get_SIZE_path(size)
        if os.path.exists(new_path):
            os.remove(new_path)
        im.save(new_path, "JPEG")
    os.remove(old_path)
    
    if instance.gallery.representative == None:
        instance.gallery.representative = instance
        instance.gallery.save()

post_save.connect(photo_post_save, sender=Photo)

class PhotoSize(models.Model):
    """
    Describe thumbnail file format
        
    """
    name = models.CharField(('name'), max_length=20, unique=True, help_text=('Photo size name should contain only letters, numbers and underscores. Examples: "thumbnail", "display", "small", "main_page_widget".'))
    width = models.PositiveIntegerField(('width'), default=0, help_text=('If width is set to "0" the image will be scaled to the supplied height.'))
    height = models.PositiveIntegerField(('height'), default=0, help_text=('If height is set to "0" the image will be scaled to the supplied width'))
    quality = models.PositiveIntegerField(('quality'), choices=JPEG_QUALITY_CHOICES, default=70, help_text=('JPEG image quality.'))
    upscale = models.BooleanField(('upscale images?'), default=False, help_text=('If selected the image will be scaled up if necessary to fit the supplied dimensions. Cropped sizes will be upscaled regardless of this setting.'))
    crop = models.BooleanField(('crop to fit?'), default=False, help_text=('If selected the image will be scaled and cropped to fit the supplied dimensions.'))

    class Meta:
        ordering = ['width', 'height']
        verbose_name = ('photo size')
        verbose_name_plural = ('photo sizes')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.__unicode__()

    def save(self, *args, **kwargs):
        if self.crop is True:
            if self.width == 0 or self.height == 0:
                raise ValueError("PhotoSize width and/or height can not be zero if crop=True.")
        super(PhotoSize, self).save(*args, **kwargs)

    #noinspection PyMethodOverriding
    def delete(self):
        assert self._get_pk_val() is not None, "%s object can't be deleted because its %s attribute is set to None." % (self._meta.object_name, self._meta.pk.attname)
        super(PhotoSize, self).delete()

    def _get_size(self):
        return (self.width, self.height)
    def _set_size(self, value):
        self.width, self.height = value
    size = property(_get_size, _set_size)

#noinspection PyUnusedLocal
def add_methods(sender, instance, signal, *args, **kwargs):
    """ Adds methods to access sized images (urls, paths)

    after the Photo model's __init__ function completes,
    this method calls "add_accessor_methods" on each instance.
    """
    if hasattr(instance, 'add_accessor_methods'):
        instance.add_accessor_methods()

post_init.connect(add_methods)
