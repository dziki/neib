# -*- coding: utf-8 -*-
from django.conf.urls.defaults import patterns
from django.contrib import admin
from gallery.views import *

admin.autodiscover()

urlpatterns = patterns('',
    ((r'^manage/$'), photos_management, {}, 'photos_management'),
    ((r'^(?P<gallery_id>[-\d]+)/add/$'), photo_add, {}, 'photo_add'),
    (r'^(?P<gallery_id>[-\d]+)/$', gallery_detail, {}, 'gallery_detail'),
    ((r'^photo/(?P<photo_id>[-\d]+)/$'), photo_detail, {}, 'photo_detail'),
    ((r'^photo/(?P<photo_id>[-\d]+)/remove/$'), photo_delete, {}, 'photo_delete'),

)