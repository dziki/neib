# -*- coding: utf-8 -*-
"""
Views of photos and galleries. Also allows users to add files to gallery.

"""

from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.shortcuts import get_object_or_404

from gallery.forms import *
from notify.models import *


def gallery_detail(request, gallery_id):
    """
    Shows all photos in given gallery

    **Required arguments**

    ``gallery_id``
       The id of gallery.

    **Context:**

    ``gallery``
        Gallery object. Gives gallery description and id

    ``photos``
        All gallery photos.

    **Template:**

    gallery/gallery.html.

    """
    gallery = get_object_or_404(Gallery, id=gallery_id)
    return render_to_response('gallery/gallery.html',
        {"gallery": gallery,
        "photos": gallery,},
        context_instance=RequestContext(request))

@login_required()
def photo_delete(request, photo_id):
    photo = get_object_or_404(Photo, id=photo_id)
    gallery_id = photo.gallery.id
    if photo.user == request.user:
        photo.delete()
        notify_user(request, u"Zdjęcie zostało usunięte","")
        if 'return_to' in request.REQUEST:
            url = request.REQUEST['return_to']#.split("?")[0]
            return redirect(url)
    else:
        notify_user(request, u"Nie masz uprawnień do usunięcia zdjęcia","")

    return gallery_detail(request, gallery_id)

def photo_detail(request, photo_id):
    """
    Shows given photo

    **Required arguments**

    ``photo_id``
       The id of photo.

    **Context:**

    ``photo``
        Photo object. Urls to all versions of files are in
            photo.get_urls.display
        Currently supported sizes:
            small
            medium
            large
            display

        small,medium,large are for thumbnails
        display stands for full screen version (fitted to browser)

    **Template:**

    gallery/photo.html.

    """
    photo = get_object_or_404(Photo, id=photo_id)
    return render_to_response('gallery/photo.html', {"photo": photo, "gallery": photo.gallery},
        context_instance=RequestContext(request))

@login_required()
def photo_add(request, gallery_id):
    """
    Gives form to add file to gallery of given id.

    **Required arguments**

    ``gallery_id``
       The id of gallety.

    **Context:**

    ``form``
        form to add photo

    or if data has been POSTed context is the same as in gallery_detail

    **Template:**

    gallery/photo.add.html or as in gallery_detail.

    """
    profile = Photo()

    form = PhotoForm(instance=profile)
    if request.POST and request.FILES:
        form = PhotoForm(request.POST, request.FILES)
        if form.is_valid():
            form.instance.gallery_id = gallery_id
            form.instance.user = request.user

            gallery_user = User.objects.filter(gallery__id = gallery_id)
            if len(gallery_user) > 0 and gallery_user[0] != request.user:
                raise Error

            notify_user(request, "Dodano zdjęcie","")
            form.save()
            if 'return_to' in request.POST:
                url = request.POST['return_to']#.split("?")[0]
                return redirect(url)
            return gallery_detail(request, gallery_id)

    return render_to_response('gallery/photo.add.html', {"form": form},
        context_instance=RequestContext(request))

@login_required()
def photos_management(request):
    """


    """

    other_photos = Photo.objects.filter(user = request.user).exclude(gallery = request.user.profile.gallery)

    return render_to_response('gallery/management.html', {"other_photos": other_photos},
        context_instance=RequestContext(request))
