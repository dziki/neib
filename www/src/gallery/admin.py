from django.contrib import admin
from gallery.models import Gallery, Photo, PhotoSize

admin.site.register(Gallery)
admin.site.register(Photo)
admin.site.register(PhotoSize)