# -*- coding: utf-8 -*-
import os
import Image

from django.core.management.base import BaseCommand, CommandError
from flagging.models import Flag

import logging as log
from reputation.models import ReputationEvent
from business.models import Review
from django.conf import settings

log.basicConfig(level=log.INFO)

from gallery.models import Photo, PhotoSize

PHOTOS_PATH = settings.PHOTOS_PATH
PHOTOS_URL = settings.PHOTOS_URL

class Command(BaseCommand):
    args = '[]'
    help = 'adding reputation'
    
    option_list = BaseCommand.option_list

    def handle(self, *args, **options):
        for instance in Photo.objects.all():
            old_path = instance.image.path
            # new path for file in original format
            new_path = settings.MEDIA_ROOT + PHOTOS_PATH + '%d.jpg' % instance.id
#            if os.path.exists(new_path):
#                os.remove(new_path)
                
            try:
                orginal = Image.open(new_path)
                sizes = PhotoSize.objects.filter(name='medium')
                for size in sizes:
                    im = instance.resize_image(orginal, size)
                    new_path = instance._get_SIZE_path(size)
                    if os.path.exists(new_path):
                        os.remove(new_path)
                    im.save(new_path, "JPEG")
            except IOError:
                continue
        return 