from django.db.models.signals import pre_save
from messages.models import Message

def notify_private_message(sender, **kwargs):
    sender.sender.name = 'test'
    sender.sender.save()

post_save.connect(my_handler, sender=Message)