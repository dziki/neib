# -*- coding: utf-8 -*-
'''
Created on 2010-05-05 at 11:14:24

@author: kalinskia
'''
from django.http import HttpResponse

def render_form_as_table(form):
    return HttpResponse(form.as_table())

def render_form_as_p(form):
    return HttpResponse(form.as_p())

def render_form_as_ul(form):
    return HttpResponse(form.as_ul())
