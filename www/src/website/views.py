from django.contrib.auth.models import User
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render_to_response

from business.models import Business, Category
from django.conf import settings
from django.utils.safestring import mark_safe
from django.template import Context, Template
from django.template.loader import get_template
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponse
import os

class Background:
    id = 0
    url = settings.MEDIA_URL + "website/backgrounds/grunge_city/grunge_city_bg_2.3.jpg"
    path = settings.MEDIA_ROOT + "website/backgrounds/grunge_city/grunge_city_bg_2.3.jpg"
    
    def __str__(self):
        return self.url

class Banner:
    id = 0
    url = settings.MEDIA_URL + "website/default/images/pic1.jpg"
    path = settings.MEDIA_ROOT + "website/default/images/pic1.jpg"

    def __str__(self):
        return self.url


b1 = Background()
b2 = Background()
b2.id = 1
b2.url = settings.MEDIA_URL + "website/backgrounds/vegetables.jpg"
b2.path = settings.MEDIA_ROOT + "website/backgrounds/vegetables.jpg"

backgrounds = [b1,b2]

ba1 = Banner()
ba2 = Banner()
ba2.id = 1
ba2.url = settings.MEDIA_URL + "website/photos/vegetables.jpg"
ba2.path = settings.MEDIA_ROOT + "website/photos/vegetables.jpg"

class Theme:
    template = "default"
    background = b1
    banner = ba2
    colors = {
             "main_menu": "#94733A",
    }

class Panel:
    title = ""
    dom_id = ""

    def nav_title(self):
        return self.title
    
    content = None
    
    def has_content(self):
        if self.content:
            return True
        return False

THEME_DEFAULT = Theme()

THEME = Theme()
THEME.background = b2
THEME.banner = ba2
THEME.colors =  {
    "main_menu": "#426932",
}


def generic_site(request, business_slug, page):
    business = Business.objects.get(slug=business_slug)

    cat = Category.objects.get(slug="restaurants")
    if cat in business.categories.all():
        theme = THEME
    else:
        theme = THEME_DEFAULT
    
    media_path = os.path.join(os.path.dirname(__file__),'media', 'debug_toolbar')
    
    p1 = Panel()
    p1.title = _("Appearance")
    p1.dom_id = "u1"
    
    t = get_template("toolbar/panel/appearance.html")

    c = RequestContext(request,{"backgrounds": [b1,b2,],
                 "banners": [ba1,ba2,],})
    p1.content = t.render(c)

    t = get_template("toolbar/panel/contact.html")
    c = RequestContext(request,{})

    p2 = Panel()
    p2.title = _("Buy now!")
    p2.dom_id = "u2"
    p2.content = t.render(c)

    return render_to_response('website/%s/%s.html' % (theme.template, page),
                              context_instance=RequestContext(request,
                              {'business':business,
                               'theme': theme,
                                'js': mark_safe(open(os.path.join(media_path, 'js', 'toolbar.js'), 'r').read()),
                                'css': mark_safe(open(os.path.join(media_path, 'css', 'toolbar.css'), 'r').read()),                               
                               'panels': [p1,p2],
                               }))

def website_home(request, business_slug):
    return generic_site(request, business_slug, 'home')

def website_about(request, business_slug):
    return generic_site(request, business_slug, 'about')

def website_reviews(request, business_slug):
    return generic_site(request, business_slug, 'reviews')

def website_offer(request, business_slug):
    return generic_site(request, business_slug, 'offer')

def website_gallery(request, business_slug):
    return generic_site(request, business_slug, 'gallery')

def website_contact(request, business_slug):
    return generic_site(request, business_slug, 'contact')


def builder_submit(request, business_slug):
    from django.core.mail import send_mail

    send_mail('neib.org - ZAMOWIENIE? - ' + business_slug, 'Phone: %s\n\n.%s.' % (request.GET["phone"], request.GET["msg"]), request.GET["email"],
        ['dziki.dziurkacz@gmail.com'], fail_silently=False)

    return HttpResponse("OK")

def builder_home(request):
    from business.forms import BusinessForm
    if request.POST:
        form = BusinessForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("website_home", business_slug=form.instance.slug)
    else:
        form = BusinessForm()
        
    return render_to_response('builder/base.html',
                              context_instance=RequestContext(request,
                              {"form": form}))
    