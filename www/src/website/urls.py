import django
from django.conf.urls.defaults import patterns

from django.contrib import admin

from website.views import *

admin.autodiscover()

urlpatterns = patterns('',
    ((r'^$'), builder_home, {}, 'builder_home'),

    ((r'^(?P<business_slug>[-\w]+)/$'), website_home, {}, 'website_home'),
    ((r'^(?P<business_slug>[-\w]+)/about/$'), website_about, {}, 'website_about'),
    ((r'^(?P<business_slug>[-\w]+)/offer/$'), website_offer, {}, 'website_offer'),
    ((r'^(?P<business_slug>[-\w]+)/reviews/$'), website_reviews, {}, 'website_reviews'),
    ((r'^(?P<business_slug>[-\w]+)/gallery/$'), website_gallery, {}, 'website_gallery'),
    ((r'^(?P<business_slug>[-\w]+)/contact/$'), website_contact, {}, 'website_contact'),

    ((r'^(?P<business_slug>[-\w]+)/([-\w]+)/submit/$'), builder_submit, {}),
    ((r'^(?P<business_slug>[-\w]+)/submit/$'), builder_submit, {}),
)