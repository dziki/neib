from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from compliments.forms import *
from django.contrib.auth.models import User

@login_required()
def compliments_add(request, username):
    user_id = username
    user = User.objects.get(id=user_id)

    if request.POST:
        form = ComplimentForm(request.POST)
        if form.is_valid():
            user = User.objects.get(id=user_id)
            form.instance.recipient = user
            form.instance.sender = request.user
            form.save()
    else:
        form = ComplimentForm()

    return render_to_response('compliments/compliments.add.html', context_instance=RequestContext(request,{"form": form,
                                                                                                           "selecteduser": user}))

@login_required()
def compliments_del(request, compliment_id):
    Compliment.objects.get(id = compliment_id, recipient = request.user).delete()
    return compliments_list(request, request.user.id)

def compliments_list(request, username):
    user_id = username
    user = User.objects.get(id=user_id)

    if (user == request.user):
        Compliment.objects.mark_read(user)

    compliments = Compliment.objects.filter(recipient=user)

    return render_to_response('compliments/compliments.list.html',
                              context_instance=RequestContext(request,{"compliments": compliments,
                                                                       "selecteduser": user}))
