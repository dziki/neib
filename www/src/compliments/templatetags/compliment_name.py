import django

register = django.template.Library()

@register.simple_tag
def compliment_name(compliment, gender):
    return compliment.name_by_gender(gender)