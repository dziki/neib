"""
Models for generic flags.
"""
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.contrib.auth.models import User
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from date.models import TimedModel

from compliments.managers import ComplimentManager
from notification import models as notification

PHOTOS_PATH = settings.PHOTOS_PATH

class ComplimentType(models.Model):
    """ A flag on the item. """
    short_name = models.CharField(max_length=20, verbose_name = _("Short title"))
    full_name_male = models.CharField(max_length=20, verbose_name = _("Extended male title"))
    full_name_female = models.CharField(max_length=20, verbose_name = _("Extended female title"))

    image = models.ImageField(_('Ikonka'), upload_to=PHOTOS_PATH)
    
    def __str__(self):
        return self.full_name_male

    def __unicode__(self):
        return unicode(self.full_name_male)
    
    def name_by_gender(self, gender):
        if (gender=='F'):
            return self.full_name_female
        return unicode(self.full_name_male)
    
# Create your models here.
class Compliment(TimedModel):
    """ A flag on the item. """
    compliment = models.ForeignKey(ComplimentType)
    body = models.TextField(_("Content"))

    sender = models.ForeignKey(User, null=True, verbose_name = _("Sender"), related_name = "compliements_sent")
    recipient = models.ForeignKey(User, null=True, verbose_name = _("Receiver"), related_name = "compliements_received")
    
    read_at = models.DateTimeField(_("Read"), null=True, blank=True) #currently unused
    
    object_id = models.PositiveIntegerField(null=True)
    content_type = models.ForeignKey(ContentType, null=True)
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    objects = ComplimentManager()

    def __unicode__(self):
        return unicode(self.compliment.__unicode__() + " " + _("from") + " " + self.sender.__unicode__())

    def __str__(self):
        return self.compliment.__str__() + " " + _("from") + " " + self.sender.__str__()

    def compliment_title(self):
        return unicode(_(self.compliment.name_by_gender('M')))

    def save(self, *args, **kwargs):
        super(Compliment, self).save(*args, **kwargs)
        notification.send([self.recipient], "compliement_received", {'action_user': self.sender, }, True, 6)

    class Meta:
        ordering = ["-id"]

