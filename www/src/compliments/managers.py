import datetime

from django.db import models

class ComplimentManager(models.Manager):
    def inbox_for(self, user):
        """
        Returns all messages that were received by the given user and are not
        marked as deleted.
        """
        return self.filter(
            recipient=user,
        )

    def mark_read(self, user):
        new = super(ComplimentManager, self).get_query_set().filter(recipient=user, read_at=None)
        return new.update(read_at=datetime.datetime.now())
