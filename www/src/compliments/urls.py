# -*- coding: utf-8 -*-
from django.conf.urls.defaults import patterns
from django.contrib import admin
from compliments.views import *

from django.utils.translation import ugettext_lazy as _

admin.autodiscover()

urlpatterns = patterns('',
    ((r'^(?P<username>[-\w]+)/add/$'), compliments_add, {}, 'compliments_add'),
    ((r'^(?P<username>[-\w]+)/list/$'), compliments_list, {}, 'compliments_list'),
    ((r'^(?P<compliment_id>[-\w]+)/remove/$'), compliments_del, {}, 'compliments_del'),
)