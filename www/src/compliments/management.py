#!/usr/bin/env python
# -*- coding: utf-8 -*- 
from django.db.models import signals
from django.conf import settings

if "notification" in settings.INSTALLED_APPS:
    from notification import models as notification

    #noinspection PyUnusedLocal
    def create_notice_types(app, created_models, verbosity, **kwargs):
        notification.create_notice_type("compliement_received", (u"Nowy komplement"), ("otrzymałeś nowy komplement"), default=2)

    signals.post_syncdb.connect(create_notice_types, sender=notification)
else:
    print "Skipping creation of NoticeTypes as notification app not found"
