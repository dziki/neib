import django.contrib
from compliments.models import *

django.contrib.admin.site.register(ComplimentType)
django.contrib.admin.site.register(Compliment)