"""
"""
import django.forms

from compliments.models import Compliment

class ComplimentForm(django.forms.ModelForm):
    class Meta:
        model = Compliment
        fields = (
            'compliment',
            'body',
        )