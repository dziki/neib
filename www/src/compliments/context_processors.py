import compliments.models

def compliment_types_list_processor(request):
    return {'compliment_types': compliments.models.ComplimentType.objects.all()}