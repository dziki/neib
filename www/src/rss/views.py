from django.contrib.syndication.views import Feed
from business.models import Review
from community.models import Profile
from django.utils.feedgenerator import RssUserland091Feed
from django.contrib.auth.models import User
from django.utils.text import truncate_html_words

class ReviewsFeed(Feed):
    feed_type = RssUserland091Feed
    title = "Recenzje neib.org"
    link = "/"
    description = "Nowe recenzje na stronie neib.org"

    def items(self):
        return Review.objects.order_by('-id')
		
    def item_description(self, obj):
        if obj:
            return truncate_html_words(obj.content, 100)
        return u""

    def description(self, obj):
        if obj:
            return truncate_html_words(obj.content, 100)
        return u""
		
class UsersFeed(Feed):
    feed_type = RssUserland091Feed
    title = "Uzytkownicy neib.org"
    link = "/"
    description = "Nowi uzytkownicy na stronie neib.org"

    def items(self):
        return User.objects.order_by('-id')

    def link(self, obj):
        return "profil/" + obj.id + "/aktualnosci/"