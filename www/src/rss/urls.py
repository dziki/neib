from django.conf.urls.defaults import *
from rss.views import UsersFeed, ReviewsFeed

urlpatterns = patterns('',
    (r'^uzytkownicy/$', UsersFeed()),
    (r'^recenzje/$', ReviewsFeed()),
)