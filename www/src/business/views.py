# -*- coding: utf-8 -*-
"""
Businesses views.

"""
import math
import random
from django.db.models.aggregates import Count, Variance
from django.shortcuts import get_object_or_404

from django.utils.encoding import smart_unicode
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponsePermanentRedirect
from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import redirect
from django.utils.safestring import mark_safe
from django.db import IntegrityError
from django.contrib.auth import login, authenticate, logout
from inlineadmin.decorators import permission_required
from django.contrib.contenttypes.models import ContentType

from business.forms import *

from flagging.models import Flag, flag_translator

from notify.models import notify_user
from search.models import SearchEngine

from location import update_area
from dynamicforms.forms import DynamicForm
from datetime import datetime
from django.utils import translation

from django.http import Http404

from django.utils.translation import ugettext_lazy as _
from forum.models import add_comments

def expand_tree(tree):
        li = []
        items = tree.items()
        items.sort()
        for key, value in items:
            recursive = ""
            if value and isinstance(value, dict):
                recursive = expand_tree(value)
            li.append(u'"%d":{"name": %s,"recursion": %s}' % (key.id, '"'+key.name+'"', recursive or 'null'))
        html = u"{%s}" %  u", ".join(li)
        return html

def category_widget_js(request):
    html = expand_tree(Category.get_category_tree())
    return render_to_response('js/category.widget.js',
                          {'category_tree':mark_safe(html)},
                          context_instance=RequestContext(request),
                          mimetype="application/javascript")

#noinspection PyUnusedLocal
def review_add_confirm_email(request, form, **kwargs):

    return render_to_response('business/review.confirm.html',
        context_instance=RequestContext(request, {"business": form.instance.business, "form": form}))

def business_detail(request, business_slug, **kwargs):
    """
    Shows details of business with given id.
    If flag is given then also tries to set flag to given id

    **Required arguments**

    None

    **Context:**

    ``business``
        Business with given busines_id

    ``reviews``
        reviews of given business

    ``gallery``
        gallery attached to business

    ``flagged_by``
        Users who bookmarked business

    **Template:**

    business/business.html.html.

    """
    try:
        business = Business.objects.select_related('gallery__representative','main_review').get(slug=business_slug)
    except Business.DoesNotExist:
        raise Http404
    
    # Get reviews
    reviews_with_related = business.review_set.select_related('user__profile__gallery__representative', 'business')
    reviews = reviews_with_related.extra(
                    select={'userlang': "lang = '" + translation.get_language() + "'",
                            }).filter(active=1).order_by("-userlang","-id")
    reviews_hidden = reviews_with_related.filter(business__exact=business.id).filter(active=0)

    # Update area to given city
    if business.city != request.session['area'].name:
        try:
            area = Area.objects.get(name__iexact=business.city)
            update_area(request, area.slug)
        except Area.DoesNotExist:
            pass

    # RECOMMENDATIONS

    # recommendation updates once a week so corresponding business might not
    # exist
    recommendations = None

    rec_ids = business.business_recommendation_set.all().values_list\
        ('recommendation', flat=True)
    recommendations = Business.objects.filter(id__in=rec_ids)[:3]

    area = request.session['area']
    if not request.session.has_key( 'stat_recommendation_ver' ):
        area_all = Business.objects.filter(position__within=area.poly).annotate(ratings=Count('review__rating')).exclude(id=business.id)
        request.session['stat_recommendation_ver'] = random.randint(0,2)
        if request.session['stat_recommendation_ver'] == 1:
            recommendations = area_all.order_by('-ratings')[:4]
        elif request.session['stat_recommendation_ver'] == 2:
            recommendations = area_all.annotate(variance=Variance('review__rating')).filter(ratings__gt = 2).order_by('-variance')[:4]

    if not recommendations:
        recommendations = Business.objects.select_related('business__gallery__representative').all().filter(categories=business.categories.all()[:1]).filter(position__within=area.poly).filter(rating__gte=3).order_by('?')[:3]
    recommendations = recommendations.select_related('main_review', 'gallery__representative')

    # Favorite
    bookmarked_by = Flag.objects.filter(content_type__pk=22,
                                    object_id=business.id,
                                    flag=flag_translator('ulubiony')).select_related('user__profile__gallery__representative')
    if request.user in bookmarked_by == 1:
        business.is_favorite = True

    new_review = kwargs.get("new_review", None)

    if not request.user.is_anonymous():
        FormClass = ReviewForm
    else:
        FormClass = ReviewFormUnauthorized

    if request.POST:
        form = FormClass(request.POST)
        
        form.instance.business = business
        form.instance.user = None
        if not request.user.is_anonymous():
            form.instance.user = request.user

        # Check if user is anonymous
        #  - if not, add review
        #  - if yes, check if he provided existing e-mail
        #    - if not, add review
        #    - if yes set the flag needs_email_confirm and
        #       redirect user to confirmation form (i.e. + password)
        if form.is_valid():
            # if e-mail exists -> add password field to form
            if form.needs_email_confirm:
                return review_add_confirm_email(request, form)

            # get profile - if user is registered login him
            profile = form.instance.user.get_profile()
            if profile.registered and form.instance.user.is_anonymous():
                login(request, form.instance.user)

            # save review
            form.save()

            notify_user(request, _(u'Your review has been added. Thank you.'), None)

            # reset form
            form = FormClass()
        else:
            notify_user(request, _(u'Error in the review form'), None)
    else:
        form = FormClass()

    # Choose the sponsored business
    business_sponsored = None
    
    # spns = Business.advert_objects.with_active_advert("sponsored_link_business").filter(categories__in=business.categories.all()).order_by("?")
    # if spns.count():
    #     business_sponsored = spns[0]
    if (not business_sponsored) and (len(recommendations) > 2):
        business_sponsored = recommendations[2]
    # if business_sponsored == business:
    #     business_sponsored = None

    # attach posts GENERIC, TODO nicer (acording to my research it is probably impossible
    # through django, but still can be generalized at least a little bit
    add_comments(reviews)

    events = business.event_set.all().filter(event_date__gt=datetime.now()).select_related('business__gallery__representative')

    return render_to_response('business/business.html',
        context_instance=RequestContext(request,
            {"business": business,
            "events": events,
            "reviews": reviews,
            "recommendations": recommendations,
            "bookmarked_by": bookmarked_by,
            "new_review": new_review,
            "form": form,
            "business_sponsored": business_sponsored,
            "reviews_hidden": reviews_hidden,
            }))

@login_required()
def business_add_to_category(request, category_id):
    """
    Gives form to add business

    **Required arguments**

    None

    **Optional arguments**

    ``request.POST``
        Data from form.

    **Context:**

    ``form``
        Business add form

    **Template:**

    business/business.add.html.

    """

    cat = Category.objects.get(id=category_id)

    if request.POST:
        form = BusinessForm(request.POST)
        if form.is_valid():
            form.instance.active = True
            form.instance.user = request.user
            form.save()
            c = cat
            while (c is not None) and (c != 0):
                form.instance.categories.add(c)
                c = c.parent

            notify_user(request, 'Lokal został dodany, dziękujemy.', None)
            return business_detail(request, form.instance.slug)

    form = BusinessForm()

    return render_to_response('business/business.add.html', context_instance=RequestContext(request, {"form": form, "category": cat}))

@login_required()
def business_add(request):
    """
    Gives form to add business

    **Required arguments**

    None

    **Optional arguments**

    ``request.POST``
        Data from form.

    **Context:**

    ``form``
        Business add form

    **Template:**

    business/business.add.html.

    """

    if request.POST:
        form = BusinessForm(request.POST)
        if form.is_valid():
            inst = form.instance
            inst.active = True
            inst.user = request.user

            form.save()
            notify_user(request, 'Lokal został dodany, dziękujemy.', None)
            return redirect('business_detail', permanent=True, business_slug=inst.slug)

    else:
        form = BusinessForm()

    return render_to_response('business/business.add.html', context_instance=RequestContext(request, {"form": form}))

@login_required()
@permission_required(1)
def business_edit(request, business_slug):
    business = Business.objects.get(slug__exact=business_slug)

    exform = DynamicForm(InfoField, InfoFieldValue, instance = business) # Create the form
    print InfoField.objects.filter(category__in = business.categories.all())

    if request.POST:
        hfs = HoursFormSet(business, request.POST)
        exform.set_data(request.POST)
        form = BusinessForm(request.POST, instance=business)
        if form.is_valid() and hfs.is_valid():
            hfs.save()
            form.instance.active = True
            form.save()
            notify_user(request, 'Nowe dane lokalu zostały zmienione, dziękujemy.', None)
            return redirect(business, permanent=True)
    else:
        hfs = HoursFormSet(business)
        form = BusinessForm(instance=business)

    return render_to_response('business/business.edit.html', context_instance=RequestContext(request,
            {"form": form,
             "formset": hfs.formset,
             "exform": exform}))

def business_detail_old(request, business_id):
    business = get_object_or_404(Business, id__exact=business_id)
    return redirect(business, permanent=True)

def business_bigmap(request, business_slug):
    business = get_object_or_404(Business, slug__exact=business_slug)
    return render_to_response('business/business.bigmap.html',
        context_instance=RequestContext(request,
            {"business": business,
            }))

def business_city_category(request, category_name, city_slug):
    """
    Shows businesses in given category and city
    
    **Required arguments**

    ``review_id``

    **Template:**

    business/category.html.
    """
    
    area = update_area(request, city_slug)
    try:
        category = Category.objects.get(slug=category_name)
    except:
        category = None
    if category == None:
        try:
            category_trans = CategoryTranslation.objects.get(slug=category_name)
            category = category_trans.category
            return redirect("category_city_listing", permanent=True, category_name=category.slug, city_slug=city_slug)
        except CategoryTranslation.DoesNotExist:
            raise Http404

    cat_id = category.id

    businesses = Business.objects
    if category_name > 0:
        businesses = businesses.filter(categories = category)
#    businesses = businesses.extra(
#                    select={'userlang': "lang = '" + translation.get_language() + "'"})
    businesses = businesses.filter(position__within = area.poly).filter(active=True).order_by('-rating', '-last_edit')[:7]

    categories_main = Category.objects.all().filter(parent__id=0)
    if category_name is not None:
        subcategories = Category.objects.all().filter(parent__id__exact=cat_id)
    else:
        subcategories = categories_main

    categories = Category.objects.all().filter(parent__id=0).annotate(businesses=Count('business__id')).order_by('-businesses')
    category = Category.objects.get(id__exact=cat_id)
    category_path = Category.objects.get_path(cat_id)

    business_sponsored = None
    spns = Business.advert_objects.with_active_advert("sponsored_link_category").filter(categories = category).order_by("?")
    if (len(spns)):
        business_sponsored = spns[0]

    return render_to_response('business/category.html',
        context_instance=RequestContext(request, {"businesses": businesses,
        "subcategories": subcategories,
        "categories_main": categories_main,
        "category": category,
        "category_path": category_path,
        "categories": categories,
        "business_sponsored": business_sponsored,
        }))

def business_city_category_old(request, city_slug, category_id):
    cat = get_object_or_404(Category, id=category_id)
    return HttpResponsePermanentRedirect("http://neib.org" + cat.get_absolute_url())


def business_search(request, category_name):
    """
    Get businesses in given category

    **Required arguments**

    ``category_name``
        Category to get businesses from.

    **Context:**

    ``businesses``
        Business in category

    ``categories``
        List of all categories

    **Template:**

    business/listing.html.

    """
    # create search dict for AJAX
    search_dict = dict()
    search_dict["what"] = request.GET.get('co',"")
    search_dict["where"] = request.GET.get('gdzie',request.session['area'].name)
    search_dict["category"] = category = request.GET.get('kategoria',None)

    search = SearchEngine()
    if (len(search_dict["what"])):
        search.filter_what(search_dict["what"])

    if (category is not None) and (category != 0) and (category != '0') and (category != ''):
        search.filter_category(category)

    search.filter_where(search_dict["where"])

    # slice results and count pages
    businesses = search.businesses[:10]
    num_of_results = search.businesses.count()
    pages = math.ceil(num_of_results / 10)
    if pages == 1:
        pages = 0
    if pages > 10:
        pages = 10

    # get adv
    business_sponsored = None

    if search.businesses.count():
        b0 = search.businesses[0]
        spns = Business.advert_objects.with_active_advert("sponsored_link_search").filter(categories__in = b0.categories.all).order_by("?")
        if (len(spns)):
            business_sponsored = spns[0]

    return render_to_response('business/listing.html',
        context_instance=RequestContext(request, {"businesses": businesses,
        "search": search_dict,
        "pages": range(1,int(pages)+1),
        "business_sponsored": business_sponsored,
        }))

@user_passes_test(lambda user: user.is_staff or user.is_superuser)
def category_tag_chooser(request):
    form = CategoryTaggingForm()
    if request.method == "POST":
        form = CategoryTaggingForm(request.POST)
        if form.is_valid():
            Tag.objects.update_tags(form.cleaned_data['category'], form.cleaned_data['tags'], form.cleaned_data['fitting'])
            return HttpResponse("OK")
    cat_list = []
    for cat in Category.objects.all():
        cat_list.append(tuple([cat.pk, ', '.join([t.name for t in Tag.objects.get_for_object(cat)])]))
    return render_to_response('business/category.add.tag.html',
                          {'form':form,
                           'categories': cat_list},
                          context_instance=RequestContext(request))
