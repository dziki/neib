
@login_required()
def business_list_del(request, business_list_id):
    businesslist = BusinessList.objects.get(id__exact=business_list_id)
    if businesslist.user == request.user:
        businesslist.delete()
    from community.views import business_lists
    return business_lists(request, request.user.id)

def business_list_detail(request, business_list_id, flag=None):
    #TODO what should i do with the flags
    """
    Shows details of businessList with given id.
    If flag is given then also tries to set flag to given id

    **Required arguments**

    None

    **Optional arguments**

    ``flag``
        Flag to set.

    **Context:**

    ``business``
        Business with given busines_list_id


    **Template:**
    TODO
    //business/business.html.html. old

    """
    businesslist = BusinessList.objects.get(id__exact=business_list_id)

    return render_to_response('business/list.html',
                context_instance=RequestContext(request,
                             {'businesslist' : businesslist}))

@login_required()
def business_list_add(request, business_list_id=None):
    businesslist = None
    if business_list_id:
        businesslist = BusinessList.objects.get(id__exact=business_list_id)
        if businesslist.user != request.user:
            businesslist = None

    if request.POST:
        form = BusinessListForm(request.POST, instance = businesslist)
        if form.is_valid():
            form.instance.user = request.user
            new_business_list = form.save()
            return business_list_detail(request, new_business_list.id)
    else:
        form = BusinessListForm(instance = businesslist)

    return render_to_response('business/list.add.html', context_instance=RequestContext(request, {"form": form}))

@login_required()
def business_list_item_add(request, business_list_id, business_slug):
    business_list = BusinessList.objects.get(id__exact=business_list_id, user=request.user)
    business = Business.objects.get(slug__exact=business_slug)
    business_list.businesses.add(business)
    return business_list_detail(request, business_list_id)

@login_required()
def business_list_item_del(request, business_list_id, business_slug, flag=None):
    # TODO: check if it is user's location!
    business_list = BusinessList.objects.get(id__exact=business_list_id, user=request.user)
    business = Business.objects.get(slug__exact=business_slug)
    business_list.businesses.remove(business)
    return business_list_detail(request, business_list_id, flag)

def business_loader(request):
    filename_in = 'csv/teatry.csv'

    file_handle = open(filename_in, 'r')
    lines = file_handle.readlines()
    file_handle.close()

    title = lines[0]
    keys = title.split(';')

    for line in lines[1:]:
        line = line.decode('utf8')
        vals = line.split(';')
        item = {}
        for i in range(0, len(keys)-1):
            item[keys[i]] = vals[i]

        buss = Business()
        name = item['name']
        buss.name = name[0] + name[1:].lower()
        buss.address = item['street']
        buss.zip = ''
        buss.city = item['city']
        buss.phone = item['phone']
        cat = Category.objects.get(id=102)
        buss.save()
        #noinspection PyUnresolvedReferences
        buss.categories.add(cat)
        buss.save()

def business_batch(request):
    cat = Category.objects.get(name='Kawiarnie')
    businesses = cat.business_set.all()

    for bus in businesses:
        bus.name = bus.name[0] + bus.name[1:].lower()
        bus.save()

def business_batch_slug(request):
    businesses = Business.objects.all().filter(id__gt=2228)

    for bus in businesses:
        slug = bus.name + ' ' + bus.city
        bus.slug = unique_slug(Business, 'slug', slug)
        bus.save()

def business_batch_pos(request):
    businesses = Business.objects.all().order_by('-id')
    ret = ""

    for bus in businesses:
        bus.update_pos()

    return HttpResponse('juz:<br>' + ret)
