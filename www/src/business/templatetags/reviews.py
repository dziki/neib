"""Splits query results list into multiple sublists for template display."""

from django.template import Library, Context
from django import template

register = Library()

@register.filter
def nooptions(value):
    t = template.loader.get_template('include/review.nooptions.html')
    return t.render(Context({'item': value}, autoescape=True))