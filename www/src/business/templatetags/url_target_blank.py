"""Splits query results list into multiple sublists for template display."""

from django.template import Library

register = Library()

@register.simple_tag
def url_target_blank(text):
    return (text.replace('<a ', '<a target="_blank" '))

url_target_blank = register.filter(url_target_blank)
url_target_blank.is_safe = True