"""Splits query results list into multiple sublists for template display."""

from django.template import Library
from django.conf import settings
from math import ceil

register = Library()

@register.simple_tag
def stars(rating, bg=True):
    full_width = 85
    height = 18
    imgbg = settings.MEDIA_URL + 'gfx/stars-small-0.gif'

    if rating is None:
        rating = 0

    img_id = int(ceil(rating))
    width = ceil(full_width * rating / 5.0)
    
    imgurl = settings.MEDIA_URL + 'gfx/stars-small-' + img_id.__str__() +'.gif'

    ret = '<img width="'+(full_width * img_id / 5).__str__()+'" height="'+height.__str__()+'" src="' + imgurl + '" style="clip:rect(0px,' + width.__str__() + 'px,' + height.__str__() + 'px,0px); position: absolute"/>'
    if bg:
        ret += '<img width="'+(full_width).__str__()+'" height="'+height.__str__()+'" src="' + imgbg + '" style="clip:rect(0px,' + (full_width).__str__() + 'px,' + height.__str__() + 'px,' + width.__str__() + 'px); position: absolute;"/>'

    return ret

@register.simple_tag
def stars_no_bg(rating):
    return stars(rating, bg=False)