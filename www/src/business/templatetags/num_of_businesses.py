"""Splits query results list into multiple sublists for template display."""

from django.template import Library
register = Library()

@register.simple_tag
def num_of_businesses(category, area):
    return category.num_of_businesses(area)
