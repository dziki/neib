"""Splits query results list into multiple sublists for template display."""

from django.template import Library
from business.models import Business
from django.utils.safestring import mark_safe
from django.utils.encoding import smart_unicode

register = Library()

@register.filter
def as_dict(business):
    res = u"{ \"id\": " + unicode(business.id) + u", "
    i = 0
    for field in Business.export_fields:
        i = i + 1
        attribute = getattr(business,field)
        if callable(attribute):
            attribute = attribute()
        attribute = smart_unicode(attribute).replace("\n","\\\n")
        attribute = attribute.replace("'","\\'")
        attribute = attribute.replace("\r","")
        res += u" \"" + field + u"\": '" + attribute + u"'"
        if i<len(Business.export_fields):
            res += u", "
    return mark_safe(res + u" }")
as_dict.needs_autoescape = True