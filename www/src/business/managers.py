from django.contrib.gis.db import models as geomodels
from django.db import models
from django.db.models import Q
from location.managers import GeoModelManager
from django.db.models import Avg

from django.db import models
from international.managers import TransManager

class InfoFieldManager(models.Manager):
    def get_fields(self, instance):
        return self.filter(category__in = instance.categories.all()).distinct()
    
class BusinessManager(GeoModelManager):
    def get_active(self):
        qs = self.get_query_set().filter(active=True).annotate(rating=Avg('review__rating')).order_by('-rating')
#        raise qs.query.as_sql() 
        return qs

    def get_best(self, location=None, num=5):
        return self.get_active()[:num]

    def get_new(self, location=None, num=5):
        return self.get_active()[:num]

    def get_recommended(self, location=None, num=5):
        return self.get_active()[:num]
    
    def get(self, *args, **kwargs):
        if not 'active' in kwargs.keys():
            kwargs['active'] = True
        return super(BusinessManager, self).get(*args, **kwargs)
    
#    def get_query_set(self):
#        return super(BusinessManager, self).get_query_set().filter(active=True)
    
#    def all(self):
#        return self.get_query_set()
    
class ReviewManager(TransManager):
    """   """
    def get_new(self, location=None):
        return super(ReviewManager, self).get_query_set().filter(business__active = True).order_by('-id')

class CategoryManager(geomodels.GeoManager):
    """   """
    def get_path(self, parent_id):
        query_set = super(CategoryManager, self).get_query_set()
        cat = query_set.get(id=parent_id)

        cats = list()
        cats.append(cat)
        
        while cats[-1].parent is not None:
            cat = query_set.get(id=cats[-1].parent.id)
            cats.append(cat)
            
        cats.reverse()

        return cats
    
    def get_with_subcategories(self, parent_id):
        query_set = super(CategoryManager, self).get_query_set()
        cats = {0: (parent_id)}
        cats[1] = query_set.filter(Q(parent__id=cats[0]) | Q(id=cats[0]))
        return cats[1]
    
