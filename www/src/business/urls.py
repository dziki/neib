from django.conf.urls.defaults import patterns
from business.views import *

from django.contrib import admin
admin.autodiscover()

from django.utils.translation import ugettext_lazy as _

urlpatterns = patterns('',
    (r'^(?P<business_id>[-\d]+)/$', business_detail_old, {}, 'business_detail_old'),

    ((r'^new/$'), business_add, {}, 'business_add'),
    ((r'^new/(?P<category_id>[-\d]+)/$'), business_add_to_category, {}, 'business_add_to_category'),
    
    ((r'^(?P<business_slug>[-\w]+)/edit/$'), business_edit, {}, 'business_edit'),
    ((r'^map/(?P<business_slug>[-\w]+)/$'), business_bigmap, {}, 'business_bigmap'),
    ('^(?P<business_slug>[-\w]+)/$', business_detail, {}, 'business_detail'),
)