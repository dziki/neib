# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from business.models import Business

import logging as log

log.basicConfig(level=log.INFO)

class Command(BaseCommand):
# DROP TABLE `business_fitting`
# CREATE TABLE `business_fitting` SELECT * FROM `business_fitting_view` WHERE 1
    args = '[]'
    help = 'Creates fitting cache table'
    
    option_list = BaseCommand.option_list
    
    def handle(self, *args, **options):
        for biz in Business.objects.all().order_by('-id'):
            pos = biz.position
            biz.update_pos()
            if pos != biz.position:
                biz.save()
        
        return 