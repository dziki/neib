# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from business.models import Business, Recommend

from django.db.models import Avg

import logging as log

log.basicConfig(level=log.INFO)

class Command(BaseCommand):
    args = '[]'
    help = 'Fills up recomendations'
    
    option_list = BaseCommand.option_list
    
    def handle(self, *args, **options):
        Recommend.objects.all().delete()
        bizs = Business.objects.all().filter(review__isnull=False).distinct()
        for biz in bizs:
            users = User.objects.filter(review__business = biz)
            recommends = Business.objects.exclude(id=biz.id).annotate(rating=Avg('review__rating')).filter(review__user__in=users).order_by('?')[:4]
            i = 0
            for bizrec in recommends:
                r = Recommend()
                r.business = biz
                r.position = i
                r.recommendation = bizrec
                r.checked = True
                r.save()
                i += 1
        return 