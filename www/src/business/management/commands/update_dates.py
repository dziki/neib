# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
from business.models import Review

import logging as log

log.basicConfig(level=log.INFO)

class Command(BaseCommand):
# DROP TABLE `business_fitting`
# CREATE TABLE `business_fitting` SELECT * FROM `business_fitting_view` WHERE 1
    args = '[]'
    help = 'Creates fitting cache table'
    
    option_list = BaseCommand.option_list

    def handle(self, *args, **options):
        for line in open('dates.txt','r').readlines():
            a, b = line.split(', \'')
            a = a[1:]
            b = b[:-4]
            print a, b
            rev = Review.objects.get(id=a)
            rev.date = b
            rev.save()
        return 