# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.core.cache import cache

from business.models import Business, Category

import logging as log

log.basicConfig(level=log.INFO)

class Command(BaseCommand):
    args = '[]'
    help = 'Creates fitting cache table'
    
    option_list = BaseCommand.option_list

    def handle(self, *args, **options):
        cache._cache.flush_all()
        return 