# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from business.models import CategoryForArea

import logging as log

log.basicConfig(level=log.INFO)

class Command(BaseCommand):
    args = '[]'
    help = 'Creates fitting cache table'
    
    option_list = BaseCommand.option_list
    
    def handle(self, *args, **options):
        for cfa in CategoryForArea.objects.all():
            cfa.recalculate()
        return 