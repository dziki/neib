# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
from business.models import Category, CategoryTranslation
from django.utils.translation import ugettext as _

import logging as log

log.basicConfig(level=log.INFO)

def repair_cats(categories):
    for category in categories:
        ct = CategoryTranslation()
        name = category.name
        category.name = _(category.name)
        slug = category.slug
        category.slug = category.generate_slug()
        category.save()
        ct.category = category
        ct.lang = "pl"
        ct.name = name
        ct.slug = slug
        ct.save()
        
        repair_cats(category.category_set.all())

class Command(BaseCommand):
    args = '[]'
    help = 'Creates fitting cache table'
    
    option_list = BaseCommand.option_list

    def handle(self, *args, **options):
        from django.utils import translation
        lang="pl"

        categories = CategoryTranslation.objects.all().filter(lang=lang)
        import codecs
        file = codecs.open('locale/'+lang+'/LC_MESSAGES/'+'django.po', 'a', 'utf-8')
        d = dict()
        for category in categories:
            if d.get(category.category.name, None) == None:
                file.write("\n")
                file.write("msgid \"%s\"\n" % category.category.name)
                d[category.category.name] = True
                file.write(u"msgstr \"%s\"\n" % category.name)
        file.close()
        return 