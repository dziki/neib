# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError

import logging as log
from business.models import Review

log.basicConfig(level=log.INFO)

class Command(BaseCommand):
    args = '[]'
    help = 'Creates fitting cache table'
    
    option_list = BaseCommand.option_list

    def handle(self, *args, **options):
        for review in Review.objects.all():
            review.update_flag_counter(1)
            review.update_flag_counter(3)
            review.update_flag_counter(4)
            review.update_flag_counter(5)
            review.update_flag_counter(6)
