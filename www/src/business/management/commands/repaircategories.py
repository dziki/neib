# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from business.models import Business, Category

import logging as log

log.basicConfig(level=log.INFO)

class Command(BaseCommand):
# DROP TABLE `business_fitting`
# CREATE TABLE `business_fitting` SELECT * FROM `business_fitting_view` WHERE 1
    args = '[]'
    help = 'Creates fitting cache table'
    
    option_list = BaseCommand.option_list
    
    def add_to_cat(self, name, cat_id):
        c = Category.objects.get(id = cat_id)
        bizs = Business.objects.all().filter(name__icontains = name)
        for biz in bizs:
            biz.categories.add(c)

    def handle(self, *args, **options):
#        self.add_to_cat(u'Klub',282)

        
        bizs = Business.objects.all().order_by('-id')
        for biz in bizs:
            biz.fix_categories()
        return 