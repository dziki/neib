# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError

import logging as log

log.basicConfig(level=log.INFO)

class Command(BaseCommand):
# DROP TABLE `business_fitting`
# CREATE TABLE `business_fitting` SELECT * FROM `business_fitting_view` WHERE 1
    args = '[]'
    help = 'Creates fitting cache table'
    
    option_list = BaseCommand.option_list

    def handle(self, *args, **options):
        if len(args) < 1:
            raise CommandError("Not enough arguments")
        cmd = args[0]

        from django.db import connection, transaction
        cursor = connection.cursor()
    
        from importer import business_loader
        business_loader(cmd, "csv/" + cmd + ".csv")
    
        return 