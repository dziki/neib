from django.forms.widgets import Widget, Select,HiddenInput
from django.template import Context, loader
from django.utils.safestring import mark_safe

class SelectCategoryWidget(Widget):
    """

    """
    selected_categories = list()
    
    categories = list()
    
    def __init__(self, *args, **kwargs):
        pass
        
    def render(self, name, value, attrs=None):
        output = []

        choices = []
        select_html = Select(choices=choices).render()
        output.append(select_html)

        return mark_safe(u'\n'.join(output))

class CategoryWidget(HiddenInput):
    is_hidden = False
    def __init__(self, *args, **kwargs):
        if 'attrs' in kwargs and 'id' in kwargs['attrs']:
            self.id = kwargs['attrs']['id']
        else:
            self.id = "category-widget"
        super(CategoryWidget, self).__init__(*args, **kwargs)
    
    @classmethod
    def id_for_label(self,id_):
        return id_
    
    def _expand_tree(self, tree):
        li = []
        items = tree.items()
        items.sort()
        for key, value in items:
            recursive = ""
            if value and isinstance(value, dict):
                recursive = self._expand_tree(value)
            li.append('''{
                            "id": %d,
                            "name": %s,
                            "recursion": %s
                        }''' % (key.id, key.name, recursive))
        html = "\n%s\n" %  "\n\t".join(li)
        return html
        
    def render(self, name, value, attrs=None):
        t = loader.get_template("business/business.add.category.widget.html")
        if isinstance(value, list):
            value = ",".join([int(c).__str__() for c in value])

        c = Context({
                     'id':self.id,
                     'name':name,
                     'hidden': super(CategoryWidget, self).render(name, value, attrs=None)
                     })
        return mark_safe(t.render(c))
            
    class Media:
        js = (
              "/dynamicmedia/category.widget.js",
              )
        css = {
               'all': ('style/screen/widgets/category.widget.css',)
              }

class IPodMenu(HiddenInput):
    is_hidden = False
    def __init__(self, tree, *args, **kwargs):
        self.tree = tree
        self.id = 'ipod-menu'
        self.field_text = kwargs.pop('field_text', None)
        if 'attrs' in kwargs and 'id' in kwargs['attrs']:
            self.id = kwargs['attrs']['id']
        kwargs.update({'attrs':{'id':'%s-hidden'%self.id}})
        super(IPodMenu, self).__init__(*args, **kwargs)
        
    def _expand_tree(self, tree):
        li = []
        items = tree.items()
        items.sort()
        for key, value in items:
            recursive = ""
            if value and isinstance(value, dict):
                recursive = self._expand_tree(value)
            li.append('''{
                            "id": %d,
                            "name": %s,
                            "recursion": %s
                        }''' % (key.id, key.name, recursive))
        html = "\n%s\n" %  "\n\t".join(li)
        return html
    
    @classmethod
    def id_for_label(self,id_):
        return id_
        
    def render(self, name, value, attrs=None):
        return '''<div id="%s">%s
                      <a href="#">%s</a>
                      <div>%s</div>
                  </div>''' % \
                        (self.id,
                         super(IPodMenu, self).render(name, value, attrs=attrs),
                         self.field_text,
                         self._expand_tree(self.tree))
            
    class Media:
        js = (
              'js/widgets/ipod.menu.widget.js',
              'js/jquery.ipodmenu.js'
              )
        css = {
               'all': ('style/screen/widgets/ipod.menu.widget.css',)
              }
        

