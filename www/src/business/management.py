# -*- coding: utf-8 -*- 
from django.db.models import signals
from django.conf import settings

if "notification" in settings.INSTALLED_APPS:
    from notification import models as notification

    #noinspection PyUnusedLocal
    def create_notice_types(app, created_models, verbosity, **kwargs):
        notification.create_notice_type("review_fav_biz", (u"Recenzja ulubionego lokalu"), ("ktoś dodał nową recenzję Twojego ulubionego lokalu"), default=2)
        notification.create_notice_type("review_fav_user", (u"Nowa recenzja ulubionego usera"), ("jeden z Twoich ulubionych użytkowników dodał recenzję"), default=2)
        notification.create_notice_type("review_friend", (u"Nowa recenzja znajomego"), ("Twój znajomy napisał nową recenzję"), default=2)
        notification.create_notice_type("review_reviewer", (u"Ktoś odpisał na Twoją recenzję"), ("Ktoś odpisał na Twoją recenzję"), default=2)

    signals.post_syncdb.connect(create_notice_types, sender=notification)
else:
    print "Skipping creation of NoticeTypes as notification app not found"
