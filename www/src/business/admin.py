from django.contrib import admin
from business.models import *
from forms import BusinessAdminForm, CategoryAdminForm
from tagging.models import Tag

class HoursInline(admin.TabularInline):
    model = Hours
    extra = 7
    max_num = 7
    
class BusinessAdmin(admin.ModelAdmin):
    form = BusinessAdminForm
    
    list_display = ('name','owner','city','state','zip','phone','www')
    list_filter = ('categories',)
    filter_horizontal = ('categories',)
    #fields = ['name','owner','city','address','state','zip','phone','www', 'tags', 'categories','gallery','comment']
    search_fields = ['@name']
    inlines = [HoursInline,]
    fieldsets = (
                    (None, {
                        'fields': ('name', 'city','address','state','zip','phone','www', 'tags', 'categories',)
                    }),
                    ('Advanced options', {
                        'classes': ('collapse',),
                        'fields': ('owner','gallery','comment','opened')
                    }),
                )
    
    def save_model(self, request, obj, form, change):
        res = super(BusinessAdmin, self).save_model(request, obj, form, change)
        data = dict(form.cleaned_data['tags'])
        Tag.objects.update_tags(obj, ', '.join(data.keys()))
        Tag.objects.update_fitting(obj, data)
        return res
    
    def queryset(self, request):
        qs = self.model._default_manager.get_query_set()
#        qs = qs.annotate(num_reviews = Count('review')).filter(num_reviews__gte = 1)
        return qs
        

class CategoryAdmin(admin.ModelAdmin):
    form = CategoryAdminForm
    
    list_display = ('name','parent')
    list_filter = ('parent',)
    prepopulated_feields = {'slug': ('name',),}
    
    def save_model(self, request, obj, form, change):
        res = super(CategoryAdmin, self).save_model(request, obj, form, change)
        data = dict(form.cleaned_data['tags'])
        Tag.objects.update_tags(obj, ', '.join(data.keys()))
        Tag.objects.update_fitting(obj, data)
        return res

class ReviewAdmin(admin.ModelAdmin):
    list_display = ('user','business','rating')
    date_hierarchy = 'date'
    ordering = ('-date',)
    list_filter = ('user',)

class BusinessListAdmin(admin.ModelAdmin):
    list_display = ('user', 'name')
    ordering = ('-id',)
    list_filter= ('user',)
    filter_horizontal = ('businesses',)

admin.site.register(Business, BusinessAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(InfoField)
admin.site.register(Hours)
admin.site.register(Review, ReviewAdmin)
admin.site.register(BusinessList, BusinessListAdmin)

