# -*- coding: utf-8 -*-
from django.forms import ModelForm, CharField, Textarea
from models import Business, Category
from tagging.models import Tag

DEFAULT_FITTING = 0.8

class TagableModelAdminForm(ModelForm):
    #tags = CharField(max_length=100,widget=TextInput(attrs={'size':'100'}))
    tags = CharField(widget=Textarea(), help_text=u"Tagi powinny być wpisywane każdy w kolejnej linii \
                                                    bądź rozdzielone przecinkiem. Na końcu każdej linii znajduje się \
                                                    liczba 'przystosowania' poprzedzona znakiem '#'. Przy wpisywaniu liczba \
                                                    przystosowania może być pominięta i wtedy zostanie wypełniona wartością domyślną (aktualnie %.2f)" \
                                                    % DEFAULT_FITTING)
    
    def __init__(self, *args, **kwargs):
        if 'instance' in kwargs:
            inst = kwargs['instance']
            tags = ["%s#%.2f"%(t.name, t.fitting_for_object(inst)) for t in Tag.objects.get_for_object(inst)]
            tags = "\n".join(tags)
            kwargs.update({'initial':{'tags':tags}})
        super(TagableModelAdminForm, self).__init__(*args, **kwargs)
    
    def clean_tags(self):
        tags = self.cleaned_data['tags']
        tags = tags.split("\n")
        res = []
        for t in tags:
            if "#" in t:
                fit = t.split("#")[-1]
                tag = t[:-(len(fit)+1)]
                for stag in tag.split(','):
                    res.append((stag.strip(),float(fit)))
            else:
                for stag in t.split(','):
                    res.append((stag.strip(),DEFAULT_FITTING))
        return res

class BusinessAdminForm(TagableModelAdminForm):
    class Meta:
        model = Business
        
class CategoryAdminForm(TagableModelAdminForm):
    class Meta:
        model = Category