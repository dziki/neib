# -*- coding: utf-8 -*-
"""
Models for bussinesses

"""
import datetime
import unicodedata
from contrib.optimization import keep_result
from django.utils import translation

from flagging.models import FLAG_DICTIONARY
from location.models import GeoModel

from serializers.datadumper import DataDumper

from date.models import DAYS_OF_THE_WEEK

from location.models import get_position_from_address

from common.string import unique_slug
from search.managers import SearchManager
from gallery.models import GalleryField, Photo, Gallery
from adverts.managers import BusinessAdvertManager, add_manager
from forum.models import Post

from notification import models as notification
from community.models import Friendship
from flagging.models import Flag, flag_translator

from date.models import TimedModel
from django.db.models import Avg

# stars rating
from dynamicforms.models import DynamicField, DynamicFieldValue
from business.managers import InfoFieldManager, CategoryManager, BusinessManager, ReviewManager

from django.contrib.gis.db import models as geomodels
from django.db import models
from django.contrib.auth.models import User
from django.contrib.contenttypes import generic
from django.utils.functional import curry
from django.db.models.signals import post_init

from django.utils.translation import ugettext_lazy as _
from django.utils import translation
from django.core.cache import cache

from django.conf import settings
from django.utils.encoding import smart_unicode

from denorm import denormalized, depend_on_related

INTEGER_CHOICES = (
    (1, _(u"Tragedy")),
    (2, _(u"Poor")),
    (3, _(u"Mediocre")),
    (4, _(u"Great")),
    (5, _(u"Outstanding")),
)
class InfoField(DynamicField):
    objects = InfoFieldManager()

from location.models import Area
class CategoryForArea(TimedModel):
    area = geomodels.ForeignKey(Area)
    category = geomodels.ForeignKey('Category')
    businesses = geomodels.IntegerField()

    def recalculate(self):
        #noinspection PyUnresolvedReferences
        tp = type(self)
        key = "%s.%s.%d.%s__business_count" % (tp.__module__, tp.__name__, self.category.id, self.area.slug)
        if key not in cache:
            self.businesses = self.category.business_set.filter(position__within = self.area.poly).count()
            cache.set(key, self.businesses, settings.CACHE_BUSINESS_COUNT) # once a day
        else:
            self.businesses = cache.get(key, 0)
#        self.save()
        return

class CategoryTranslation(models.Model):
    category = models.ForeignKey('Category', verbose_name="Category")
    slug = models.SlugField('slug', unique=False, max_length=100, blank=True, null=True)
    name = models.CharField(max_length=100, verbose_name="Name")
    lang = models.CharField(max_length=5, verbose_name="Language")
    
    @classmethod
    def get_translation_table(cls, lang):
        d = dict()
        key = "category_trans_tbl_%s" % (lang)
        if key not in cache:
            items = CategoryTranslation.objects.filter(lang=lang).select_related("category")
            for item in items:
                d[item.category.id] = item.name
            cache.set(key, d, 1000) #settings.CACHE_BUSINESS_COUNT) # once a day
        else:
            d = cache.get(key, 0)
        return d

class Category(TimedModel):
    """ Business category. If parent is null then that is topmost category """
    parent = models.ForeignKey('self', blank=True, null=True, verbose_name="Nadrzędna")
    name = models.CharField(max_length=100, verbose_name="Nazwa", db_column="name")
    slug = models.SlugField('slug', unique=True, max_length=100)
    main_form = models.CharField(max_length=100, verbose_name="Forma podstawowa", blank=True, null=True)

    objects = CategoryManager()
    fields = models.ManyToManyField(InfoField, null=True, blank=True)

    export_fields = ('id','parent_id','name','version')
    export_fields_ex = ()

    class Meta:
        ordering = ["name"]
        
    def generate_slug(self): 
        slug = ''
        if self.parent and self.parent != 0:
            slug += self.parent._name + ' '
        slug += self._name
        slug = unique_slug(Category,'slug', slug)
        return slug

    def save(self, *args, **kwargs):
        """
        Before saving get proper location (from address).
        """
        if self.slug is None or len(self.slug)<=2:
            self.slug = self.generate_slug()
        super(Category, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return "/category/%s/" % self.slug

    def num_all_items(self):
        return 0

    def num_of_businesses(self, area):
        cfa = CategoryForArea(category=self, area = area, businesses = 0)
        cfa.recalculate()
        return cfa.businesses

    def icon(self):
        trans_table = CategoryTranslation.get_translation_table("pl")
        name = trans_table[self.id]
        name = name.replace(u'ł', u'l')
        name = unicodedata.normalize ('NFKD', name).encode ('ASCII', 'ignore')
        name = name.lower().replace(" ", ".")
        return name + ".png"

    @classmethod
    def get_category_tree(cls):
        root = Category.objects.get(id=0)
        return cls._build_tree(root)

    @classmethod
    def _build_tree(cls,root):
        tree = {}
        for cat in root.category_set.all():
            tree.update({cat : cls._build_tree(cat)})
        return tree
    
class CategoryTag(models.Model):
    category = models.ForeignKey(Category, verbose_name="Kategoria")
    tag = models.CharField(max_length=100, verbose_name="Tag")
    suits = models.FloatField(verbose_name="Dopasowanie")

VERIFY_CHOICES = (
    (0, 'Nie'),
    (1, 'OK'),
    (2, 'Nie można się dodzwonić'),

    (3, 'Odmowa'),
    (4, 'Brak kompetencji'),
    (5, 'Błędn telefon'),
    (6, 'Nie ma lokalu'),
    (7, ''),
)
SHORT_DAY = dict()
SHORT_DAY[1] = (u'mon.')
SHORT_DAY[2] = (u'tue.')
SHORT_DAY[3] = (u'wed.')
SHORT_DAY[4] = (u'thu.')
SHORT_DAY[5] = (u'fri.')
SHORT_DAY[6] = (u'sat.')
SHORT_DAY[7] = (u'sun.')

class AdTempManager(models.Manager):
    #noinspection PyUnusedLocal
    def is_enabled(self, key=0):
        return True

class Business(GeoModel):
    """ Business description """
    slug = models.SlugField('slug', unique=True, max_length=100)
    user = models.ForeignKey(User, blank=True, null=True, verbose_name=_("Added by"), related_name="businesses_added")
    owner = models.ForeignKey(User, blank=True, null=True, verbose_name=_("Owner"))
    categories = models.ManyToManyField(Category, verbose_name=_("Categories"))
    name = models.CharField(max_length=100, verbose_name=_("Name"))
    address = models.CharField(max_length=100, verbose_name=_("Address"))
    city = models.CharField(max_length=100, verbose_name=_("City"))
    phone = models.CharField(max_length=100, verbose_name=_("Phone"), null=True, blank=True)
    state = models.CharField(max_length=100, verbose_name=_("State"), null=True, blank=True)
    zip = models.CharField(max_length=10, verbose_name=_("Postal code"), null=True, blank=True)
    www = models.CharField(max_length=100, verbose_name=_("WWW"), null=True, blank=True)
    email = models.CharField(max_length=100, verbose_name=_("E-mail"), null=True, blank=True)
    opened = models.BooleanField(verbose_name=_("Open"), default=1)
    active = models.BooleanField(verbose_name=_("Active"), default=1)
    verified = models.IntegerField(verbose_name=_("Checked"), choices=VERIFY_CHOICES, default=1)
    main_review = models.OneToOneField("Review", related_name='main_for', null=True, blank=True)

    comment = models.TextField(max_length=255, verbose_name=_("Comment"), null=True, blank=True)

    specialities = models.TextField(max_length=1000, verbose_name=_("Our strengths"), null=True, blank=True)
    offer = models.TextField(max_length=1000, verbose_name=_("Offer"), null=True, blank=True)
    history = models.TextField(max_length=1000, verbose_name=_("History"), null=True, blank=True)
    owners = models.TextField(max_length=1000, verbose_name=_("About owners"), null=True, blank=True)

    #Pawel TODO: new field in Business --> that's how we can manage this table (by date)
    #added = models.DateField('Data dodania',default = datetime.date)

    gallery = GalleryField("business")

    search = SearchManager()
    objects = BusinessManager()
    advert_objects = BusinessAdvertManager()

    flags = generic.GenericRelation(Flag)

    last_edit = models.DateTimeField(_("Last change"), auto_now=True)

    def promotions(self):
        #noinspection PyUnresolvedReferences
        return self.promotion_set.all().filter(date_to__gte=datetime.datetime.now())

    open = 1

    fields = models.ManyToManyField(InfoField, through='InfoFieldValue')
    export_fields = ('slug', 'name', 'open', 'address', 'phone', 'num_of_reviews', 'zip', 'city', 'review_body', 'rating', 'latitude', 'longitude', 'photo', 'num_of_photos','version', 'promotions')
    export_fields_ex = ('www', 'hours', 'extra', 'review_author')
    #TODO: 'categories'

    ordering = ["name"]

    adverts = AdTempManager()

    def get_json(self):
        dumper = DataDumper()
        return dumper.dump(self)
    
    def get_full_address(self):
        return self.address + ", " + self.city + " " + self.zip

    @keep_result
    def info(self):
        return InfoFieldValue.objects.filter(obj__id = self.id)\
                                     .select_related()

    @keep_result
    def extra(self):
        d = dict()
        for item in self.info():
            k = item.field.name
            v = item.value
            if v == "on":
                v = "yes"
            elif v == "off":
                v = "no"
            d[_(k)] = _(v)
        return d

    def update_pos(self):
        am = get_position_from_address(self.address + ", " + self.city)
        self.position = am.position
        return

    @denormalized(models.FloatField)
    @depend_on_related('Review', foreign_key='business')
    def rating(self):
        r = self.review_set.filter(active=1).aggregate(Avg('rating'))
        return r['rating__avg']

#    @keep_result
#    def rating(self):
#        r = self.review_set.filter(active=1).aggregate(Avg('rating'))
#        return r['rating__avg']
        

    def show_hours(self):
        class Block:
            pass

        def new_block(hour):
            b = Block()

            b.first_day = hour.day
            b.last_day = hour.day
            b.open = hour.time_from
            b.close = hour.time_to

            return b

        if not self.hours:
            self.hours = Hours.objects.filter(business=self).order_by('day')
        hours = self.hours
        
        # len instead of count on purpose, since we use this queryset later!
        if len(hours):
            return None

        blocks = []

        b = new_block(hours[0])
        blocks.append(b)

        for hour in hours:
            b = blocks[-1]
            if hour.time_from == b.open and hour.time_to == b.close:
                if hour.day > b.last_day + 1:
                    b = new_block(hour)

                    blocks.append(b)
                else:
                    b.last_day = hour.day
            else:
                b = new_block(hour)
                blocks.append(b)


        result = []
        for b in blocks:
            if b.first_day == b.last_day:
                line = _(SHORT_DAY[b.first_day])
            else:
                line = u"%s-%s" % (_(SHORT_DAY[b.first_day]), _(SHORT_DAY[b.last_day]))

            op = b.open.strftime(u"%H:%M")
            cl = b.close.strftime(u"%H:%M")
            result.append(u"%s: %s-%s" % (line, op, cl))

        return u"\n".join(result)

    def get_photo(self):
        #photo = Photo.objects.get_gallery_photo(self.gallery)
        photo = self.gallery.get_representative_photo()
        if photo is None:
            photo = Photo()
            photo.disp = 'building'
        return photo

    photo = property(get_photo)

    def get_main_review(self):
        """
        Returns the review that should be displayed as main
        """
        if not self.main_review:
            revs = self.review_set.all().extra(
                    select={'userlang': "lang = '" + translation.get_language() + "'"},
                    order_by = ["-userlang"])
            if revs.count() == 0:
                return {'content': ''}
            self.main_review = revs[0]
            self.save()
        return self.main_review

    def review_body(self):
        rev = self.get_main_review()
        if rev:
            try:
                return rev.content
            except:
                return rev['content']
        return ""

    def review_author(self):
        if self.get_main_review():
            return self.get_main_review().user
        return None

    def num_of_reviews(self):
        return self.review_set.all().count()

    def num_of_photos(self):
        return Photo.objects.num_of_gallery_photos(self.gallery)

    def save(self, *args, **kwargs):
        """
        Before saving get proper location (from address).
        """
        if self.slug is None or len(self.slug)<=2:
            self.slug = unique_slug(Business,'slug', self.name + ' ' + self.city)
        self.update_pos()

        if self.gallery is None:
            self.gallery = Gallery.objects.create()

        super(Business, self).save(*args, **kwargs)

    # executed only on with BusinessForm
    def fix_categories(self):
        cats = self.categories.all()
        for c in cats:
            cat = c.parent
            while cat is not None:
                self.categories.add(cat)
                cat = cat.parent

    def __str__(self):
        return self.name
    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('business_detail', (), {
            'business_slug': self.slug})

    @models.permalink
    def get_new_review_url(self):
        return ('review_add', (), {
            'business_slug': self.slug})

    @models.permalink
    def get_big_map_url(self):
        return ('business_bigmap', (), {
            'business_slug': self.slug})

    @models.permalink
    def get_bookmark_url(self):
        return ('business_detail_flag', (), {
            'business_slug': self.slug,
            'flag': 'ulubiony'})

    @models.permalink
    def get_spam_url(self):
        return ('business_detail_flag', (), {
            'business_slug': self.slug,
            'flag': 'spam'})

    @classmethod
    def get_sidebar_template(cls):
        return "business/business.sidebar.html"

    class Meta:
        ordering = ["-id"]

#    raise cats

#tagging.register(Business)
models.signals.post_init.connect(add_manager, sender=Business)

class InfoFieldValue(DynamicFieldValue):
    field = models.ForeignKey(InfoField)
    obj = models.ForeignKey(Business)
    objects = geomodels.Manager()

class Hours(models.Model):
    """ reserved (TODO). We will store additional info of business in given category """
    business = models.ForeignKey(Business, verbose_name=_("Venue"))

    time_from = models.TimeField(verbose_name=_("From"))
    time_to = models.TimeField(verbose_name=_("To"))

    day = models.IntegerField(max_length=1, verbose_name=_("Day"), choices=DAYS_OF_THE_WEEK)
    objects = geomodels.Manager()

class Vote(TimedModel):
    review = models.ForeignKey('Review')
    user = models.ForeignKey(User)
    value = models.IntegerField(verbose_name=_("Value"))

class Review(TimedModel):
    """ Review attached to Business """
    user = models.ForeignKey(User, blank=True, null=True)
    business = models.ForeignKey(Business, blank=True, null=True)
    content = models.TextField(verbose_name=_("Content"))
    rating = models.PositiveSmallIntegerField(choices=INTEGER_CHOICES,
                                              verbose_name=_("Rate"),
                                              blank=False,
                                              default=INTEGER_CHOICES[0])
    active = models.BooleanField(verbose_name=_("Active"), default=1)

    flags = generic.GenericRelation(Flag)
    
    comments = generic.GenericRelation(Post,
                               content_type_field='content_type',
                               object_id_field='object_id')
    
    video = models.CharField(max_length=100, verbose_name=_("Video"), null=True, blank=True)
    lang = models.CharField(max_length=6, verbose_name=_("Language"), null=False, blank=False, default='pl')

    removed = models.TextField(max_length=255, verbose_name=_("Removed"), null=True, blank=True)

    objects = ReviewManager()

    export_fields = ('content', 'rating', 'user', 'content', 'business',)
    export_fields_ex = ( )
    
    def get_comments(self):
        try:
            return self._comments
        except:
            pass
        return self.comments.reverse()
    
    useful = models.IntegerField(max_length=100, default=0)
    interesting = models.IntegerField(max_length=100, default=0)
    cool = models.IntegerField(max_length=100, default=0)
    funny = models.IntegerField(max_length=100, default=0)
    spam = models.IntegerField(max_length=100, default=0)

    class Meta:
        ordering = ["-id"]

    def __str__(self):
        #noinspection PyUnresolvedReferences
        return self.business.name + ' - ' + self.user.profile.__str__()
    def __unicode__(self):
        return self.__str__()

    # TODO: DO OGARNIĘCIA
    def get_flag_count(self, flag):
        return Flag.objects.num_of_flags(self,flag)
    def update_flag_counter(self, flag_id):
        key = (key for key,value in FLAG_DICTIONARY.items() if value==flag_id).next()
        if key == 'useful':
            self.useful = self.get_flag_count(key)
        if key == 'funny':
            self.funny = self.get_flag_count(key)
        if key == 'cool':
            self.cool = self.get_flag_count(key)
        if key == 'interesting':
            self.interesting = self.get_flag_count(key)
        if key == 'spam':
            self.spam = self.get_flag_count(key)
        self.save()
    
    def get_useful_count(self):
        return self.useful
    def get_funny_count(self):
        return self.funny
    def get_cool_count(self):
        return self.cool
    def get_interesting_count(self):
        return self.interesting
    def get_spam_count(self):
        return self.spam

    # url generators
    def get_funny_url(self):
        return "/recenzja/%i/smieszna/" % self.id
    def get_useful_url(self):
        return "/recenzja/%i/uzyteczna/" % self.id
    def get_interesting_url(self):
        return "/recenzja/%i/interesujaca/" % self.id
    def get_cool_url(self):
        return "/recenzja/%i/fajna/" % self.id
    def get_bookmark_url(self):
        return "/recenzja/%i/ulubiona/" % self.id
    def get_spam_url(self):
        return "/recenzja/%i/spam/" % self.id

    def get_absolute_url(self):
        return self.business.get_absolute_url() + "#recenzja=%i" % self.id

    def add_comment(self, text, user):
        return Post.objects.add_comment(self, text, user)

    def save(self, *args, **kwargs):
        """
        """
        #noinspection PyUnresolvedReferences
        if (self.content.find("[/url]") > -1) or (self.content.find("</a>") > -1):
            return

        super(Review, self).save(*args, **kwargs)
        
def post_on_blip(sender, instance, created, *args, **kwargs):
    if not created:
        return
    try:
        import pycurl
        bus = instance.business
        status = u"status[body]=W portalu " + u" http://neib.org" + bus.get_absolute_url() + u" pojawiła się nowa recenzja \"" + bus.__unicode__() + u"\" w #" + bus.city.lower()
        c = pycurl.Curl()
        c.setopt(pycurl.URL, "http://api.blip.pl/statuses")
        c.setopt(pycurl.HTTPHEADER, ["Accept: application/json", "X-Blip-api: 0.02"])
        c.setopt(pycurl.POSTFIELDS, smart_unicode(status).encode('utf-8'))
        c.setopt(pycurl.HTTPAUTH, pycurl.HTTPAUTH_BASIC)
        c.setopt(pycurl.USERPWD, "dood:qwer1234")
        c.perform()
    except ImportError:
        pass

models.signals.post_save.connect(post_on_blip, sender=Review)

#noinspection PyUnusedLocal
def add_methods(sender, instance, signal, *args, **kwargs):
    """ Adds methods to access sized images (urls, paths)

    after the Photo model's __init__ function completes,
    this method calls "add_accessor_methods" on each instance.
    """
    if hasattr(instance, 'add_accessor_methods'):
        instance.add_accessor_methods()

#noinspection PyUnusedLocal
def send_review_notifications(sender, instance, created, *args, **kwargs):
    if not created:
        return
    flag_obj = Flag(content_object=instance.business, flag=flag_translator('ulubiony'))
    bookmarked_by = flag_obj.get_users()
    reviewed_by = instance.business.review_set.exclude(user=instance.user).values_list('user_id').distinct()

    notification.send(User.objects.filter(id__in = reviewed_by), "review_reviewer", {'review': instance,}, True, 8)
    notification.send(Friendship.objects.get_friends(instance.user), "review_friend", {'review': instance,}, True, 5)
    notification.send(User.objects.filter(email="dziki.dziurkacz@gmail.com"), "review_friend", {'review': instance,}, True, 5)
    notification.send(bookmarked_by, "review_fav_biz", {'review': instance,}, True, 4)
    if instance.business.owner:
        notification.send(User.objects.filter(id = instance.business.owner.id), "review_fav_biz", {'review': instance,}, True, 4)

#        flag_obj = Flag(content_object=self.user, flag=flag_translator('ulubiony'))
#        observerd_by = flag_obj.get_users()
#        notification.send(observerd_by, "review_fav_user", {'review': self,})

#self.get_flag_count('useful')

def update_flag_counter(sender, instance, *args, **kwargs):
    if not (instance.flag in [FLAG_DICTIONARY['useful'],
                      FLAG_DICTIONARY['cool'],
                      FLAG_DICTIONARY['interesting'],
                      FLAG_DICTIONARY['funny'],
                      FLAG_DICTIONARY['spam'],] or instance.content_type.id != 25):
        return
    instance.content_object.update_flag_counter(instance.flag)
    

post_init.connect(add_methods)
models.signals.post_save.connect(send_review_notifications, sender=Review)
models.signals.post_save.connect(update_flag_counter, sender=Flag)

class Recommend(models.Model):
    business = models.ForeignKey(Business, db_column="id", verbose_name=_("Venue"), related_name="business_recommendation_set")
    recommendation = models.ForeignKey(Business, db_column="business_id", verbose_name=_("Recommended by"), related_name="recommendation_set")
    position = models.IntegerField(verbose_name=_("Position"))
    checked = models.BooleanField(verbose_name=_("Checked"))

###Pawel - lists of businesses
class BusinessList(TimedModel):
    user = models.ForeignKey(User, blank=True, null=True)   #owner of the list -> user who created it
    name = models.CharField(_("List name"), max_length=100)    #name of the list
    flags = generic.GenericRelation(Flag)                       #flags to list (sharing system)
    businesses = models.ManyToManyField(Business, verbose_name=_("Business list"))
                                                                #many to many relation --> list of businesses in this BusinessList
    description = models.TextField('Opis')    #name of the list
    objects = geomodels.Manager()

    def empty(self):
        if self.businesses.all().count() == 0:
            return True
        return False

    def represent(self):
        reps = self.businesses.all()[:1]
        if reps.count() == 0:
            return None
        return reps[0]

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return "/bizlista/%i/" % self.id
    def get_add_item_url(self):
        return "/bizlista/dodajbiz/%i/" % self.id
    def get_del_item_url(self):
        return "/bizlista/usunbiz/%i/" % self.id

class Similarity(TimedModel):
    business1 = models.ForeignKey(Business, verbose_name="Lokal", related_name="business_similarities1")
    business2 = models.ForeignKey(Business, verbose_name="Lokal", related_name="business_similarities2")

    level = models.IntegerField(verbose_name=_("Level of similarity"))
    checked = models.BooleanField(verbose_name=_("Checked"))

class Moderation(TimedModel):
    business = models.ForeignKey(Business, verbose_name=_("Venue"))
    type = models.IntegerField(verbose_name=_("Error"))
    checked = models.BooleanField(verbose_name=_("Checked"))
