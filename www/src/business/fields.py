# -*- coding: utf-8 -*-
'''
Created on 2010-04-22

@author: kalinskia
'''

from django.forms import CharField, HiddenInput, ModelMultipleChoiceField

class ListField(CharField):
    '''
    Hidden field that contains a list of model pk's separated by separator keyword arg.
    '''
    widget = HiddenInput 
    def __init__(self,model, *args, **kwargs):
        self.separator = kwargs.pop('separator', ";")
        self.model = model
        super(ListField, self).__init__(*args, **kwargs)
        
    def to_python(self, value):
        if not value:
            return None
        value = value.strip().split(self.separator)
        value = [int(item) for item in value if item]        
        if isinstance(value, list):
            return self.model.objects.all().filter(pk__in=value)#[self.model.objects.get(pk=key) for key in value]
        else:
            return self.model.objects.get(pk=value)

class ModelMultipleChoiceTextField(ModelMultipleChoiceField):
    def __init__(self, *args, **kwargs):
        super(ModelMultipleChoiceTextField, self).__init__(*args, **kwargs)
    
    def clean(self, value):
        from django.utils import simplejson as json
        value = value.split(",")
        
        return super(ModelMultipleChoiceTextField, self).clean(value)
        
