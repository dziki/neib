# -*- coding: utf-8 -*- 
"""
Business and review forms. Currently nothing special.
"""
from django import forms
from django.forms import ModelForm, CharField, Textarea
from business.models import *
from date.widgets import SelectTimeWidget
from tagging.models import Tag
from business.widgets import CategoryWidget
from business.fields import ModelMultipleChoiceTextField
from django.forms.formsets import formset_factory
from django.contrib.auth import login, authenticate
from captcha.fields import ReCaptchaField

from django.utils.translation import ugettext_lazy as _

def password_generator(len=8):
    from random import choice
    chrs = "abcdefghijklmnopqrstuvwxyz"
    chrs = "".join([chrs,chrs.upper()])
    chrs = "".join([chrs,"1234567890"])
    #noinspection PyUnusedLocal
    return "".join([choice(chrs) for i in xrange(len)])

class ReviewForm(ModelForm):
    class Meta:
        model = Review
        fields = (
            'rating',
            'content',
        )
        widgets={
            'rating': forms.Select(attrs={'class':'rating'}),
        }
    tags = forms.CharField(label=_("Tags"), max_length=500, required=False, help_text=_("Help others finding your review and this venue. Write down tags that best suits this business like \"delicious cakes, cheap, etc.\""))
    facebook_publish = forms.BooleanField(label=_("Add to facebook"), required=False, initial=True, help_text=_("Post review on facebook automaticly"))
    email = forms.CharField(label=_("E-mail"), max_length=500, required=False)
    password = forms.CharField(label=_("Password"), max_length=500, required=False, widget=forms.PasswordInput())
    
    def save(self,*args,**kwargs):
        super(ReviewForm, self).save(*args,**kwargs)
        #Tag.objects.update_tags(self.instance.business, self.cleaned_data['tags'])
        
    def clean(self):
        self.needs_email_confirm = False
        if not self.instance.user:
            u = None
            if self.cleaned_data["password"]:
                u = authenticate(username=self.cleaned_data["email"], password=self.cleaned_data["password"])
            else:
                try:
                    if not (self.cleaned_data["email"]):
                        msg = (u"This field is required.")
                        self._errors["email"] = self.error_class([msg,])
                        del self.cleaned_data["email"]
#                        raise forms.ValidationError(_(u"Error!"))
                        return self.cleaned_data
                    u = User.objects.get(email = self.cleaned_data["email"])
                except User.DoesNotExist:
                    u = User()
                    u.email = self.cleaned_data["email"]
                    u.password = password_generator()
                    u.username = self.cleaned_data["email"]
                    u.save()
                    profile = u.get_profile()
                    profile.registered = False
                    profile.save()

                if u and u.first_name:
                    self.needs_email_confirm = True

            self.instance.user = u
        if not self.instance.user:
            raise forms.ValidationError(_(u"Error!"))

        return self.cleaned_data
                
class ReviewFormUnauthorized(ReviewForm):
    captcha = ReCaptchaField(help_text=_("Please rewrite these characters into the box to verify you are human."))
    pass

VERIFY_CHOICES =  (
    (1, 'OK'),
    (3, 'Odmowa'),
    (4, 'Brak kompetencji'),
    (5, 'Błędny telefon'),
    (6, 'Nie ma lokalu'),
    (7, ''),
)

class BusinessForm(ModelForm):
    categories = ModelMultipleChoiceTextField(queryset=Category.objects.all(),widget=CategoryWidget(), label=_("Categories"))
    #categories = ModelMultipleChoiceTextField(queryset=Category.objects.all(), widget=IPodMenu(Category.get_category_tree(),field_text=u"Dodaj kategorię", attrs={'id':'ipod-menu'}))
    class Meta:
        model = Business
        fields = (
            'name',
            'categories',
            'address',
            'zip',
            'city',
            'phone',
            'www',
            'email',
        )
        
    def save(self, *args, **kwargs):
        super(BusinessForm, self).save(*args, **kwargs)
        self.instance.fix_categories()
        

class BusinessOwnerForm(ModelForm):
    categories = ModelMultipleChoiceTextField(queryset=Category.objects.all(),widget=CategoryWidget(), label=_("Categories"))
    #categories = ModelMultipleChoiceTextField(queryset=Category.objects.all(), widget=IPodMenu(Category.get_category_tree(),field_text=u"Dodaj kategorię", attrs={'id':'ipod-menu'}))
    class Meta:
        model = Business
        fields = (
            'name',
            'categories',
            'address',
            'zip',
            'city',
            'phone',
            'www',
            'email',

            'specialities',
            'offer',
            'history',
            'owners',
        )
        

class HoursForm(ModelForm):
    class Meta:
        model = Hours
        fields = (
            'time_from',
            'time_to',
        )
    time_from = forms.TimeField(widget=SelectTimeWidget(None, None, 5, 60))
    time_to = forms.TimeField(widget=SelectTimeWidget(None, None, 5, 60))
    opened = forms.BooleanField(label=_("Open"), initial=1, required=False)

    def __init__(self, *args, **kwargs):
#        self.fields['time_from'].widget = SelectTimeWidget(None, None, 5, 60)
#        self.fields['time_to'].widget = SelectTimeWidget(None, None, 5, 60)
        super(HoursForm, self).__init__(*args, **kwargs)
        
class HoursFormSet():
    HMF = formset_factory(HoursForm, extra=7, max_num=7)
    _formset = None
    business = None
    
    def __init__(self, business, POST=None):
        self._formset = self.HMF(POST)
        self.business = business
        
    def is_valid(self):
        return self._formset.is_valid()
    
    def save(self):
        for i, form in enumerate(self._formset.forms):
            try:
                h = Hours.objects.get(business=self.business, day=i+1)
                if not form.cleaned_data['opened']:
                    h.delete()
                    continue
            except Hours.DoesNotExist:
                if not form.cleaned_data['opened']:
                    continue
                h = Hours(business = self.business, day = i+1)

            h.time_from = form.cleaned_data['time_from']
            h.time_to = form.cleaned_data['time_to']
            h.save()
            
    def get_formset(self):
        init = []
        for i in xrange(1,8):
            try:
                h = Hours.objects.get(business=self.business, day=i)
                init.insert(i-1,{"time_from": h.time_from,
                             "time_to":h.time_to,
                             "opened": True})
            except Hours.DoesNotExist:
                init.insert(i-1,{"opened" : False})
            self._formset = self.HMF(initial=init)
        return self._formset

    formset = property(get_formset)


class BusinessListForm(ModelForm):
    class Meta:
        model = BusinessList
        fields = (
                  'name', 
                  'description', 
        )

class CategoryTaggingForm(forms.Form):
    category = forms.ModelChoiceField(queryset=Category.objects.all())
    tags = forms.CharField(widget=forms.Textarea)
    fitting = forms.FloatField(initial=0.5)
    
#########################
## Admin forms
#########################

DEFAULT_FITTING = 0.8

class TagableModelAdminForm(ModelForm):
    #tags = CharField(max_length=100,widget=TextInput(attrs={'size':'100'}))
    tags = CharField(widget=Textarea(), help_text=u"""Tagi powinny być wpisywane każdy w kolejnej linii
                                                    bądź rozdzielone przecinkiem. Na końcu każdej linii znajduje się
                                                    liczba 'przystosowania' poprzedzona znakiem '#'. Przy wpisywaniu liczba
                                                    przystosowania może być pominięta i wtedy zostanie wypełniona wartością domyślną (aktualnie %.2f)""" \
                                                    % DEFAULT_FITTING)
    
    def __init__(self, *args, **kwargs):
        if 'instance' in kwargs:
            inst = kwargs['instance']
            tags = ["%s#%.2f"%(t.name, t.fitting_for_object(inst)) for t in Tag.objects.get_for_object(inst)]
            tags = "\n".join(tags)
            kwargs.update({'initial':{'tags':tags}})
        super(TagableModelAdminForm, self).__init__(*args, **kwargs)
    
    def clean_tags(self):
        tags = self.cleaned_data['tags']
        tags = tags.split("\n")
        res = []
        for t in tags:
            if "#" in t:
                fit = t.split("#")[-1]
                tag = t[:-(len(fit)+1)]
                for stag in tag.split(','):
                    res.append((stag.strip(),float(fit)))
            else:
                for stag in t.split(','):
                    res.append((stag.strip(),DEFAULT_FITTING))
        return res

class BusinessAdminForm(TagableModelAdminForm):
    class Meta:
        model = Business
        
class CategoryAdminForm(TagableModelAdminForm):
    class Meta:
        model = Category
