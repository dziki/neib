from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter(name='onoff')
@stringfilter
def onoff(value):
    if value != 'on' and value != 'off':
        return value
    v1 = value.replace('on', 'tak')
    return v1.replace('off', 'nie')