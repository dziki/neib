from django import forms
from django.utils.translation import ugettext_lazy as _

class DynamicForm(forms.Form):    
    """
    Dynamic form that allows user to change and then verify the data that was parsed
    """
    instance = None
    
    def __init__(self, FieldModel, FieldValueModel, **kwargs):
        """
        Set the fields in the form
        """
        super(DynamicForm, self).__init__()
        self.instance = kwargs['instance']

        self.FieldModel = FieldModel        
        self.FieldValueModel = FieldValueModel        
        self.field_objects = FieldModel.objects.get_fields(self.instance)
        
        self.create_fields()
        
    def create_fields(self):
        for f in self.field_objects:
            val, res = self.FieldValueModel.objects.get_or_create(field=f, obj=self.instance)
            if (f.type == "bool"):
                initial = False
                if val.value != 'off':
                    initial = True
                field = forms.BooleanField(label=_(f.name), initial=initial, required=False)
            elif (f.type == "select"):
                initial = val.value
                vals = f.values.split(',')
                choices = list()
                i = 0
                for val in vals:
                    i = i + 1
                    choices.append((_(val),_(val)))
                field = forms.ChoiceField(label=_(f.name), required=False, choices=tuple(choices), initial=initial)
            else:
                val = val.value
                if len(val) > 0:
                    val = _(val)
                field = forms.CharField(label=_(f.name), initial=val, required=False)
            self.fields['q' + f.id.__str__()] = field
            
                    
    def set_data(self, kwds):
        """
        Set the data to include in the form
        """
        self.FieldValueModel.objects.filter(obj=self.instance).delete()
        
        for f in self.field_objects:
            field_name = 'q' + f.id.__str__()
            
            self.data[f.id] = kwds.get(field_name, 'off')
            self.fields[field_name].value = kwds.get(field_name, 'off')
            setattr(self, field_name, kwds.get(field_name, 'off'))
            
        self.save()
        self.create_fields()
        
#    def validate(self):
#        self.full_clean()  
                  
#    def is_valid(self):
#        return true
            
    def save(self):
        obj = self.instance
        
        for f in self.field_objects:
            field_val = self.FieldValueModel()
            field_val.obj=obj
            field_val.field=f
            field_val.value = self.data[f.id]
            field_val.save()
