from django.db import models
from dynamicforms.managers import DynamicFieldManager

TYPE_CHOICES = (
    ('bool', 'Tak/Nie'),
    ('text', 'Pole opisowe'),
    ('select', 'Selekt'),
)

class DynamicField(models.Model):
    name = models.CharField(max_length=100)
    type = models.CharField(max_length=6, choices=TYPE_CHOICES)
    values = models.CharField(max_length=255, null=True, blank=True)
    objects = DynamicFieldManager()

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.__str__()
    
    class Meta:
        abstract = True
        
class DynamicFieldValue(models.Model):
    value = models.CharField(max_length=255, default='')

    class Meta:
        abstract = True
        
