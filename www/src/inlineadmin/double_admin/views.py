# Create your views here.

from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import render_to_response
import datetime
from business.models import *
from properties.models import Property
from django.template import RequestContext 
from business.forms import BusinessForm
from django import forms

from doodadmin.double_admin import *
from doodadmin.common_admin import *
from doodadmin.ai.navigation import *
from doodadmin.ai.similarity import *
from doodadmin.ai.suggestions import *
from doodadmin.ai.errors_detect import *
from doodadmin.common_admin.headers import *
from doodadmin.decorators import superuser_only

from django import forms

LIST_HEADERS_DOUBLE=(
    ('Id','id'),
    ('Business 1','business1'),
    ('Business 2','business2'),
    ('Stopien podobienstwa','level'),
    ('Data Wykrycia','date'),
)

@superuser_only
def double_list(request):
    sort_headers=SortHeaders(request,LIST_HEADERS_DOUBLE)
    double_list = Similarity.objects.filter(checked__exact=False).order_by(sort_headers.get_order_by())
    return render_to_response('doodadmin/double/double_list.html',
                              context_instance=RequestContext(request,{
                                                'double_list':double_list,
                                                'headers': list(sort_headers.headers())}))
    
@superuser_only
def double_merge(request):
    id = int(request.GET.get('pair_id',1))
    return render_to_response('doodadmin/double/double_merge.html',
                                 context_instance=RequestContext(request,{}))