# -*- coding: utf-8 -*- 
"""
Forms and validation code for user registration.

"""


from django import forms
from django.forms import ModelForm, Form
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from business.models import Business

class BusinessBulkAddForm(Form):
    csv = forms.FileField()