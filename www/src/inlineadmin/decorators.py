from django.contrib.auth.decorators import user_passes_test

def permission_required(perm):
    """Replacement for django.contrib.auth.decorators.permission_required that
    returns 403 Forbidden if the user is already logged in.
    """

    return user_passes_test(lambda u: u.profile.privileges >= perm)