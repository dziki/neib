from django.contrib.auth.models import User
from django.template.context import RequestContext
from forum.models import Post
from inlineadmin.decorators import permission_required
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render_to_response
from ownership.models import Request
from business.models import Review
from inlineadmin.forms import BusinessBulkAddForm
from business.models import Business, Category
from ai.navigation import *
from properties.models import Property

@login_required()
@permission_required(2)
def privileges_set(request, user_id, privileges):
    user = User.objects.get(id=user_id)

    privileges = int(privileges)

    if privileges > 1:
        privileges = 1
    if privileges < 0:
        privileges = 0

    user.profile.privileges = privileges
    user.profile.supervisor = request.user
    user.profile.save()
    return redirect('profile', username=user.id)

@login_required()
@permission_required(2)
def ownership_enable(request):
    requests = Request.objects.all().filter(accepted=False)
    for item in requests:
        item.accepted=True
        item.save()
    return redirect('home')

@login_required()
@permission_required(2)
def upload_venues(request):
    form = UploadBusinessesForm()
    return render_to_response('inlineadmin/ownership_requests.html', {"form": form},
        context_instance=RequestContext(request))

@login_required()
@permission_required(2)
def ownership_requests(request):
    requests = Request.objects.all().filter(accepted=False)

    return render_to_response('inlineadmin/ownership_requests.html', {"requests": requests},
        context_instance=RequestContext(request))

@login_required()
@permission_required(2)
def admin_main(request):
    last_double_count =  Property.objects.filter(key__exact="last_double_count")
    last_activity = Property.objects.filter(key__exact="last_activity")
    pending_doubles=Similarity.objects.filter(checked__exact=False).count()
    pending_verify=(Business.objects.filter(verified__exact=0)).count()
    links = create_initial_links()
    pending_moderate=(Moderation.objects.filter(checked__exact=False).count())
    first_not_verified=(Business.objects.order_by('date').filter(verified__exact=0).reverse()[:1])
    if len(first_not_verified)>0:
        first_not_verified=first_not_verified[0].id
    return render_to_response('inlineadmin/base.html',
                              context_instance=RequestContext(request,
                              {'last_activity':last_activity,
                               'pending_doubles':pending_doubles,
                               'pending_verify':pending_verify,
                               'pending_moderate':pending_moderate,
                               'links':links,
                               'last_double_count':last_double_count,})
                               )

@login_required()
@permission_required(2)
def ownership_request_accept(request, request_id):
    r = Request.objects.all().get(id=request_id)
    r.accepted = True
    r.save()

    return ownership_requests(request)

@login_required()
@permission_required(2)
def ownership_request_decline(request, request_id):
    r = Request.objects.all().get(id=request_id)
    r.delete()

    return ownership_requests(request)

@login_required()
@permission_required(1)
def hide_review(request, review_id):
    rev = Review.objects.get(id=review_id)
    rev.active = 0
    rev.save()
    return redirect('business_detail', business_slug=rev.business.slug)

@login_required()
@permission_required(1)
def hide_post(request, post_id):
    post = Post.objects.get(id=post_id)
    post.active = 0
    post.save()
    return redirect(post.thread)

def business_loader(file_handle):
    lines = file_handle.readlines()
#    file_handle.close()

    title = lines[0]
    keys = title.split(';')
    
    businesses = list()

    for line in lines[1:]:
        line = line.decode('utf8')
        vals = line.split(';')
        item = {}
        for i in range(0, len(keys)-1):
            item[keys[i]] = vals[i]

        buss = dict()
        buss["name"] = item['name']
        buss["address"] = item['address']
        buss["zip"] = item.get('zip','')
        buss["city"] = item['city']
        buss["phone"] = item.get('phone','')
        buss["www"] = item.get('www','')
        buss["comment"] = item.get('comment','')
        buss["categories"] = item.get('categories','')

        cats = Category.objects.filter(slug__in = buss["categories"].split(','))
        buss["categories"] = cats
        #cat = Category.objects.get(id=102)
        #buss.save()
        #buss.categories.add(cat)
        businesses.append(buss)
    return businesses

@login_required()
@permission_required(1)
def business_bulk_add(request):
    form = BusinessBulkAddForm(request.POST,request.FILES)
    errors = []
    
    try:
        if form.is_valid():
            request.session["businesses_bulk"] = business_loader(form.cleaned_data["csv"])
            return redirect('business_bulk_verify')
        else:
            form = BusinessBulkAddForm()
    except KeyError, e:
        errors.append("Missing field '" + e.message + "'")
    
    
    return render_to_response('inlineadmin/business_bulk_add.html', {"form": form, "errors": errors},
        context_instance=RequestContext(request))

@login_required()
@permission_required(1)
def business_bulk_verify(request):
    return render_to_response('inlineadmin/business_bulk_verify.html', {"businesses": request.session["businesses_bulk"][:100]},
        context_instance=RequestContext(request))

@login_required()
@permission_required(1)
def business_bulk_save(request):
    businesses = list()
    for business in request.session["businesses_bulk"]:
        buss = Business()
        buss.name = business["name"]
        buss.address = business["address"]
        buss.zip = business["zip"]
        buss.city = business["city"]
        buss.phone = business["phone"]
        buss.www = business["www"]
#        cats = Category.objects.filter(slug__in = business["categories"].split(','))
        buss.save()
        for cat in business["categories"]:
            buss.categories.add(cat)
        businesses.append(buss)
        
    request.session["businesses_bulk"] = None
    return render_to_response('inlineadmin/business_bulk_save.html', {"businesses": businesses[:100]},
        context_instance=RequestContext(request))
