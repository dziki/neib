# Create your views here.
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import render_to_response
import datetime
from business.models import *
from properties.models import Property
from django.template import RequestContext 
from business.forms import BusinessForm
from django import forms

from doodadmin.double_admin import *
from doodadmin.common_admin import *
from doodadmin.ai.navigation import *
from doodadmin.ai.similarity import *
from doodadmin.ai.suggestions import *
from doodadmin.ai.errors_detect import *
from doodadmin.common_admin.headers import *
from doodadmin.decorators import superuser_only

LIST_HEADERS_BUSINESS=(
    ('Id', 'id'),
    ('Nazwa', 'name'),
    ('Adres', 'address'),
    ('Telefon', 'phone'),
    ('Weryfikacja','verified'),
    ('Data Modyfikacji','date'),
)


@superuser_only
def business_list(request):
    sort_headers=SortHeaders(request,LIST_HEADERS_BUSINESS)
    mode = request.GET.get('mode',request.session.get('mode','all'))
    if mode not in ["all","verification","moderation"]:
        mode="all"    
    request.session['mode']=mode
    businesses=list()
    if mode=="verification":
        businesses = Business.objects.filter(verified__exact=0).order_by(sort_headers.get_order_by())
    if mode=="moderation":
        ids = Moderation.objects.filter(checked__exact=False)
        businesses = Business.objects.filter(id__in=ids).order_by(sort_headers.get_order_by())
    if mode=="all":
        businesses = Business.objects.all().order_by(sort_headers.get_order_by())
        
    return render_to_response('doodadmin/business/business_list.html',
                               context_instance=RequestContext(request,
                              {'businesses':businesses,
                               'verify_choices':VERIFY_CHOICES,
                               'mode':mode,
                               'headers': list(sort_headers.headers())}))

class VerifyForm(forms.Form):
    mode=forms.ChoiceField(choices=VERIFY_CHOICES)
    


@superuser_only
def business_details(request, business_id):
    id = business_id
    if request.session['mode']:
        mode=request.session['mode']
    else:
        mode="all"
    verifyForm=VerifyForm()
    if request.method=='POST':
        form = BusinessForm(request.POST)
        if form.is_valid():
            pass
    else:
        
        business = None
        if id != None:
            business = Business.objects.get(id__exact=id)
            navigation = get_navigation(mode,business.id)
            verifiedFlag=business.verified>0
            (l,verifiedStatus) = VERIFY_CHOICES[business.verified]
            suggestions = get_suggestions_from_business(business)
            errors = get_errors_from_business(business)
            reviews = Review.objects.all().filter(business__exact=id)
            similar = getSimilarForBusiness(id)
            form = BusinessForm()
            form.fields['name'].initial = business.name
            form.fields['address'].initial = business.address
            form.fields['zip'].initial = business.zip
            form.fields['phone'].initial = business.phone
            form.fields['www'].initial = business.www
            form.fields['email'].initial = business.email
#            form.fields['comment'].initial = business.comment
            
            
    return render_to_response('doodadmin/business/business_details.html',
                                 context_instance=RequestContext(request, {'business':business, 
                                                        'reviews':reviews,
                                                        'similar':similar,
                                                        'form':form,
                                                        'verifyForm':verifyForm,
                                                        'suggestions':suggestions,
                                                        'errors':errors,
                                                        'verifiedFlag':verifiedFlag,
                                                        'verifiedStatus':verifiedStatus,
                                                        'navigation':navigation}))
    