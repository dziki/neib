'''
Created on 2010-03-09
'''
from django import template

def table_header(context, headers):
    return {
        'headers': headers,
    }

register = template.Library()
register.inclusion_tag('doodadmin/tags/table_header.html', takes_context=True)(table_header)

