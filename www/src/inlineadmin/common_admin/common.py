'''
Created on 2010-03-09

@author: bediej
'''

def numeric_compare(t1,t2):
    (l1,p1)=t1
    (l2,p2)=t2
    if p1==p2:
        return 0
    else:
        if p1>p2:
            return 1
        else:
            return -1
        