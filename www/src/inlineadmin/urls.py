import django
from django.conf.urls.defaults import patterns

from django.contrib import admin

from inlineadmin.views import *

admin.autodiscover()

urlpatterns = patterns('',
    ((r'^$'), admin_main, {}, 'admin_main'),

    ((r'^privileges/set/(?P<user_id>[-\w]+)/(?P<privileges>[-\d]+)/$'), privileges_set, {}, 'privileges_set'),
    ((r'^review/hide/(?P<review_id>[-\d]+)/$'), hide_review, {}, 'inlineadmin_hide_review'),
    ((r'^post/hide/(?P<post_id>[-\d]+)/$'), hide_post, {}, 'inlineadmin_hide_post'),

    ((r'^business/bulk/add/$'), business_bulk_add, {}, 'business_bulk_add'),
    ((r'^business/bulk/verify/$'), business_bulk_verify, {}, 'business_bulk_verify'),
    ((r'^business/bulk/save/$'), business_bulk_save, {}, 'business_bulk_save'),
    
    ((r'^owner/enable/all/$'), ownership_enable, {}, 'ownership_enable'),
    ((r'^owner/list/'), ownership_requests, {}, 'ownership_requests'),
    ((r'^owner/disable/(?P<request_id>[-\d]+)/'), ownership_request_accept, {}, 'ownership_request_accept'),
    ((r'^owner/remove/(?P<request_id>[-\d]+)/'), ownership_request_decline, {}, 'ownership_request_decline'),

)