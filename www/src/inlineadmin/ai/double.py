'''
Created on 2010-02-15
'''
from business.models import Business

MINIMAL_FOR_SIMILAR = 20

NAME_IMPACT = 40 
ADDRESS_IMPACT = 40 
PHONE_IMPACT = 20  


def getSimilarityLevel(business1, business2):
    similarity_points = 0
    (adres1, adres2) = sortByLen(business1.address.lower().strip(), business2.address.lower().strip())
    (name1, name2) = sortByLen(business1.name.lower().strip(), business2.name.lower().strip())
    (phone1, phone2) = sortByLen(business1.phone, business2.phone)
    similarity_points += NAME_IMPACT * getStringSimilarityByWords(name1, name2) 
    similarity_points += ADDRESS_IMPACT * getStringSimilarityByWords(adres1, adres2)
    similarity_points += PHONE_IMPACT * getPhoneSimilarity(phone1, phone2)
    return similarity_points


def getStringSimilarityByWords(string1, string2):
    words1 = string1.split(' ', 100)
    words2 = string2.split(' ', 100)
    (words1, words2) = sortByLen(words1, words2)
    if len(words2) == 0:
        return 0
    numInter = 0
    for word in words2:
        if  word in words1:
            numInter += 1
    return numInter / len(words2)

def getStringSimilarity(string1, string2):
    numChars = len(string2)
    for i in range(len(string2)):
        for j in range(len(string2) - i):
            curStr = string2[i:len(string2) - j]
            if string1.find(curStr) != -1:
                return len(curStr) / numChars
    return 0

def getPhoneSimilarity(phone1, phone2):
    if not (phone1 and phone2):
        return 0
    (st1,st2) = (phone1[-7:],phone2[-7:])
    (m1,m2)=(phone1[-9:],phone2[-9:])
    if len(phone2)>=8:
        if st1==st2:
            return 1
        else:
            return 0
    else:
        if m1==m2:
            return 1
        else:
            return 0

def sortByLen(s1, s2):
    if not (s1 and s2):
        return (s1, s2)
    if len(s1) < len(s2):
        return (s2, s1)
    else:
        return (s1, s2)
    
