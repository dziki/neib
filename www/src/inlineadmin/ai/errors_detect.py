from business.models import Business, Review, Category

def get_errors_from_business(business):
    errors={}
    phonee=detect_phone_error(business.phone)
    emaile=detect_email_error(business.email)
    if phonee !=None:
        errors['phone']=phonee
    return errors

def detect_email_error(email):
    if email==None or email=="":
        return None
    if email.find("@") == -1:
        return "NIE ZAWIERA @"
    return None

def detect_phone_error(phone):
    if phone==None or phone == "":
        return None
    if len(phone)<7:
        return "ZA KROTKI"
    return None
    