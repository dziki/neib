from business.models import Business, Review, Category
from phone_filter import * 

SUGGEST_CLEAR="**WYCZYSC**"

def get_suggestions_from_business(business):
    suggestions={}
    address = get_suggested_address(business.address)
    phone   = get_suggested_phone(business.phone)
    zip     = get_suggested_zip(business.zip)
    email   = get_suggested_email(business.email)
    www     = get_suggested_www(business.www)
    if address != None:
        suggestions['address']=address
    if phone != None:
        suggestions['phone']=phone
    if zip != None:
        suggestions['zip']=zip
    if email != None:
        suggestions['email']=email
    if www != None:
        suggestions['www']=www
    return suggestions

def get_suggested_address(address):
    if address==None or address=="":
        return None
    new = address.strip()
    prefix = (new[0:2]).lower()
    if prefix!= "ul" and prefix!="al" and prefix!="pl":
        new = "ul" + address
    else:
        if new[0:2]=="UL" or new[0:2]=="Ul":
            new="ul"+new[2:]
        if new[0:2]=="AL" or new[0:2]=="Al":
            new="al"+new[2:]
        if new[0:2]=="PL" or new[0:2]=="Pl":
            new="pl"+new[2:]
    if new[2]!=".":
        new = new[0:2]+". "+new[2:].strip()
    if new[3]!=" ":
        new = new[0:3]+" "+new[3:].strip()
    if new!=address:
        return new
    else:
        return None
    

def get_suggested_phone(phone):
    if phone==None or phone=="":
        return None
    new = strip_digits(phone)
    if len(new)==0:
        return SUGGEST_CLEAR
    if new[0]=="0":
        new=new[1:]
    if len(new)<7:
        return SUGGEST_CLEAR      
    if new!=phone:
        return new
    else:
        return None

def get_suggested_zip(zip):
    if zip==None or zip=="":
        return None
    new = strip_digits(zip)      
    if len(new)<5:
        return None
    new = new[0:2]+"-"+new[2:5]
    if new!=zip:
        return new
    else:
        return None
    

def get_suggested_www(www):
    if www==None or www=="":
        return None
    new = www
    new = new.strip()
    new = new.lower()
    if new[:7]!="http://":
        new="http://"+new
    if new[len(new)-1]=="/":
        new=new[:-1]
    if new!=www:
        return new
    else:
        return None    

def get_suggested_email(email):
    if email==None or email=="":
        return None
    new=email
    new.strip()
    new.lower()
    if email.find('@')==-1:
        new = SUGGEST_CLEAR
    if new!=email:
        return new
    else:
        return None
    
def strip_digits(str):
    new=""
    for i in range(0, len(str)):
        if str[i].isdigit():
            new+=str[i]  
    return new
    