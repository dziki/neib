'''
Created on 2010-03-09

@author: bediej
'''

from doodadmin.common_admin.common import *
from business.models import *
from doodadmin.ai.double import *

MINIMAL_FOR_SIMILAR=30

def getSimilarForBusiness(id):
    relBusiness = Business.objects.get(id__exact=id);
    businesses = Business.objects.exclude(id__exact=id);
    similar = list()
    for business in businesses:
        slevel = getSimilarityLevel(relBusiness, business)
        if slevel > MINIMAL_FOR_SIMILAR:
            similar.append((business,slevel))
    similar.sort(numeric_compare,reverse=True)
    return similar