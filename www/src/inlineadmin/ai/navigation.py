'''
Created on 2010-03-09

@author: bediej
'''
from business.models import *

def get_navigation(mode,id=None):
    next=None
    first=None
    if mode=="all":
        businesses = Business.objects.order_by('date').values('id')
    if mode=="verification":
        businesses = Business.objects.order_by('date').filter(verified__exact=0).values('id')
    if mode=="moderation":
        businesses = Moderation.objects.order_by('date').filter(checked__exact=False).values('business_id')
    if mode=="double":
        businesses = Similarity.objects.order_by('date').values('id')#tu id pary a nie business
    id_list = [i['id'] for i in businesses]
    if len(id_list)>0:
        first=id_list[0]
        try:
            nextind=id_list.index(id)+1
            if nextind + 1 > len(id_list):
               nextind=nextind-1
               next=id_list[nextind]
        except ValueError:
            next=first
    if next==None:
        next=first  
    return get_navigation_dictionary(next,first)
    
def get_navigation_dictionary(next, first):
    return {'next':next,'first':first}

def create_initial_links():
    double=get_navigation('double').get('first',1)
    verification=get_navigation('verification').get('first',1)
    moderation=get_navigation('moderation').get('first',1)
    return {'verification':verification,
            'double':double,
            'moderation':moderation}