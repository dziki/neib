'''
Created on 2010-02-23
'''

INVALID_TUPLE = ("",False)

def filterPhone(phone_number):
    phone_number = phone_number.strip()
    numberscollon = str.split(phone_number,";")
    numbers=list()
    for str in numberscollon:
        numbers.append()
    return True
        
def detectOneNumber(phone_number):
    if len(phone_number) < 7:
        return INVALID_TUPLE
    is_mobile = detectMobile(phone_number)
    is_landline = detectLandline(phone_number)
    
    if (is_landline and is_mobile):
        return INVALID_TUPLE


def detectMobile(phone_number):
    if len(phone_number) < 9:
        return False
    str = phone_number[-9:]
    if str[0] in (5,6,7,8,9):
        pass
    return True

def detectLandline(phone_number):
    if phone_number[:1] == "48" or phone_number[:1] == "22":
        pass
    return True


def constructTuple(new_number):
    return (new_number,True)