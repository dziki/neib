# -*- coding: utf-8 -*-
'''
The main file containing the searching engine related objects 
and functions
'''

from django.db import models
import re


def word_match_value(word, context):
    context= context.strip()
    ret = int(bool(re.match("^.*%s.*$" % word,context)))
    ret += int(bool(re.match("^"+word+".*$",context)))
    ret += int(bool(re.match("^.*"+word+"$",context)))
    return ret

class BusinessSearchEngine(models.Manager):
    def name(self, name):
        bizs = self.filter(name__icontains=name)
        for biz in bizs:
            biz.name_match_level = word_match_value(name, biz.name)
        
        return bizs 