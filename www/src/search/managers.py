# -*- coding: utf-8 -*-

from django.contrib.gis.db import models as geomodels
from django.db.models import Avg, Count
from django.db import connection
from tagging.models import Tag
from django.db.models import Q

#noinspection PyUnusedLocal
def _search_by_tag_sql(phrase, **kwargs):
    cursor = connection.cursor()

#    cursor.execute('''
#        SELECT  biz_id as id,
#                sum(fitting) as fitting
#            FROM business_fitting
#            WHERE tag_name LIKE "%s" OR biz_name LIKE "%s" group by biz_id order by fitting DESC
#        ''' % ('%%' + phrase + '%%','%%' + phrase + '%%',))
#    row = cursor.fetchall()

    cursor.execute('''
        SELECT id FROM tagging_tag
            WHERE name LIKE "%s"
        ''' % ('%%' + phrase + '%%',))
    row = cursor.fetchall()
    str = [r[0] for r in row].__str__()
    
    if not len(row):
        str = "[-1]"
    str = str.replace("L","")
    str = str.replace("[","(")
    str = str.replace("]",")")

    cursor.execute('''
        SELECT object_id, content_type_id FROM tagging_taggeditem
            WHERE tag_id IN %s
        ''' % str)
    tagitems = cursor.fetchall()
    
#    cursor.execute('''
#        SELECT id, content_type_id FROM tagging_taggeditem
#            WHERE tag_id IN %s AND content_type_id = 22
#        ''' % str)
#    categories = cursor.fetchall()

    categories = [r[0] for r in tagitems if r[1] == 21]
    businesses = [r[0] for r in tagitems if r[1] == 22]
    return businesses, categories

# CREATE OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tagging_tagitems_view` AS select `t`.`id` AS `tag_id`,`i`.`id` AS `item_id`,`t`.`name` AS `name`,`i`.`content_type_id` AS `content_type_id`,`i`.`object_id` AS `object_id`,`i`.`weight` AS `weight`,`i`.`fitting` AS `fitting` from (`tagging_tag` `t` join `tagging_taggeditem` `i` on((`t`.`id` = `i`.`tag_id`)))
# CREATE OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dood`.`business_category_view` AS select `b`.`id` AS `biz_id`,`b`.`date` AS `biz_date`,`b`.`last_edit` AS `biz_last_edit`,`b`.`user_id` AS `biz_user_id`,`b`.`owner_id` AS `biz_owner_id`,`b`.`slug` AS `biz_slug`,`b`.`position` AS `biz_position`,`b`.`name` AS `biz_name`,`b`.`address` AS `biz_address`,`b`.`city` AS `biz_city`,`b`.`phone` AS `biz_phone`,`b`.`state` AS `biz_state`,`b`.`zip` AS `biz_zip`,`b`.`www` AS `biz_www`,`b`.`email` AS `biz_email`,`b`.`opened` AS `biz_opened`,`b`.`active` AS `biz_active`,`b`.`verified` AS `biz_verified`,`b`.`comment` AS `biz_comment`,`b`.`gallery_id` AS `biz_galery_id`,`b`.`rating` AS `biz_rating`,`c`.`id` AS `cat_id`,`c`.`parent_id` AS `cat_parent_id`,`c`.`name` AS `cat_name`,`c`.`main_form` AS `cat_main_form`,`c`.`date` AS `cat_date` from ((`business_business` `b` join `business_business_categories` `bc` on((`b`.`id` = `bc`.`business_id`))) join `business_category` `c` on((`bc`.`category_id` = `c`.`id`)))
# CREATE OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `business_fitting_view` AS (select `biz`.`id` AS `biz_id`, `biz`.`name` AS `biz_name`, `tag`.`name` AS `tag_name`,`item`.`fitting` AS `fitting` from ((`tagging_tag` `tag` join `tagging_taggeditem` `item` on((`tag`.`id` = `item`.`tag_id`))) join `business_business` `biz` on((`item`.`object_id` = `biz`.`id`))) where ((`item`.`content_type_id` = (select `django_content_type`.`id` AS `id` from `django_content_type` where ((`django_content_type`.`app_label` = 'business') and (`django_content_type`.`model` = 'business')))) and (`item`.`fitting` > 0))) union (select `bc`.`biz_id` AS `biz_id`,`t`.`name` AS `tag_name`,`t`.`fitting` AS `fitting` from (`tagging_tagitems_view` `t` join `business_category_view` `bc` on((`t`.`object_id` = `bc`.`cat_id`))) where ((`t`.`content_type_id` = (select `django_content_type`.`id` AS `id` from `django_content_type` where ((`django_content_type`.`app_label` = 'business') and (`django_content_type`.`model` = 'category')))) and (`t`.`fitting` > 0))) order by `biz_id`

# DROP TABLE `business_fitting`
# CREATE TABLE `business_fitting` SELECT * FROM `business_fitting_view` WHERE 1

class SearchQuerySet(geomodels.query.GeoQuerySet):
    #noinspection PyUnusedLocal
    def filter_by_tag(self, phrase, **kwargs):
        bussinesses, categories = _search_by_tag_sql(phrase)
#        if order_by == "reviews":
#            bus_query = bus_query.annotate(reviews=Sum('review')).order_by('-reviews')
        return self.filter(Q(id__in=bussinesses) | Q(categories__in=categories) | Q(name__icontains = phrase)).distinct()

    def order_by_annotation(self, order_by):
        if order_by == 'reviews':
            return self.annotate(reviews=Count('review__rating')).order_by('-'+order_by)
        if order_by == 'rank':
            return self.annotate(rank=Avg('review__rating')).order_by('-'+order_by)
        
        return self.annotate(reviews=Count('review__rating')).order_by('-reviews')
        
    def filter_active(self):
        return self.filter(active=True)

class SearchManager(geomodels.GeoManager):
    ''' Manager for Business model responsible for searching '''
    def get_query_set(self):
         return SearchQuerySet(self.model)
    
#    def full_index_search(self, phrase):
#        return self.model.objects.extra(where=['''id IN (SELECT business_id FROM business_review WHERE MATCH (content) AGAINST (%s IN BOOLEAN MODE))'''], params=[phrase])