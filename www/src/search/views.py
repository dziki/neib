from search.models import Declension

def dict_loader(request):
    filename_in = 'csv/odm.csv'

    file_handle = open(filename_in, 'r')
    lines = file_handle.readlines()
    file_handle.close()

    for line in lines:
        words = line.split(',',1000)
        main = words[0].decode('utf-8')
        for word in words:
            word = word.decode('utf-8').strip()
            dec = Declension()
            dec.word = main
            dec.declension = word
            try:
                dec.save()
            except:
                raise main + ' - ' + word
    
    return None