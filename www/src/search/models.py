# -*- coding: utf-8 -*- 
from django.db import models
from django.db.models.aggregates import Count
from business.models import Business, Category
from community.models import Location
from string import lower
from location.models import get_poly
from django.utils.encoding import smart_unicode
import re

class DeclensionManager(models.Manager):
    def get_declensions(self, words):
        words_dec = list()
        for word in words:
            word = lower(word)
            words_tmp = DeclensionProxy.objects.filter(declension = word)
            if words_tmp.count() == 0:
                words_tmp = super(DeclensionManager, self).filter(declension = word)
                for w in words_tmp:
                    nw = DeclensionProxy()
                    nw.word = w.word
                    nw.declension = w.declension
                    nw.save()
                if len(words_tmp) == 0:
                    nw = DeclensionProxy()
                    nw.word = word
                    nw.declension = word
                    nw.save()
                words_tmp = DeclensionProxy.objects.filter(declension = word)

            for w in words_tmp:
                if not w.word in words_dec:
                    words_dec.append(w.word)
            
        return words_dec

class Declension(models.Model):
    word = models.CharField('Forma podstawowa', max_length=40, primary_key = True)
    declension = models.CharField('Odmiana', max_length=40, primary_key = True)
    
    objects = DeclensionManager()
    
class DeclensionProxy(models.Model):
    word = models.CharField('Forma podstawowa', max_length=40)
    declension = models.CharField('Odmiana', max_length=40, primary_key = True)
    
class Tag(models.Model):
    tag = models.CharField(max_length=100)
    num_occur = models.IntegerField()
    from_dict = models.SmallIntegerField()
    decl = models.CharField(max_length=100)
    
class SearchEngine():
    businesses = None
    location = Location()
    order_by = None

    #noinspection PyUnusedLocal
    def __init__(self, *args, **kwargs):
        self.businesses = Business.search.all().filter_active()
        
    def order_by(self, order_by):
        if order_by == 'promotion':
            self.businesses = self.businesses.order_by_annotation('rank')
            self.businesses = self.businesses.annotate(null_promotion=Count('promotion')).order_by('-null_promotion','rank')
        else:
            self.businesses = self.businesses.order_by_annotation(order_by)
        return self.businesses
        
    def filter_polygon(self, top, right, bottom, left):
        self.businesses = self.businesses.filter(position__within=get_poly(top,
                                right, 
                                bottom,
                                left))
        return self.businesses
    
    def filter_category(self, category):
        categories = category.split(',')
        self.businesses = self.businesses.filter(categories__slug__in = categories)
        return self.businesses
    
    def filter_where(self, where):
        radius = 0.025
        where = smart_unicode(where)
        
        # check if latitude,longitude is given
        exp = "[0-9]+\.[0-9]+,[0-9]+\.[0-9]+"

        if re.match(exp, where):
            x,sep,y = where.partition(',')
            x = float(x)
            y = float(y)
            self.location.position.x = x
            self.location.position.y = y
            self.businesses = self.businesses.filter(position__within=get_poly(y + radius,
                                x + radius, 
                                y - radius,
                                x - radius))
            self.location.save()
            return

        self.location.get_from_string(where)
        if self.location.box:
            self.businesses = self.businesses.filter(position__within=self.location.box)
        else:
            x = self.location.position.x
            y = self.location.position.y
            self.businesses = self.businesses.filter(position__within=get_poly(y + radius,
                            x + radius, 
                            y - radius,
                            x - radius))
        self.location.save()
        return
    
    def filter_what(self, phraze):
        bus = self.businesses.filter_by_tag(phraze)
        
#        if (not bus.exists()):
#            bus = self.businesses.filter(name__icontains = phraze)
            
        self.businesses = bus

        return

    def filter_name(self, name):
        self.businesses =  self.businesses.filter(name__icontains=name)


class SearchHint(object):
    def __init__(self, phrase, max_results=10):
        self.phrase = phrase
        self.max_results = max_results
        
    def _generate(self):
#        businesses = Business.objects.filter(name__icontains=self.phrase)#[0:self.max_results]
#        declensions = DeclensionProxy.objects.filter(word__icontains=self.phrase)#[0:self.max_results]
        categories = Category.objects.filter(name__istartswith=self.phrase)
#        events = Event.objects.filter(name__icontains=self.phrase)
        
        result = []
 #       result += [ d.word for d in declensions]
 #       result += [ b.name for b in businesses]
        result += [ c.name for c in categories] 
 #       result += [ e.name for e in events]

        result = list(set(result))[0:self.max_results]
        result.sort()
        return result
    
    hints = property(_generate)