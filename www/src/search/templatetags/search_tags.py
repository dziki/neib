"""Splits query results list into multiple sublists for template display."""

from django.template import Library
from django.core.urlresolvers import reverse


register = Library()

@register.simple_tag
def search_nearby(co,gdzie):
    link = reverse('business_search')
    link += '?'
    kwargs = {'co':co,'gdzie':gdzie}
    for k in kwargs.keys():
        link += k + '=' + kwargs[k] + '&'
    return link