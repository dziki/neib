from django.contrib import admin
from badges.models import Badge

class BadgeAdmin(admin.ModelAdmin):
    list_display = ('user','badge')
    
admin.site.register(Badge, BadgeAdmin)
