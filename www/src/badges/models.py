# -*- coding: utf-8 -*- 
"""
Models for bussinesses

"""
from django.db import models

from date.models import TimedModel
from django.contrib.auth.models import User
from notification import models as notification
from community.models import Profile

from django.utils.translation import ugettext_lazy as _

BADGES = dict()

class Badge(TimedModel):
    user = models.ForeignKey(User, blank=True, null=True, verbose_name=_("User"))
    badge = models.IntegerField()
    
    def save(self, *args, **kwargs):
        super(Badge, self).save(*args, **kwargs)

        if self.user.id == 2:
            notification.send([self.user,], "badge_new", {'badge': self,}, True, 8)


class AbstractBadge():
    id = 0
    type = "odznaczenie"
    border_color = "#cccccc"
    bg_color = "#eeeeee"
    font_color = "#555555"

    def __unicode__(self):
        desc = self.type
        desc = desc.replace('*','<strong>',1)
        desc = desc.replace('*','</strong>',1)
        return desc

    @classmethod
    def get_users(cls):
        return Profile.objects.all().filter(user__badge__badge=cls.id).select_related("gallery__representative", "user")

class ReviewerBadge(AbstractBadge):
    id = 1
    type = _("Critic")
    border_color = "#5D8A51"
    bg_color = "#85EB6A"
    font_color = "#555555"
    description = _("for first 10 reviews")
    
    @classmethod
    def check_for_user(cls, user):
        if user.review_set.all().count() >= 10:
            return True
        return False
    pass

class JapanTuristBadge(AbstractBadge):
    id = 2
    type = _("Japan tourist")
    border_color = "#cccccc"
    bg_color = "#eeeeee"
    font_color = "#555555"
    description = _("for first 20 photos")
    
    @classmethod
    def check_for_user(cls, user):
        if user.photo_set.all().count() >= 20:
            return True
        return False
    pass

class GastroDoodBadge(AbstractBadge):
    id = 3
    type = _("Gastromaster")
    border_color = "#A19C68"
    bg_color = "#F7AF87"
    font_color = "#555555"
    description = _("for first 10 restaurant reviews")
    
    @classmethod
    def check_for_user(cls, user):
        if user.review_set.filter(business__categories__slug="restauracje").count() >= 10:
            return True
        return False
    pass

class NewbieDoodBadge(AbstractBadge):
    id = 4
    type = _("Debiutant")
    border_color = "#5E6F85"
    bg_color = "#82A4D1"
    font_color = "#2E3F55"
    description = _("for first review")

    @classmethod
    def check_for_user(cls, user):
        if user.review_set.filter(business__categories__slug="restauracje").count() >= 1:
            return True
        return False
    pass

class EventerBadge(AbstractBadge):
    id = 5
    type = _("Dance leader")
    border_color = "#5E6F85"
    bg_color = "#82A4D1"
    font_color = "#2E3F55"
    description = _("for first 20 events added")

    @classmethod
    def check_for_user(cls, user):
        if user.dood_event_set.all().count() >= 20:
            return True
        return False
    pass

class ExplorerBadge(AbstractBadge):
    id = 6
    type = _("Explorer")
    border_color = "#5E6F85"
    bg_color = "#82A4D1"
    font_color = "#2E3F55"
    description = _("for 5 new venues added")

    @classmethod
    def check_for_user(cls, user):
        if user.businesses_added.all().count() >= 5:
            return True
        return False
    pass

class WhatIlookAtBadge(AbstractBadge):
    id = 7
    type = _("Visioneer")
    border_color = "#A19C68"
    bg_color = "#F7AF87"
    font_color = "#555555"
    description = _("for first 5 ophthalmologist reviews")
    
    @classmethod
    def check_for_user(cls, user):
        if user.review_set.filter(business__categories__slug="lekarze-okulisci").count() >= 5:
            return True
        return False
    pass


BADGES[1] = ReviewerBadge
BADGES[2] = JapanTuristBadge
BADGES[3] = GastroDoodBadge
BADGES[4] = NewbieDoodBadge
BADGES[5] = EventerBadge
BADGES[6] = ExplorerBadge
BADGES[7] = WhatIlookAtBadge


