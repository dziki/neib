from django.template.context import RequestContext
from django.shortcuts import render_to_response
from badges.models import BADGES

__author__ = 'kidzik'

def badge_details(request, badge_id):
    badge = BADGES[int(badge_id)]
    users = badge.get_users()

    return render_to_response('badges/badge.details.html',
        context_instance=RequestContext(request, {"badge": badge, "users": users}))