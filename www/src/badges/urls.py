from badges.views import badge_details
from django.conf.urls.defaults import patterns
from badges.models import BADGES

__author__ = 'kidzik'

urlpatterns = patterns('django.views.generic.simple',
    (r'^$', 'direct_to_template', {'template': 'badges/info.html', 'extra_context' : { 'badges': BADGES.values() }}, 'badges_info'),
    (r'^(?P<badge_id>[-\d]+)/$', badge_details, {}, 'badge_details'),
)