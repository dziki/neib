"""Splits query results list into multiple sublists for template display."""

from django.template import Library
from django.utils.safestring import mark_safe
from badges.models import Badge, BADGES
from django.core.urlresolvers import reverse

register = Library()

@register.filter
def show_badge(badge):
    if isinstance(badge, Badge):
        type = BADGES[badge.badge]
    else:
        type = badge
    desc = type.type
    desc = desc.replace('*','<strong>',1)
    desc = desc.replace('*','</strong>',1)
    html = "<a href=\"" + reverse('badge_details', args=str(type.id)) + "\" class=\"badge\">" + desc + "</a>"
    
    return mark_safe(html)