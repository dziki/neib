# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from badges.models import Badge, BADGES

import logging as log
log.basicConfig(level=log.INFO)

class Command(BaseCommand):
# DROP TABLE `business_fitting`
# CREATE TABLE `business_fitting` SELECT * FROM `business_fitting_view` WHERE 1
    args = '[]'
    help = 'Calculate badges'
    
    option_list = BaseCommand.option_list

    def handle(self, *args, **options):
        users = User.objects.all()
        for user in users:
            for k in BADGES.keys():
                BadgeClass = BADGES[k]
                if BadgeClass.check_for_user(user):
                    a,b = Badge.objects.get_or_create(badge=BadgeClass.id, user=user) 
    
        return 