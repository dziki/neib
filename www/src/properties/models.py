# -*- coding: utf-8 --
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

class Property(models.Model):
    key = models.CharField(max_length=100, verbose_name=u"Klucz", unique=True)
    value = models.CharField(max_length=100, verbose_name=u"Wartość")
    date = models.DateTimeField("Ostatnia zmiana",auto_now=True)

    def __str__(self):
        return self.key
    
    def __unicode__(self):
        return self.__str__()

    class Meta:
        verbose_name = u"Właściwości"
        verbose_name_plural = u"Właściwości"
