# -*- coding: utf-8 -*- 
from django.db import models
from django.utils.translation import ugettext_lazy as _

class Question(models.Model):
    """Category model."""
    question = models.CharField(_('pytanie'), max_length=100)
    answer = models.TextField(_('odpowiedź'), max_length=1000)
    priority = models.IntegerField(_('prorytet'), default=0)
    group = models.CharField(_('grupa'), max_length=100)

    def __unicode__(self):
        return u'%s' % self.question
