# -*- coding: utf-8 -*-
'''
Created on 2010-05-05 at 10:38:06

@author: lukasz
'''
from django.contrib import admin
from faq.models import Question

admin.site.register(Question)
