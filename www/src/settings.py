# -*- coding: utf-8 -*-
# Django settings for neib project.
from os import path

DEBUG = False
TEMPLATE_DEBUG = DEBUG
THUMBNAIL_DEBUG = DEBUG

# Debug toolbar
INTERNAL_IPS = ('127.0.0.1',)

DEBUG_TOOLBAR_CONFIG = dict()
DEBUG_TOOLBAR_CONFIG['INTERCEPT_REDIRECTS'] = False


PROJECT_PATH = path.realpath(path.dirname(__file__))
APPEND_SLASH = True

ADMINS = (
    ('Lukasz Kidzinski', 'lukasz.kidzinski@gmail.com'),
    ('Adam Kalinski', 'adamkalinski@gmail.com'),
)

MANAGERS = ADMINS


DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.mysql',
        'NAME': 'dood',
        'USER': 'root',
        'PASSWORD': 'udDtKkZn',
        'HOST': 'localhost',
        'PORT': '',
        'DENORM_BACKEND': 'mysql',
#        'OPTIONS': {
#            'autocommit': True,
#        },
    }
}

DATABASE_ENGINE = 'mysql'           # 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
DATABASE_ENGINE = 'django.contrib.gis.db.backends.mysql'
DATABASE_NAME = 'dood'             # Or path to database file if using sqlite3.
DATABASE_USER = 'root'             # Not used with sqlite3.
DATABASE_PASSWORD = 'udDtKkZn'         # Not used with sqlite3.
DATABASE_HOST = 'localhost'             # Set to empty string for localhost. Not used with sqlite3.
DATABASE_PORT = ''             # Set to empty string for default. Not used with sqlite3.

DENORM_BACKEND = 'mysql'

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Warsaw'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en'

LANGUAGES = (
    ('pl', 'Polski'),
    ('en', 'English'),
    ('fr', 'Francais'),
    ('de', 'Deutsch'),
    ('cs', '\xc4\x8de\xc5\xa1tina'),
    ('uk', '\xd0\xa3\xd0\xba\xd1\x80\xd0\xb0\xd1\x97\xd0\xbd\xd1\x81\xd1\x8c\xd0\xba\xd0\xb0'),
)

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = '/var/neib-media/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = 'http://static.neib.org/admin-media/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'xw*a*k9h+2b3m574jmr97cv6r=1-!k^rna=n2mz7(1#3qm35!-'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    #'django.template.loaders.filesystem.load_template_source',
    #'django.template.loaders.app_directories.load_template_source',
    #'django.template.loaders.eggs.load_template_source',
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
#    "django.core.context_processors.auth",
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
    "django.core.context_processors.csrf",
    "community.context_processors.location",
    "location.context_processors.areas",
    "context_processors.googleapi",
    "notify.context_processors.notify",
)

MIDDLEWARE_CLASSES = (
#    'django.middleware.cache.UpdateCacheMiddleware',
#    'django.middleware.csrf.CsrfViewMiddleware',
#    'staticgenerator.middleware.StaticGeneratorMiddleware',
#    'common.middleware.CsrfFuckMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'pagination.middleware.PaginationMiddleware',
#    'api.middleware.SetCurrentSessionInRequest',
    'api2.middleware.SetCurrentSessionInRequest',
#    'facebook.djangofb.FacebookMiddleware',
#    'facebookconnect.middleware.FacebookConnectMiddleware',
    'socialregistration.middleware.FacebookMiddleware',
#    'django.middleware.cache.FetchFromCacheMiddleware',
#    'common.middleware.MinifyHTMLMiddleware',
    'international.middleware.SubdomainLangMiddleware',
    'i18nurls.middleware.LocaleMiddleware',
    'location.middleware.SetCurrentLocationInSession',
#    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    PROJECT_PATH + '/templates',
    PROJECT_PATH + '/templates/messages',
    PROJECT_PATH + '/badges/templates',
    PROJECT_PATH + '/invite/templates',
    PROJECT_PATH + '/socialregistration/templates',

    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
#    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.gis',
    'django.contrib.sitemaps',
    'django.contrib.markup',
#    'articles',
    'feedback',
    'api',
    'badges',
    'dynamicforms',
    'community',
    'compliments',
    'common',
    'business',
    'search',
    'registration',
    'gallery',
    'flagging',
    'messages',
    'forum',
    'location',
    'pagination',
    'notify',
    'ownership',
    'date',
    'notification',
    'event',
    'tagging',
    'newsletter',
    'rss',
    'properties',
    'compress',
    'words',
    'adverts',
    'businessownersguide',
    'faq',
    'tests',
    'partners',
    'inlineadmin',
    'socialregistration',
    'spriter',
    'i18nurls',
    'rosetta',
#    'invite',
    'debug_toolbar',
    'denorm',
    'reputation',
    'chunks',
    'captcha',
    'mobile',
    'website',
    'main',
    'db',
    'sorl.thumbnail',
)

AUTHENTICATION_BACKENDS = (
    'community.auth.EmailBackend',
    'socialregistration.auth.FacebookAuth',
)

DUMMY_FACEBOOK_INFO = {
    'uid':0,
    'name':'(Private)',
    'first_name':'(Private)',
    'last_name':'(Private)',
    'pic_square_with_logo':'http://www.facebook.com/pics/t_silhouette.gif',
    'affiliations':None,
    'status':None,
    'proxied_email':None,
}

AUTH_PROFILE_MODULE = 'community.Profile'

DEFAULT_CHARSET = 'utf-8'

DEFAULT_FROM_EMAIL = 'neib.org <admin@neib.org>'

DEFAULT_PAGE_TITLE = 'neib.org'

PHOTOS_PATH = 'photos/'
PHOTOS_URL = 'photos/'

SERIALIZATION_MODULES = {
    'json2': 'serializers.json'
}

LOGIN_REDIRECT_URL = '/start/'

ACCOUNT_ACTIVATION_DAYS = 7

FACEBOOK_API_KEY = 'd17b98d9f31c0e6857fdd6be8eee4d75'
FACEBOOK_SECRET_KEY = '6cd30b63fa2dd6f609ed55ca15571b22'
FACEBOOK_INTERNAL = True

FACEBOOK_CACHE_TIMEOUT = 1800

SOCIALREGISTRATION_GENERATE_USERNAME = True

COMPRESS = False #not DEBUG
COMPRESS_VERSION = True
COMPRESS_YUI_BINARY = '../libs/yuicompressor-2.4.2.jar'

CSSTIDY_BINARY = '/usr/bin/csstidy'

COMPRESS_CSS = {
    'screen': {
        'source_filenames': ('style/screen/base/style.css',
                             'style/screen/jquery/jquery.intellisearch.css',
                             'style/screen/jquery/jquery.rating.css',
                             'style/screen/jquery/ui/jquery-ui.css',
                             'style/screen/base/ownership/photo.css',
                             'style/screen/base/search/filters.css',
                             'style/screen/widgets/comments.plugin.css',
                             ),
        'output_filename': 'style/screen.r?.css',
        'extra_context': {
            'media': 'screen,projection',
        },
    },
    'print': {
        'source_filenames':('style/print/print.css',),
        'output_filename': 'style/print.r?.css',
        'extra_context':{
            'media': 'print',
        }
    }
}

COMPRESS_JS = {
    'all': {
        'source_filenames': ('js/jquery-1.4.2.min.js',
                             'js/jquery-ui-1.8.custom.min.js',
                             'js/jquery.rating.js',
                             'js/jquery.cookie.js',
                             'js/jquery.tinysort.js',
                             'js/jquery.translate.js',
                             'js/jquery.dood.js',
                             'js/ui/jquery.ui.datepicker-pl.js',
                             'js/widgets/comments.plugin.js',
                             'js/dood.api.js',
                             'js/dood.header.js',
                             'js/dood.map.js',
                             'js/dood.search.js',
                             'js/dood.gallery.js',

                             'js/dood.category.js',
                             'js/dood.business.js',
                             'js/dood.event.js',
                             'js/dood.profile.js',
                             ),
        'output_filename': 'js/all_r?.js',
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

WEB_ROOT = 'C:\\Users\\kidzik\\workspace\\www\\src\\tmp\\'
WEB_ROOT = '/var/dood-html/'

SESSION_COOKIE_DOMAIN = ".neib.org"

STATIC_GENERATOR_URLS = (
#    r'/biz/([\w-]+)/',
)

LOCALE_PATHS = (
    '/var/neib-data/locale/db',
)

# Cache
CACHE_BUSINESS_COUNT = 60*60*24

ROSETTA_ENABLE_TRANSLATION_SUGGESTIONS = False
ROSETTA_EXCLUDED_APPLICATIONS = ('neib','src')

RECAPTCHA_PUBLIC_KEY = '6Ld2nMcSAAAAAMJqvsfSQkaeh3u3-AJlPu8a7YsN'
RECAPTCHA_PRIVATE_KEY = '6Ld2nMcSAAAAABD8M5ERXGLv28xwyasi7sDLaWOU'

try:
    from localsettings import *
except ImportError:
    pass
