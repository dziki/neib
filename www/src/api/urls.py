from api.views import *
from django.conf.urls.defaults import patterns
from forum.models import Post, Thread

urlpatterns = patterns('',
    (r'^session/start/$', api_start),
    (r'^session/set-position/$', api_session_position_set),
    (r'^session/set-address/$', api_session_address_set),

    (r'^profile/login/$', api_profile_login),
    (r'^profile/logout/$', api_profile_logout),

    (r'^business/list/$', api_business_list),
    (r'^business/get/$', api_generic_details, {'Model': Business}),

    (r'^user/list/$', api_user_list),
    (r'^location/list/$', api_generic_auth_list, {'Model': Location}),
    (r'^category/list/$', api_category_list),
    (r'^review/list/$', api_review_list),
    (r'^photo/list/$', api_photo_list),

    (r'^review/get/$', api_generic_details, {'Model': Review}),
    (r'^user/get/$', api_generic_details, {'Model': Profile}),
    (r'^post/get/$', api_generic_details, {'Model': Post}),
    (r'^thread/get/$', api_generic_details, {'Model': Thread}),
    (r'^category/get/$', api_generic_list, {'Model': Category}),
    (r'^photo/get/$', api_generic_details, {'Model': Photo}),
    
    (r'^thread/list/$', api_blank),
    (r'^thread/get/$', api_blank),

    (r'^post/get/$', api_blank),

    (r'^profile/register/$', api_user_register),

    (r'^friend-proposition/(?P<action>[-\w]+)/$', api_friend_proposition, {}),

    (r'^business/(?P<id>\d+)/(?P<flag>[-\w]+)/$', api_generic_bookmark, {'Model': Business}),
    (r'^user/(?P<id>\d+)/(?P<flag>[-\w]+)/$', api_generic_bookmark, {'Model': User}),
    
    (r'^review/add/$', api_review_add),
    (r'^review/(?P<flag>[-\w]+)/$', api_generic_bookmark, {'Model': Review}),
    (r'^review/(?P<review_id>[-\w]+)/comment/$', api_review_comment_add, {}),
    (r'^business/(?P<flag>[-\w]+)/$', api_generic_bookmark, {'Model': Business}),
    (r'^user/(?P<flag>[-\w]+)/$', api_generic_bookmark, {'Model': User}),

    (r'^user/friend/add/$', api_friend_add),
    
    (r'^compliment/add/$', api_compliment_add),
    
    # non-api
    (r'^search/hint/$', api_search_hints),
)
