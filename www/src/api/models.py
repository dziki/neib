"""
Models for api

"""
from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from location.models import GeoModel

import random
import string

from serializers.datadumper import DataDumper

class ApiSession(GeoModel):
    key = models.CharField(max_length=90, primary_key=True, verbose_name="Wersja klienta")
    user = models.ForeignKey(User, blank=True, null=True, verbose_name="Wlasciciel")
    client_version = models.CharField(max_length=100, verbose_name="Wersja klienta")
    api_version = models.PositiveIntegerField()
    categories_date = '2008-08-08 23:00:00'
    photo_server = settings.STATIC_URL + 'photos/'
    
    export_fields = ('key','msg','categories_date','photo_server')
    export_fields_ex = ()
    
    def id(self):
        return self.key


    #noinspection PyUnusedLocal
    def generate_key(self, length=8, chars=string.letters + string.digits):
        self.key = ''.join([random.choice(chars) for i in range(length)])
    
    def msg(self):
        return 'Czesc Tereska!'

class ApiKeys(models.Model):
    key = models.CharField(max_length=100, verbose_name="Wersja klienta")
    user = models.ForeignKey(User, blank=True, null=True, verbose_name="Wlasciciel")
    name = models.CharField(max_length=100, verbose_name="Nazwa aplikacji")
    notice = models.TextField(verbose_name="Uwagi")
    
class ApiResponse:
    error = 0
    request_id = 0
    num_of_results = 0
    result = list()
    fields = list()
    kwargs = dict()
    
    
    def __init__(self, objects, **kwargs):
        self.result = objects
        self.kwargs = kwargs
        self.error = kwargs.get('error', 0)
        self.request_id = kwargs.get('request_id', 0)
        self.num_of_results = kwargs.get('num_of_results', 0)
    
    def dump(self):
        dumper = DataDumper(**self.kwargs)
        if self.error != 0:
            self.result = list()
        return dumper.dump({ 'result': self.result, 'error': self.error, 'num_of_results': self.num_of_results, 'request_id': self.request_id})

