from api.models import ApiSession

class SetCurrentSessionInRequest(object):

    def process_request(self, request):
        try:
            session_key = request.GET['session_key']
        except KeyError:
            pass
        else:
            # HTTP_X_FORWARDED_FOR can be a comma-separated list of IPs.
            # Take just the first one.
            try:
                request.api_session = ApiSession.objects.get(key=session_key)
                if request.api_session.user is not None:
                    request.user = request.api_session.user
            except ApiSession.DoesNotExist:
                pass
