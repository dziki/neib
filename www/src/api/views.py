# -*- coding: utf-8 -*- 
from django.core import serializers
from django.contrib.auth import authenticate, login

from search.models import SearchEngine, SearchHint
from django.utils.decorators import decorator_from_middleware
from django.middleware.gzip import GZipMiddleware

from django.views.decorators.csrf import csrf_exempt
from flagging.models import get_flagged_objects, Flag
from django.db import IntegrityError
from api.models import ApiSession
from api.shortcuts import api_response, api_response_ok, api_response_error
from community.models import Location, Profile, Friendship, FriendshipProposition
from django.contrib.gis.geos.geometry import GEOSGeometry
from business.models import Category, Review, Business
from django.contrib.auth.models import User
from gallery.models import Photo
from compliments.models import ComplimentType, Compliment
from datetime import datetime
from django.http import HttpResponse

SHORT_FIELDS = ['id','date']

# TODO: dopisać do info o API informację o 'pagination' w business_list

@decorator_from_middleware(GZipMiddleware)
def api_start(request):
    session = ApiSession()
    session.api_version = 100
    session.client_version = 'TEST'
    session.generate_key()
    session.save()
    
    return api_response([session,])

@decorator_from_middleware(GZipMiddleware)
def api_session_position_set(request):
    loc = Location()
    loc.name = 'Pozycja automatyczna'
    loc.position = GEOSGeometry('POINT(%s %s)' % (request.GET['longitude'], request.GET['latitude']))
    
    return api_response([loc,])

# TODO: ZWRACA LOKACJE Z NAZWA PUSTA
@decorator_from_middleware(GZipMiddleware)
def api_session_address_set(request):
    loc = Location()
    loc.location_from_string(request.GET['address'])
    
    return api_response([loc,])

@decorator_from_middleware(GZipMiddleware)
def api_profile_login(request):
    request.api_session.user = authenticate(username=request.GET['email'], password=request.GET['password']) 
    request.api_session.save()
    if request.api_session.user is None:
        return api_response(None, error=-2)
    users = Profile.objects.filter(user=request.api_session.user)

    return api_response(users)

@decorator_from_middleware(GZipMiddleware)
def api_profile_logout(request):
    request.api_session.user = None
    request.api_session.save()
    return api_response_ok()

@decorator_from_middleware(GZipMiddleware)
def api_category_list(request):
    parent_id = request.GET.get('parent', None)
    cats = Category.objects.all()
    if parent_id is not None:
        cats = cats.filter(parent__id = parent_id)
    return api_response(cats, extend=Category, fields=None)

@decorator_from_middleware(GZipMiddleware)
def api_generic_details(request, Model):
    ids = request.GET['id'].split(',')
    return api_response(Model.objects.filter(id__in=ids), expand=3)

@decorator_from_middleware(GZipMiddleware)
def api_generic_bookmark(request, Model, flag, id=None):
    if not request.user.is_authenticated():
        return api_response(None, error=-4)

    if id is None:
        id = request.GET['id']
    
    object = Model.objects.get(id__exact=id)
    
    if (Model == Review):
        if (object.user == request.user):
            return api_response(None, error=-107)
    
    flag_obj = Flag(content_object=object, user=request.user)
    flag_obj.get_from_name(flag)
    err = flag_obj.save()
    if err != 0:
        return api_response(None, error=err)

    return api_response_ok()

@decorator_from_middleware(GZipMiddleware)
def api_friend_add(request):
    if not request.user.is_authenticated():
        return api_response(None, error=-4)
    
    user = User.objects.get(id=request.GET['id'])
    f = Friendship(user1=request.user, user2=user)
    f.save()
    
    return api_response_ok()
    
@decorator_from_middleware(GZipMiddleware)
def api_friend_proposition(request, action):
    if not request.user.is_authenticated():
        return api_response(None, error=-4)

    id = request.GET.get('id',0)
    if id == 0:
        return api_response(None, error=-5) # todo opisać error
    
    id1 = int(id)
    id2 = int(request.user.id)
    if (id1 > id2):
        id1 = int(request.user.id)
        id2 = int(id)
    object = FriendshipProposition.objects.get(user1__id=id1,user2__id=id2)
    object.used = True
    object.save()
    
    if action == 'add': 
        return api_friend_add(request)

    return api_response_ok()

@decorator_from_middleware(GZipMiddleware)
def api_user_register(request):
    profile = Profile()
    profile.birth_date = request.GET.get('birth_date',datetime.now())
    profile.gender = request.GET.get('gender','M')
    profile.nickname = request.GET.get('nickname',' ')
    
    email = request.GET.get('email',None)
    password = request.GET.get('password','')
    
    if len(password) < 2:
        return api_response(None, error=-2)
        
    try:
        user = User.objects.create_user(email, email, password)
    except IntegrityError:
        return api_response(None, error=-1)
    user.first_name = request.GET.get('first_name','')
    user.last_name = request.GET.get('last_name','')
    
    user.username = request.GET.get('email','')
    user.save()
    profile.user = user
    profile.save()
    
    user = authenticate(username=email, password=password)
    login(request, user)
    
    return api_response([user,])
    
@decorator_from_middleware(GZipMiddleware)
def api_generic_list(request, Model):
    return api_response(Model.objects.all(), fields=SHORT_FIELDS)

@decorator_from_middleware(GZipMiddleware)
def api_generic_auth_list(request, Model):
    user = request.api_session.user
    return api_response(Model.objects.filter(user=user))

@decorator_from_middleware(GZipMiddleware)
def api_blank(request):
    return api_response_ok()

@decorator_from_middleware(GZipMiddleware)
def api_user_details(request, user_id):
    """

    """
    profile = Profile.objects.filter(id=user_id)
    
    serialized = serializers.serialize('json', profile, ensure_ascii=False)
    return HttpResponse(serialized, mimetype='application/json')


@decorator_from_middleware(GZipMiddleware)
def api_user_list(request):
    user_id = request.GET.get('user_id', None)

    user = request.user
    if user_id is not None:
        user = User.objects.get(id=user_id)

    if user is None:
        return api_response(None, error=-4)

    friends = Friendship.objects.get_friends(user)
    ids = list()
    for item in friends:
        ids.append(item.id)

    return api_response(Profile.objects.filter(id__in = ids).order_by('user__last_name'))

@decorator_from_middleware(GZipMiddleware)
def api_review_list(request):
    fields = None
    if request.GET.get('extend','0') != '1':
        fields = SHORT_FIELDS
    if request.GET.has_key('business_id'):
        return api_response(Review.objects.filter(business__id=request.GET['business_id']), fields=fields)
    return api_response(Review.objects.filter(user__id=request.GET['user_id']), fields=fields)
    
@decorator_from_middleware(GZipMiddleware)
def api_photo_list(request):
    return api_response(Photo.objects.filter(gallery__id=request.GET['gallery_id']), fields=SHORT_FIELDS)
    
@csrf_exempt
@decorator_from_middleware(GZipMiddleware)
def api_review_add(request):
    review = Review()
    review.user = request.api_session.user
    if review.user is None:
        return api_response(None, error=-4)

    review.content = request.POST.get('body',None)
    review.rating = request.POST.get('rating',None)
    business_id = request.POST.get('business_id', None)
    if review.content is None or review.rating is None or business_id is None:
        return api_response(None, error=-103)
    
    business = Business.objects.get(id__exact=business_id)
    review.business = business
    review.save()
    
    return api_response_ok()

@decorator_from_middleware(GZipMiddleware)
def api_compliment_add(request):
    if not request.user.is_authenticated():
        return api_response(None, error=-4)
    
    user = User.objects.get(id = request.POST['user_id'])
    compliment_type = ComplimentType.objects.get(id = request.POST['compliment_id'])
    compliment = Compliment()
    compliment.body = request.POST['body']
    compliment.compliment = compliment_type
    compliment.recipient = user
    compliment.sender = request.user
    compliment.save()

    return api_response_ok()
    
@decorator_from_middleware(GZipMiddleware)
def api_business_list(request):
    """
    JSON version of search results. For ajax and mobile phones

    **Required arguments**

    We shell see what we need here. Currently none.

    **Response:**

    JSON businesses data.

    """
    fields = None
    if request.GET.get('extend','0') != '1':
        fields = SHORT_FIELDS

    what = request.GET.get('what','')
    page = request.GET.get('page',1)
    pagination = request.GET.get('pagination','1')
    where = request.GET.get('where','')
    category = request.GET.get('category','')
    name = request.GET.get('name', None)

    search = SearchEngine()

    if name:
        if where:
            search.filter_where(where)
        businesses = search.businesses.filter(name__icontains = name)[:10]
        return api_response(businesses)
    


    # what?
    if len(what) > 1:
        search.filter_what(what)
    
    if (category is not None) and (category != 0) and (category != '0') and (category != ''):
        search.filter_category(category)
    
    # where?
    if request.GET.get('bounds',0) == '1':
        top = request.GET.get('top',0)
        right = request.GET.get('right',0)
        bottom = request.GET.get('bottom',0)
        left = request.GET.get('left',0)
        search.filter_polygon(top, right, bottom, left)
    elif where:
        search.filter_where(where)
        
    search.order_by(request.GET.get('order_by',None))
    
    # get results and slice them
    businesses = search.businesses
    num_of_results = businesses.count()
    businesses = businesses.select_related('main_review','gallery__representative')

    bookmarked_by = request.GET.get('bookmarked_by',0)
    if bookmarked_by:
        businesses = get_flagged_objects('ulubiony', User.objects.get(id=bookmarked_by), Business)
        return api_response(businesses, num_of_results=len(businesses), fields=fields, request_id=request.GET.get('request_id',0))

    if pagination == '1':
        businesses = businesses[(10*(int(page)-1)):(10*(int(page)))]
    else:
        businesses = businesses[:50]

    for business in businesses:
        business.address = business.address.encode('utf8')

    return api_response(businesses, num_of_results=num_of_results, fields=fields, request_id=request.GET.get('request_id',0))


@decorator_from_middleware(GZipMiddleware)
def api_search_hints(request):
    #where = request.GET.get("where",None)
    if "what" in request.GET:
        hint = SearchHint(request.GET['what'], max_results=5)
        return api_response(hint.hints)
    else:
        return api_response_error([])


def api_review_comment_add(request, review_id):
    user = request.user
    try:
        review = Review.objects.get(pk=int(review_id))
        comment = request.POST.get('comment', None)
        if user.is_authenticated() and comment:
            review.add_comment(comment, user)
            return api_response_ok()
        else:
            return api_response_error([])

    except Review.DoesNotExist:
        return api_response_error(['Review does not exist'])


