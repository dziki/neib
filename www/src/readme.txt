INSTALATION:
# django -> revision 14507
apt-get install python-mysql
apt-get install libgeos-c1
# setup database in settings.py
apt-get install python-setuptools

# Download https://github.com/simplegeo/python-oauth2
# python setup.py install
easy_install django-social-auth
apt-get install csstidy
apt-get install memcached
apt-get install libmemcache-dev libmemcache0
easy_install python-memcached

django-debug-toolbox needs to be installed

------------

CRON JOBS etc.

python manage.py denorm_rebuild

python manage.py denorm_deamon is needed  