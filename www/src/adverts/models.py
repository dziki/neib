# -*- coding: utf-8 -*-
from django.db import models

from business.models import Business

from datetime import datetime, timedelta
from date.models import TimedModel

AD_TYPES = (
    ('sponsored_link_search', "Link sponsorowany w wynikach"),
    ('sponsored_link_category', "Link sponsorowany w kategorii"),
    ('promotions', "Promocje na głównej"),
    ('ads_off_business', "Wyłączenie reklam w lokalu"),
    ('main_review', "Wybór recenzji głównej"),

    ('sponsored_link_main', "Link sponsorowany na stronie głównej"),
    ('sponsored_link_business', "Link sponsorowany w lokalu"),
    ('positioning', "Pozycjonowanie w wynikach"),
    ('neib_visit', "Odwiedzenie lokalu przez neib"),
    ('facebook_entry', "Wpisy na Facebook"),
    ('newsletter_entry', "Wpis w newsletter"),
)
AD_TYPES_DICT = dict(AD_TYPES)

SUBSCRIPTION_TYPES = (
    ('premium', "neibPremium"),
    ('pro', "neibPro"),
)
SUBSCRIPTION_DICT = dict(SUBSCRIPTION_TYPES)

class Advertisment(models.Model):
    business = models.ForeignKey(Business)
    start = models.DateTimeField(default=datetime.now())
    expire = models.DateTimeField(default=datetime.now())
    type = models.CharField(max_length=30, choices=AD_TYPES)

class CurrentSubscription(models.Model):
    business = models.ForeignKey(Business)
    type = models.CharField(max_length=30, choices=SUBSCRIPTION_TYPES)
    
    expire = models.DateTimeField(default=datetime.now())
    
    def __str__(self):
        return SUBSCRIPTION_DICT[self.type]
    def __unicode__(self):
        return self.__str__()
    
class SubscriptionOrder(TimedModel):
    business = models.ForeignKey(Business)
    months = models.IntegerField(default = 1)
    type = models.CharField(max_length=30, choices=SUBSCRIPTION_TYPES)

    payed = models.DateTimeField(default = None, null = True, blank = True)
    price = models.FloatField()
        
    def complete(self):
        if self.payed:
            return 
        
        self.payed = datetime.now()
        
        cs, b = CurrentSubscription.objects.get_or_create(business = self.business)
        cs.type = self.type
        
        if (cs.expire > datetime.now()):
            cs.expire += timedelta(days=31 * self.months)
        else:
            cs.expire = datetime.now() + timedelta(days=31 * self.months)
        cs.save()
        self.save()
        
        for type in AD_TYPES:
            adv, b = Advertisment.objects.get_or_create(business = self.business, type = type[0])
            adv.expire = cs.expire
            adv.save()
        
        return self