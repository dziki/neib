from django.contrib import admin
from adverts.models import Advertisment, SubscriptionOrder, CurrentSubscription

class AdvertismentAdmin(admin.ModelAdmin):
    pass

class SubscriptionOrderAdmin(admin.ModelAdmin):
    pass

class CurrentSubscriptionAdmin(admin.ModelAdmin):
    pass

admin.site.register(Advertisment, AdvertismentAdmin)
admin.site.register(SubscriptionOrder, SubscriptionOrderAdmin)
admin.site.register(CurrentSubscription, CurrentSubscriptionAdmin)
