# -*- coding: utf-8 -*-
from django.db import models
# !!! do not import   business.models
from datetime import datetime

class AdvertsManager(models.Manager):
    pass

class AdsBusinessQuerySet(models.query.QuerySet):
    def with_active_advert(self, ad_type):
        now = datetime.now()
        return self.filter(advertisment__type__exact=ad_type,
#                           advertisment__start__lt=now,
                           advertisment__expire__gt=now)

class BusinessAdvertManager(models.Manager):
    def get_query_set(self):
         return AdsBusinessQuerySet(self.model)

    def __getattr__(self, name):
        return getattr(self.get_query_set(), name)
    

class AdManager(object):
    def __init__(self, instance):
        self.adverts = instance.advertisment_set

    def is_enabled(self, ad_type):
        return bool(self.adverts.filter(type__exact=ad_type))

def add_manager(sender, instance, **kwargs):
    instance.adverts = AdManager(instance)

