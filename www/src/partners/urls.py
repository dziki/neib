from django.conf.urls.defaults import patterns
from django.contrib import admin

from partners.models import Partner

admin.autodiscover()

urlpatterns = patterns('django.views.generic.simple',
    (r'^$', 'direct_to_template', {'template': 'partners/index.html', 'extra_context': {'partners': Partner.objects.all()}}, 'partners_index'),
)