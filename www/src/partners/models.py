# -*- coding: utf-8 -*- 
from django.db import models


class Partner(models.Model):
    name = models.CharField(max_length=90, verbose_name="Nazwa")
    photo = models.ImageField(verbose_name="Logo", upload_to="partners")
    url = models.CharField(max_length=90, verbose_name="URL")
    description = models.TextField(max_length=1000, verbose_name="Opis")
    

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name