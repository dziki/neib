from django.contrib import admin

from partners.models import Partner


class PartnerAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'url')


admin.site.register(Partner, PartnerAdmin)
