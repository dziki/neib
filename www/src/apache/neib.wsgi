import os, sys

#Calculate the path based on the location of the WSGI script.
apache_configuration= os.path.dirname(__file__)
project = os.path.dirname(apache_configuration)
workspace = os.path.dirname(project)
sys.path.append(workspace) 
sys.path.append(project)

#Add the path to 3rd party django application and to django itself.
sys.path.append('/usr/lib/python2.5/site-packages/django-trunk/django')

os.environ['DJANGO_SETTINGS_MODULE'] = 'neib.settings'
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

