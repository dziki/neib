# -*- coding: utf-8 -*- 
from django.db import models
from django.contrib.auth.models import User
from event.models import Event
from business.models import Review

class Newsletter(models.Model):
    """ Business description """
    user = models.ForeignKey(User, blank=True, null=True, verbose_name="Autor", related_name="author_of_newsletters")
    date = models.DateField(unique=True, max_length=100)
    
    events = models.ManyToManyField(Event, verbose_name="Wydarzenia")
    users = models.ManyToManyField(User, verbose_name="Użytkownicy", related_name="mentioned_in_newsletters")
    reviews = models.ManyToManyField(Review, verbose_name="Recenzje")
    
    subject = models.CharField(max_length=200, verbose_name="Temat")
    lead = models.TextField(max_length=2000, verbose_name="Lead")
    content = models.TextField(max_length=6000, verbose_name="Treść")
    photo = models.ImageField(verbose_name="Fotka", upload_to="newsletter")

    class Meta:
        ordering = ["-id"]
