# -*- coding: utf-8 -*- 

from django.shortcuts import render_to_response
from django.template.loader import render_to_string
from django.template import RequestContext

from newsletter.models import Newsletter

from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives

from community.models import PreferencesFieldValue, Profile, PreferencesField

def newsletter_archive(request):
    
    return render_to_response('newsletter/archive.html', 
                              {"newsletters": Newsletter.objects.all()},
                               context_instance=RequestContext(request)) 

def newsletter(request, date):
    newsletter = Newsletter.objects.get(date=date)

    return render_to_response('newsletter/newsletter.html', 
                              {"newsletter": newsletter, "static": True},
                               context_instance=RequestContext(request))

def newsletter_send(request, date):
    newsletter_obj = Newsletter.objects.get(date=date)
    rendered = render_to_string('newsletter/newsletter.html', 
                              {"newsletter": newsletter_obj},
                               context_instance=RequestContext(request))
    
    text_content = 'TwĂłj klient pocztowy nie obsĹ‚uguje HTML. Zobacz newsletter tutaj: http://neib.org/newsletter/' + newsletter_obj.date.__str__() + '/'

    profiles = Profile.objects.all().filter(registered=1)
    field = PreferencesField(id=7)

#    for profile in profiles:
#        pref = PreferencesFieldValue(field=field, obj=profile)
#        pref.value= "on"
#        pref.save()

    raport = ""
    for profile in profiles:
        pref, v = PreferencesFieldValue.objects.get_or_create(field=field, obj=profile)
        if pref.value != "off":
            msg = EmailMultiAlternatives(newsletter_obj.subject, text_content, 'newsletter@neib.org', [profile.user.email])
            msg.attach_alternative(rendered, "text/html")
            msg.send()
            raport += "Wyslano do " + profile.user.email + "\n"
        else:
            raport += "Niewyslano do " + profile.user.email + "\n"

    send_mail("Raport z mailingu",raport,"newsletter@neib.org",["dziki.dziurkacz@gmail.com"])

    return newsletter(request,date)
