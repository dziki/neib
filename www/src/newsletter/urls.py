from django.conf.urls.defaults import patterns, url

from newsletter.views import *

urlpatterns = patterns('',
                       # Activation keys get matched by \w+ instead of the more specific
                       # [a-fA-F0-9]{40} because a bad activation key should still get to the view;
                       # that way it can return a sensible "invalid key" message instead of a
                       # confusing 404.
                       
                       url((r'^archive/$'),
                           newsletter_archive,
                           name='newsletter_archive'),
                       url(r'^(?P<date>[-\w]+)/$',
                           newsletter,
                           name='newsletter'),
                       url((r'^(?P<date>[-\w]+)/send/$'),
                           newsletter_send,
                           name='newsletter_send'),
                           )