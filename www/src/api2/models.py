"""
Models for api

"""
from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from location.models import GeoModel

import random
import string

from serializers.datadumper import DataDumper

class ApiSession(GeoModel):
    key = models.CharField(max_length=90, primary_key=True, verbose_name="Wersja klienta")
    user = models.ForeignKey(User, blank=True, null=True, verbose_name="Wlasciciel", related_name="api2session_set")
    client_version = models.CharField(max_length=100, verbose_name="Wersja klienta")
    api_version = models.PositiveIntegerField()
    categories_date = '1256953732'
    photo_server = settings.STATIC_URL + 'photos/'
    
    export_fields = ('session_key','category_version','photo_server','version')
    export_fields_ex = ()
    
    def session_key(self):
        return self.key
    def id(self):
        return self.key
    def category_version(self):
        return self.categories_date


    #noinspection PyUnusedLocal
    def generate_key(self, length=8, chars=string.letters + string.digits):
        self.key = ''.join([random.choice(chars) for i in range(length)])
    
    def msg(self):
        return 'Czesc Tereska!'

class ApiKeys(models.Model):
    key = models.CharField(max_length=100, verbose_name="Wersja klienta")
    user = models.ForeignKey(User, blank=True, null=True, verbose_name="Wlasciciel", related_name="api2key_set")
    name = models.CharField(max_length=100, verbose_name="Nazwa aplikacji")
    notice = models.TextField(verbose_name="Uwagi")
    
class ApiResponse:
    error = 0
    num_of_results = 0
    result = list()
    fields = list()
    kwargs = dict()
    
    
    def __init__(self, objects, **kwargs):
        self.result = objects
        self.kwargs = kwargs
        self.error = kwargs.get('error', 0)
        self.num_of_results = kwargs.get('num_of_results', 0)

    def dump(self, version=1):
        dumper = DataDumper(**self.kwargs)
        if self.error != 0:
            self.result = list()
        return dumper.dump({ 'data': self.result, 'code': self.error}, version=version)

