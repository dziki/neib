from api2.models import ApiSession

class SetCurrentSessionInRequest(object):

    def process_request(self, request):
        request.api_session = None

        try:
            session_key = request.GET['session_key']
        except KeyError:
            session_key = None

        if session_key:
            try:
                session = ApiSession.objects.get(key=session_key)
                if session:
                    request.api_session = session
                    if request.api_session.user != None:
                        request.user = request.api_session.user
            except ApiSession.DoesNotExist:
                pass
