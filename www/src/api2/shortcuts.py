from api2.models import ApiResponse
from django.http import HttpResponse

def api_response(request, objects, **kwargs):
    response = ApiResponse(objects, **kwargs)
    return HttpResponse(response.dump(version=2), mimetype='application/json')

def api_response_ok():
    return api_response([])

def api_response_error(objects, **kwargs):
    response = ApiResponse(objects, error=1)
    return HttpResponse(response.dump(version=2), mimetype='application/json')